# frozen_string_literal: true

module Contacts
  class CreateOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
    include DependencyImporter[
      'repositories.contact_repo',
      'validators.contacts.contact_validator',
      'services.translations',
      'tools.error_message',
      'tools.captcha_client'
    ]
    def call(params:, user:)
      validate_params(params)
        .then { |output| verify_captcha(output, user) }
        .then { |output| create_contact(output, user) }
        .then(&method(:result))
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def validate_params(params)
      contact_validator.(params).tap do |validator|
        raise Error::ProcessError, { errors: validator.errors.to_h }.to_json if validator.failure?
      end.to_h
    end

    def verify_captcha(output, user)
      output.tap do |origin_output|
        return origin_output unless user.blank?

        captcha_client.(token: origin_output.delete(:'g-recaptcha-response'))
      end
    end

    def create_contact(output, user)
      output[:user_id] = user.try(:id)

      contact_repo.create(output)
    end

    def result(_contact)
      Success(notice: translations.(%i[flash create_contact]))
    end
  end
end
