# frozen_string_literal: true

namespace :aws do
  desc 'Add lifecycle to bucket properties'
  task add_lifecycle: :environment do
    client = Aws::S3::Client.new(
      access_key_id:     Credentials[:aws, :public_key],
      secret_access_key: Credentials[:aws, :secret_key],
      region:            Credentials[:aws, :region]
    )

    client.put_bucket_lifecycle_configuration(
      bucket:                  Credentials[:aws, :bucket],
      lifecycle_configuration: {
        rules: [
          {
            expiration: { days: Constants::S3_BUCKET_EXPIRATION },
            filter:     { prefix: 'cache/' },
            id:         'cache-clear',
            status:     'Enabled'
          }
        ]
      }
    )
  end
end
