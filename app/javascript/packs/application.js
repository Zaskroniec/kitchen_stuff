// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
import 'src/application.sass';
import 'packs/dependencies/fontawesome';
import 'packs/dependencies/images';
import Turbolinks from 'turbolinks';
import { Application } from 'stimulus';
import { definitionsFromContext } from 'stimulus/webpack-helpers'

const application = Application.start();
const components = require.context('packs/components/', true, /\.js$/);
const pages = require.context('packs/pages/', true, /\.js$/);
application.load(definitionsFromContext(components));
application.load(definitionsFromContext(pages));

Turbolinks.start();
