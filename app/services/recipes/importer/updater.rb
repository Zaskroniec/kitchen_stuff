# frozen_string_literal: true

module Recipes
  module Importer
    class Updater
      include DependencyImporter[
        'repositories.recipe_repo',
        'services.recipes.importer.runner',
        'services.recipes.importer.data_mapper',
        'services.translations',
      ]

      def call(recipe_id:)
        retrieve_recipe(recipe_id)
          .then(&method(:validate_recipe))
          .then(&method(:run_importer_runner))
          .then { |recipe_and_data| update_recipe(*recipe_and_data) }
      end

      private

      def retrieve_recipe(recipe_id)
        recipe_repo.find(recipe_id)
      rescue ActiveRecord::RecordNotFound
        raise Error::ProcessError,
              translations.(%i[exceptions not_found_record], id: recipe_id, repo: recipe_repo.name.capitalize)
      end

      def validate_recipe(recipe)
        recipe.tap do
          unless recipe.external
            raise Error::ProcessError,
                  translations.(%i[exceptions external_recipe], id: recipe.id)
          end

          if recipe.imported
            raise Error::ProcessError,
                  translations.(%i[exceptions imported_recipe], id: recipe.id)
          end
        end
      end

      def run_importer_runner(recipe)
        runner
          .(url: recipe.recipe_url)
          .then { |data| [recipe, data] }
      end

      def update_recipe(recipe, data)
        ActiveRecord::Base.transaction do
          recipe.tap { recipe.update(data_mapper.(data: data)) }
        end
      end
    end
  end
end
