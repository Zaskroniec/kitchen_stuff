# frozen_string_literal: true

require 'rails_helper'

module Recipes
  module Importer
    describe DataMapper do
      describe '#call' do
        subject                     { described_class.new(tag_generator: tag_generator).(data: importer_data) }
        let(:expected_ingredients)  { [{ name: '8 ounces' }, { name: '24 raspberries' }] }
        let(:expected_instructions) { [{ content: 'Equipment: 16 4-oz glass' }] }
        let(:expected_prep_time)    { { hours: nil, minutes: 30 } }
        let(:expected_cook_time)    { { hours: 1, minutes: nil } }
        let(:expected_total_time)   { { hours: 1, minutes: 30 } }
        let(:category)              { double(:category, id: random_uuid) }
        let(:cuisine)               { double(:cuisine, id: random_uuid) }
        let(:tag_generator)         { double(:tag_generator) }
        let(:expected_nutrition) do
          {
            'calories'              => '200 calories',
            'carbohydrate_content'  => '21 grams',
            'cholesterol_content'   => '30 milligrams',
            'fat_content'           => '12 grams',
            'fiber_content'         => '3 grams',
            'protein_content'       => '4 grams',
            'saturated_fat_content' => '6 grams',
            'sodium_content'        => '250 milligrams',
            'sugar_content'         => '11 grams'
          }
        end
        let(:category_args) do
          {
            hash:   { categories_attributes: [{ name: 'Pasta' }] },
            target: :categories_attributes,
            result: :recipe_categories_attributes
          }
        end
        let(:cuisine_args) do
          {
            hash:   { cuisines_attributes: [{ name: 'Japanese' }] },
            target: :cuisines_attributes,
            result: :recipe_cuisines_attributes
          }
        end
        let(:category_result) do
          [{ categories_attributes: [] }, { recipe_categories_attributes: [{ category_id: category.id }] }]
        end
        let(:cuisine_result) do
          [{ cuisines_attributes: [] }, { recipe_cuisines_attributes: [{ cuisine_id: cuisine.id }] }]
        end

        before do
          expect(tag_generator).to receive(:call).with(category_args).and_return(category_result)
          expect(tag_generator).to receive(:call).with(cuisine_args).and_return(cuisine_result)
        end

        context 'success' do
          let(:importer_data) { example_importer_data }

          it 'returns valid name' do
            expect(subject[:name]).to eq(example_importer_data[:name])
          end

          it 'returns valid description' do
            expect(subject[:description]).to eq(example_importer_data[:description])
          end

          it 'returns valid recipe_url' do
            expect(subject[:recipe_url]).to eq(example_importer_data[:recipe_url])
          end

          it 'returns valid recipe_ingredients_attributes' do
            expect(subject[:recipe_ingredients_attributes]).to eq(expected_ingredients)
          end

          it 'returns valid recipe_instructions_attributes' do
            expect(subject[:recipe_instructions_attributes]).to eq(expected_instructions)
          end

          it 'returns valid nutrition' do
            expect(subject[:nutrition]).to eq(expected_nutrition)
          end

          it 'returns valid prep_time' do
            expect(subject[:prep_time]).to eq(expected_prep_time)
          end

          it 'returns valid cook_time' do
            expect(subject[:cook_time]).to eq(expected_cook_time)
          end

          it 'returns valid total_time' do
            expect(subject[:total_time]).to eq(expected_total_time)
          end

          it 'returns valid recipe_categories_attributes' do
            expect(subject[:recipe_categories_attributes]).to eq([{ category_id: category.id }])
          end

          it 'returns valid recipe_cuisines_attributes' do
            expect(subject[:recipe_cuisines_attributes]).to eq([{ cuisine_id: cuisine.id }])
          end
        end

        context 'failure' do
          let(:importer_data) { example_importer_data }

          before { importer_data.merge!(name: nil) }

          it 'raises valid error' do
            expect { subject }.to raise_error(
              Error::ProcessError,
              /^Invalid.*/
            )
          end
        end
      end
    end
  end
end
