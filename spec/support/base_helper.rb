# frozen_string_literal: true

def remove_bucket_id(params)
  params.tap { |p| p.delete(:bucket_id) }
end

def read_example(path)
  File.read(File.expand_path("./spec/support/examples/#{path}"))
end

def example_importer_data
  {
    name:                'No-bake Raspberry Cheesecake Pots',
    image:               'https://lh3.googleusercontent.com/ZaqIZFmoFINFCjnX1aDKzM4bfrv_eYwAcQ2TWGgNgU6NYWf1s8cz',
    description:         'Whip up this layered, no-bake ...',
    recipe_ingredient:   ['8 ounces', '24 raspberries'],
    prep_time:           { hours: nil, minutes: '30M' },
    cook_time:           { hours: '1H', minutes: nil },
    total_time:          { hours: '1H', minutes: '30M' },
    recipe_cuisine:      ['Japanese'],
    recipe_category:     ['Pasta'],
    recipe_yield:        '16 servings',
    recipe_instructions: ['Equipment: 16 4-oz glass'],
    keywords:            'fastfood, bestfood',
    nutrition:           { calories:            '200 calories',
                           carbohydrateContent: '21 grams',
                           cholesterolContent:  '30 milligrams',
                           fatContent:          '12 grams',
                           fiberContent:        '3 grams',
                           proteinContent:      '4 grams',
                           saturatedFatContent: '6 grams',
                           sodiumContent:       '250 milligrams',
                           sugarContent:        '11 grams' },
    recipe_url:          'https://www.allrecipes.com/recipe/132815/balsamic-roasted-pork-loin'
  }
end

def example_mapped_data
  {
    name:                           'No-bake Raspberry Cheesecake Pots',
    description:                    "Whip up this layered, no-bake, naturally colored dessert in a pinch. These pots call for crushed graham crackers and\ncrushed frozen raspberries…making them a dessert worthy for your Valentine crush (or your crush any time of year\nthanks to the year-round availability of frozen raspberries!).",
    recipe_url:                     'https://www.allrecipes.com/recipe/132815/balsamic-roasted-pork-loin',
    keywords:                       nil,
    image_remote_url:               'https://lh3.googleusercontent.com/ZaqIZFmoFINFCjnX1aDKzM4bfrv_eYwAcQ2TWGgNgU6NYWf1s8czlrjWks8PNpk1cVFiIqhU2kD9MaxY9l0ObPg',
    recipe_ingredients_attributes:  [{ name: '8 ounces graham crackers 1/2 box' },
                                     { name: '24 ounces frozen raspberries 2 12-ounce packages' },
                                     { name: '1 pound cream cheese softened' },
                                     { name: '1/2 cup confectioners sugar' },
                                     { name: '4 ounces 2% lowfat greek yogurt plain' },
                                     { name: '1/2 teaspoon salt' },
                                     { name: '2 teaspoons lemon juice' }],
    recipe_instructions_attributes: [{ content: 'Equipment: 16 4-oz glass dishes (such as mason jars, wine glasses, soufflé cups, etc.) OR 8 8-oz glass dishes OR 1 trifle bowl' },
                                     { content: 'To make the cookie crumble layer: In a sealed, plastic bag, crush the cookies with a rolling pin. Set aside.' },
                                     { content: "To make the yogurt cheesecake filling: Using a mixer, beat the softened cream cheese on medium speed for 6 minutes. Scrape down the sides of the\nbowl." },
                                     { content: "Add the confectioner’s sugar, plain yogurt, salt and lemon juice. Mix until combined and smooth (about 3\nminutes). Scrape down the sides of the bowl." },
                                     { content: 'In a sealed bag, gently crush 12-ounces of frozen raspberries with a rolling pin.' },
                                     { content: "Add the 12 ounces of crushed raspberries to the cream cheese mixture. Mix to combine the raspberries, making\nsure the filling is uniform in color. The filling will become lumpy once the raspberries are added to the cream\ncheese." },
                                     { content: 'To make the melted raspberry topping: Crush the second bag of raspberries in the same way you crushed the first. Set aside.' },
                                     { content:                                                   "Assemble: Evenly divide the cookie crumbles in the bottom of each jar; shake the jar to make sure the cookie layer is\nsomewhat evenly distributed on the bottom of the jar." },
                                     { content: "Divide the yogurt cheesecake filling evenly onto the cookie crumbles in the 8 mason jars. Smooth the top of each\njar after adding the filling." },
                                     { content: "Divide the crushed, melted raspberries between the 8 mason jars by placing it on top of the raspberry cream\ncheese filling. Smooth the top." },
                                     { content: 'Refrigerate the finished cheesecakes until ready to serve.' },
                                     { content: 'If desired garnish the tops of the mason jars with chocolates, additional frozen raspberries, or sprinkles!' }],
    nutrition:                      { 'calories'              => '200 calories',
                                      'carbohydrate_content'  => '21 grams',
                                      'cholesterol_content'   => '30 milligrams',
                                      'fat_content'           => '12 grams',
                                      'fiber_content'         => '3 grams',
                                      'protein_content'       => '4 grams',
                                      'saturated_fat_content' => '6 grams',
                                      'sodium_content'        => '250 milligrams',
                                      'sugar_content'         => '11 grams' },
    servings:                       '16 servings',
    prep_time:                      { hours: nil, minutes: nil },
    cook_time:                      { hours: nil, minutes: nil },
    total_time:                     { hours: 1, minutes: nil },
    imported:                       true,
    recipe_categories_attributes:   [],
    recipe_cuisines_attributes:     []
  }
end

def random_uuid
  SecureRandom.uuid
end

def random_token
  SecureRandom.hex(32)
end

def default_recipe_url
  'https://www.allrecipes.com/recipe/132815/balsamic-roasted-pork-loin'
end

def html_recipe_url
  'https://www.allrecipes.com/recipe/261325/roasted-eggplant-pastitsio'
end

def not_supported_url
  'http://test.testing.test.com/recipes'
end

def file_path(file_name)
  File.expand_path("./spec/support/files/#{file_name}")
end

def file_body_example(file_name)
  File.read(file_path(file_name))
end

def time_now
  Time.zone.now
end

class DummyClassError
  def mock
    raise StandardError, 'test message'
  rescue StandardError => e
    Tools::ErrorMessage.(error: e)
  end
end
