# frozen_string_literal: true

Rollbar.configure do |config|
  config.access_token = Credentials[:rollbar, :secret_key]
end
