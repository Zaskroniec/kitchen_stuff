# frozen_string_literal: true

require 'rails_helper'

module Recipes
  describe TimeCalculator do
    describe '#call' do
      let(:array) do
        [
          { 'hours' => nil, 'minutes' => 30 },
          { 'hours' => nil, 'minutes' => 60 }
        ]
      end

      context 'raw value' do
        subject { described_class.new.(array: array) }

        it 'retuns sum' do
          expect(subject).to eq(5400)
        end
      end

      context 'formatted value' do
        subject { described_class.new.(array: array, options: { format: :short }) }

        it 'retuns formatted sum' do
          expect(subject).to eq('1:30')
        end
      end
    end
  end
end
