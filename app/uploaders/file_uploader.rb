# frozen_string_literal: true

class FileUploader < Shrine
  process(:recache) do |io|
    versions = { original: io }

    io.download do |original|
      pipeline = ImageProcessing::MiniMagick.source(original)

      versions[:original] = io
      versions[:medium]   = pipeline.resize_to_limit!(*Constants::MEDIUM_FILE_SIZE)
    end

    versions
  end

  process(:store) do |io|
    io[:original].download do |original|
      pipeline = ImageProcessing::MiniMagick.source(original)

      io[:large]  = pipeline.resize_to_limit!(*Constants::BIG_FILE_SIZE)
      io[:small]  = pipeline.resize_to_limit!(*Constants::SMALL_FILE_SIZE)
    end

    io
  end
end
