# frozen_string_literal: true

module ErrorsConcern
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound,  with: :record_not_found
    rescue_from ActiveRecord::RecordInvalid,   with: :record_invalid
    rescue_from ActiveRecord::RecordNotUnique, with: :record_not_unique

    def record_not_found(_exception)
      flash[:error] = t('exceptions.not_found')
      redirect_back(fallback_location: buckets_path)
    end

    def record_invalid(_exception)
      flash[:error] = t('exceptions.invalid_record')
      redirect_back(fallback_location: buckets_path)
    end

    def record_not_unique(_exception)
      flash[:error] = t('exceptions.not_unique_record')
      redirect_back(fallback_location: buckets_path)
    end
  end
end
