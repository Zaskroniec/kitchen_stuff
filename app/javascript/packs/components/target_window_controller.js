import { Controller } from 'stimulus';

export default class extends Controller {
  openWindow(event) {
    event.preventDefault();

    let clickedElement = event.target;

    if(clickedElement['href'] === undefined) {
      clickedElement = clickedElement.parentNode;
    }
    const newWindow = window.open(clickedElement['href']);

    newWindow.opener = null;
  }
}
