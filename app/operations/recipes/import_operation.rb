# frozen_string_literal: true

module Recipes
  class ImportOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
    include DependencyImporter[
      'validators.recipes.importer_validator',
      'jobs.recipes.importer_job',
      'tools.url_parser',
      'services.translations',
      'tools.error_message'
    ]

    def call(params:, bucket:)
      url_parser.(url: params[:url])
        .then { |url_object| validate_path(url_object.merge(bucket_id: params[:bucket_id])) }
        .then { |url_object| create_recipe(url_object, bucket) }
        .then(&method(:schedule_importer_job))
        .then { |record| Success(result(record)) }
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def validate_path(url_object)
      importer_validator.(url_object)
        .then do |validator|
          return raise_error_message(validator.errors.to_h) if validator.failure?

          validator.to_h.tap { |hash| hash.delete(:bucket_id) }
        end
    end

    def raise_error_message(errors)
      raise Error::ProcessError, translations.(%i[exceptions corrupted_data]) if errors.key?(:path)

      if uniq_error?(errors)
        raise Error::ProcessError, translations.(%i[exceptions duplicated_bucket_recipe])
      end

      raise Error::ProcessError, { errors: errors }.to_json
    end

    def uniq_error?(errors)
      errors.key?(:base_url) &&
        errors[:base_url].include?(translations.(%i[errors unique_field_by_relation?]))
    end

    def create_recipe(url_object, bucket)
      bucket.recipes.create(
        user_id:    bucket.user_id,
        recipe_url: url_object[:base_url],
        external:   true,
        name:       Constants::DEFAULT_IMPORT_NAME
      )
    end

    def schedule_importer_job(record)
      record.tap { importer_job.perform_later(recipe_id: record.id) }
    end

    def result(record)
      {
        notice: translations.(%i[flash import]),
        record: record
      }
    end
  end
end
