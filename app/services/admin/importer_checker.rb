# frozen_string_literal: true

module Admin
  class ImporterChecker
    include DependencyImporter[
      'services.recipes.importer.runner',
      'repositories.admin.recipe_import_repo'
    ]

    EMPTY_DATA = {}.freeze

    def call(supported_sites:)
      recipe_import_repo
        .then(&:create)
        .then { |recipe_import| run_importer(recipe_import, supported_sites) }
    end

    private

    def run_importer(recipe_import, supported_sites)
      recipe_import.tap do |r|
        supported_sites.each do |node|
          begin
            data = runner.(url: node[:url])

            r.imports << build_status_node(node, data)
          rescue StandardError => e
            r.imports << build_status_node(node, EMPTY_DATA, e.message)
          end

          r.result += 1
          r.save
        end
      end
    end

    def build_status_node(node, data, error = nil)
      {
        name:   node[:name],
        data:   data,
        errors: [error].compact
      }
    end
  end
end
