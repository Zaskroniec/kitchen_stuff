# frozen_string_literal: true

module Recipes
  class ImporterJob < ApplicationJob
    queue_as :importer

    def perform(recipe_id:)
      updater.(recipe_id: recipe_id)
    end

    private

    def updater
      @updater ||= dependency_container['services.recipes.importer.updater']
    end
  end
end
