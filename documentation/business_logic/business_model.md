# Business Model

#### ***<span style="color: #77B592;">[&larr; Go to Guide page](./guide.md)</span>***

Kitchen stuff uses Freemium business model and its divided into:

* [Free](#free)
* [Premium](#premium)


## [Free](#free)
Free account type is set automatically after registration.

Plan benefits include:
* Limit of 10 recipe collections
* Limit of 50 recipes per collection
* Exporting recipes to PDF and collections to CSV files.
* Importing recipes for external services
* Dashboard to search for public recipes

---

## [Premium](#premium)
At beta phase it will be unavailable. Later after phase is finished total monthly cost will be around **9.99 €** (it will include also vat.)

Plan benefits include all Free features and additional advantages:
* Unlimited number of recipe collections.
* Unlimited number of recipes per collections.
* Possibiliy to follow user profile, which introduce a notifications about new recipes (status must be public) by author.
* Reviews public recipes
