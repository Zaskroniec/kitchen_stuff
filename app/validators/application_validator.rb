# frozen_string_literal: true

class ApplicationValidator < Dry::Validation::Contract
  include DependencyImporter['services.translations']

  config.messages.default_locale = :en
  config.messages.backend        = :i18n

  register_macro(:match_passwords?) do
    unless values.data[:password].present? && value.eql?(values.data[:password])
      key.failure(translations.(%i[errors password_confirmation]))
    end
  end
end
