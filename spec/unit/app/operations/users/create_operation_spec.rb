# frozen_string_literal: true

require 'rails_helper'

module Users
  describe CreateOperation do
    describe '#call' do
      let(:user_repo)          { double(:user_repo) }
      let(:email)              { Faker::Internet.email }
      let(:nick)               { Faker::Internet.username(4..16) }
      let(:password)           { Faker::Internet.password(8, 32, true) }
      let(:token)              { '1234' }
      let(:new_user_validator) { Users::NewUserValidator.new(user_repo: user_repo) }

      subject do
        described_class.new(user_repo: user_repo, new_user_validator: new_user_validator).(params: params)
      end

      before do
        params[:'g-recaptcha-response'] = token
        params[:terms] = '1'

        expect(user_repo).to receive(:find_by).with(nick: nick).and_return([])
      end

      context 'success' do
        let(:user) { double(:user, id: 'test_id') }
        let(:params) do
          {
            email:                 email,
            nick:                  nick,
            password:              password,
            password_confirmation: password
          }
        end
        let(:request_args) do
          {
            method:    :post,
            url:       Constants::CAPTCHA_URL,
            options:   {
              body: {
                response: token,
                secret:   Credentials[:google, :captcha_secret_key]
              }
            },
            resp_code: 200,
            body:      { 'success' => true }.to_json,
            headers:   { 'Content-Type' => 'application/json' }
          }
        end

        before do
          mock_request_with_result(request_args)

          expect(user_repo).to receive(:find_by).with(email: email).and_return([])
          expect(user_repo).to receive(:create).and_return(user)
        end

        it 'enqueue new job with confirmation instructions' do
          expect { subject }.to enqueue_job(Users::ConfirmationJob)
        end

        it 'enqueue new job with starting buckets' do
          expect { subject }.to enqueue_job(Buckets::StartBucketsJob)
        end

        it 'returns notice message' do
          expect(subject.success[:notice]).to eq('Successfuly created new account.')
        end
      end

      context 'failure' do
        context 'socket error' do
          let(:params) do
            {
              email:                 email,
              nick:                  nick,
              password:              password,
              password_confirmation: password
            }
          end
          let(:request_args) do
            {
              method:  :post,
              url:     Constants::CAPTCHA_URL,
              options: {
                body: {
                  response: token,
                  secret:   Credentials[:google, :captcha_secret_key]
                }
              },
              error:   SocketError.new('Failed to open TCP')
            }
          end

          before do
            mock_request_with_error(request_args)

            expect(user_repo).to receive(:find_by).with(email: email).and_return([])
          end

          it 'not enqueue new job with confirmation instructions' do
            expect { subject }.not_to enqueue_job(Users::ConfirmationJob)
          end

          it 'not enqueue new job with starting_buckets' do
            expect { subject }.not_to enqueue_job(Buckets::StartBucketsJob)
          end

          it 'returns captcha error' do
            expect(subject.failure[:message])
              .to eq('We are sorry, but our system could not connect to external service.')
          end
        end

        context 'invalid captcha' do
          let(:params) do
            {
              email:                 email,
              nick:                  nick,
              password:              password,
              password_confirmation: password
            }
          end
          let(:request_args) do
            {
              method:    :post,
              url:       Constants::CAPTCHA_URL,
              options:   {
                body: {
                  response: token,
                  secret:   Credentials[:google, :captcha_secret_key]
                }
              },
              resp_code: 200,
              body:      { 'success' => false }.to_json,
              headers:   { 'Content-Type' => 'application/json' }
            }
          end

          before do
            mock_request_with_result(request_args)

            expect(user_repo).to receive(:find_by).with(email: email).and_return([])
          end

          it 'not enqueue new job with confirmation instructions' do
            expect { subject }.not_to enqueue_job(Users::ConfirmationJob)
          end

          it 'not enqueue new job with starting_buckets' do
            expect { subject }.not_to enqueue_job(Buckets::StartBucketsJob)
          end

          it 'returns captcha error' do
            expect(subject.failure[:message])
              .to eq('We are sorry, but something goes wront while verify captcha.')
          end
        end

        context 'validation' do
          context 'unique field' do
            let(:collection) { [double(:user)] }
            let(:params) do
              {
                email:                 email,
                nick:                  nick,
                password:              password,
                password_confirmation: password
              }
            end

            before do
              expect(user_repo).to receive(:find_by).with(email: email).and_return(collection)
            end

            it 'not enqueue new job with confirmation instructions' do
              expect { subject }.not_to enqueue_job(Users::ConfirmationJob)
            end

            it 'not enqueue new job with starting_buckets' do
              expect { subject }.not_to enqueue_job(Buckets::StartBucketsJob)
            end

            it 'returns validation errors' do
              expect(subject.failure[:message][:errors]).to eq(email: ['is not unique'])
            end
          end

          context 'invalid password format' do
            let(:params) do
              {
                email:                 email,
                nick:                  nick,
                password:              'testtest$',
                password_confirmation: 'testtest$'
              }
            end

            before do
              expect(user_repo).to receive(:find_by).with(email: email).and_return([])
            end

            it 'not enqueue new job with confirmation instructions' do
              expect { subject }.not_to enqueue_job(Users::ConfirmationJob)
            end

            it 'not enqueue new job with starting_buckets' do
              expect { subject }.not_to enqueue_job(Buckets::StartBucketsJob)
            end

            it 'returns validation errors' do
              expect(subject.failure[:message][:errors])
                .to eq(password: ['is in invalid format'])
            end
          end

          context 'empty confirmation password' do
            let(:params) do
              {
                email:                 email,
                nick:                  nick,
                password:              password,
                password_confirmation: ''
              }
            end

            before do
              expect(user_repo).to receive(:find_by).with(email: email).and_return([])
            end

            it 'not enqueue new job with confirmation instructions' do
              expect { subject }.not_to enqueue_job(Users::ConfirmationJob)
            end

            it 'not enqueue new job with starting_buckets' do
              expect { subject }.not_to enqueue_job(Buckets::StartBucketsJob)
            end

            it 'returns validation errors' do
              expect(subject.failure[:message][:errors]).to eq(password_confirmation: ['does not match password'])
            end
          end
        end
      end
    end
  end
end
