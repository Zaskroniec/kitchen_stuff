import { Controller } from 'stimulus';
import axios from 'axios';

export default class extends Controller {
  openRow(event) {
    const new_path = this.data.get('path');

    window.location.href = new_path;
  }
}
