# frozen_string_literal: true

class AddResetLogicToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :reset_token, :string
    add_column :users, :reset_token_lifetime, :datetime
    add_column :users, :reset_token_at, :datetime

    add_index :users, :reset_token, unique: true
  end
end
