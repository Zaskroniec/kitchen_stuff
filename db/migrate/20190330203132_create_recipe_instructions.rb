# frozen_string_literal: true

class CreateRecipeInstructions < ActiveRecord::Migration[6.0]
  def change
    create_table :recipe_instructions, id: :uuid do |t|
      t.text       :content, null: false
      t.belongs_to :recipe, type: :uuid, foreign_key: true, null: false

      t.timestamps
    end
  end
end
