# frozen_string_literal: true

Sidekiq.configure_server do |config|
  config.redis = { url: Credentials[:worker, :redis_server] }
end

Sidekiq.configure_client do |config|
  config.redis = { url: Credentials[:worker, :redis_client] }
end
