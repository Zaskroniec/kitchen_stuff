# frozen_string_literal: true

module Recipes
  class DataMapperValidator < ApplicationValidator
    params do # rubocop:disable Metrics/BlockLength
      required(:name).value(:string, max_size?: Constants::MAX_STRING_LENGTH)
      required(:recipe_url).value(:string, max_size?: Constants::MAX_STRING_LENGTH)
      required(:imported).value(:bool?, :true?)
      required(:recipe_ingredients_attributes).each do
        schema do
          required(:name).value(:string, :filled?, max_size?: Constants::MAX_STRING_LENGTH)
        end
      end
      required(:recipe_instructions_attributes).each do
        schema do
          optional(:content).maybe(:string)
        end
      end
      required(:prep_time).schema do
        optional(:hours).maybe(:integer)
        optional(:minutes).maybe(:integer)
      end
      required(:cook_time).schema do
        optional(:hours).maybe(:integer)
        optional(:minutes).maybe(:integer)
      end
      required(:total_time).schema do
        optional(:hours).maybe(:integer)
        optional(:minutes).maybe(:integer)
      end
      optional(:description).maybe(:string)
      optional(:keywords).maybe(:string, max_size?: Constants::MAX_STRING_LENGTH)
      optional(:image_remote_url).maybe(:string)
      optional(:servings).maybe(:string, max_size?: Constants::MAX_STRING_LENGTH)
      optional(:nutrition).maybe(:hash)
      optional(:recipe_categories_attributes).maybe(:array)
      optional(:recipe_cuisines_attributes).maybe(:array)
    end

    rule(:recipe_categories_attributes) do
      if value.present? && value.size != value.uniq.size
        key.failure(translations.(%i[errors duplicated_items]))
      end
    end

    rule(:recipe_cuisines_attributes) do
      if value.present? && value.size != value.uniq.size
        key.failure(translations.(%i[errors duplicated_items]))
      end
    end
  end
end
