# frozen_string_literal: true

module Users
  class InlineUpdatePasswordValidator < Users::PasswordValidator
    params do
      required(:current_password).filled(:string)
    end
  end
end
