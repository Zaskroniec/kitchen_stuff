import { Controller } from 'stimulus';

export default class extends Controller {
  addChild(event) {
    event.preventDefault();

    const newElementId = (new Date()).getTime();
    const domParser = new DOMParser();
    const parent = this.element.parentNode;
    const childData = this.data.get('child');
    const parsedChildData = childData.replace(new RegExp(/\d/, 'g'), newElementId);
    const doc = domParser.parseFromString(parsedChildData, 'text/html');
    const newChildElement = doc.body.firstChild;

    parent.insertBefore(newChildElement, this.element);
  }

  removeChild(event) {
    event.preventDefault();

    const parent = this.element.parentNode;
    const root = parent.parentNode;
    const isNewRecord = this.data.get('newRecord');
    const hiddenField = parent.querySelector("input[type='hidden']");

    if(isNewRecord === 'true') {
      root.removeChild(parent);
      return;
    }

    parent.classList.add('removed');
    hiddenField.value = "true";
  }
}
