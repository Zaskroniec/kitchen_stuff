# frozen_string_literal: true

class User < ApplicationRecord
  has_many :buckets
  has_many :recipes
  has_many :contacts

  def reset_token_expired?
    reset_token_lifetime < Time.zone.now
  end

  def show_statistics
    Containers::DependencyContainer['services.users.statistics'].(user: self)
  end
end
