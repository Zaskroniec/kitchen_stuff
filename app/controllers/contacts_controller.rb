# frozen_string_literal: true

class ContactsController < ApplicationController
  include PaginationConcern
  include DependencyImporter[
    'operations.contacts.create_operation',
    'services.static_meta_tags'
  ]

  layout false, only: %i[paginate]

  def index
    set_meta_tags(**static_meta_tags.(key: :contact))
    new_contact
  end

  def paginate
    @pagy, @contacts = pagy_countless(current_user.contacts.newest)
  rescue Error::PageError => _e
    @contacts = []
    render nothing: true, status: :no_content && return
  rescue Pagy::OverflowError => _e
    @contacts = []
    render nothing: true, status: :no_content && return
  end

  def create
    create_operation.(params: prepare_contact_params, user: current_user) do |service|
      service.success do |result|
        redirect_to contacts_path, notice: result[:notice]
      end

      service.failure do |error_object|
        # TODO: Add custom structs instead AR objects
        # new_contact.attributes = unsafe_params[:contact]
        new_contact
        render_error(:index, error_object)
      end
    end
  end

  private

  def prepare_contact_params
    unsafe_params[:contact].tap do |h|
      h['g-recaptcha-response'] = unsafe_params[:'g-recaptcha-response'] unless current_user.present?
    end
  end

  def new_contact
    @new_contact ||= Contact.new
  end
end
