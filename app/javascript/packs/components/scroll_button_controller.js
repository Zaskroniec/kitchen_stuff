import { Controller } from 'stimulus';

export default class extends Controller {
  initialize() {
    window.addEventListener('scroll', this.monitorScroll.bind(this), false);
  }

  monitorScroll() {
    let calc = (window.scrollY / window.innerHeight) * 100;

    if(calc >= 20) {
      this.element.classList.add('btn__scroll--visible')
    } else {
      this.element.classList.remove('btn__scroll--visible')
    }
  }

  scrollUp() {
    const targetPosition = 40;

    this.scrollTo(window.scrollY, targetPosition, 300);
  }

  scrollTo(scrollPosition, to, duration) {
    let difference = to - scrollPosition;
    let perTick = difference / duration * 10;

    setTimeout(() => {
      scrollPosition = scrollPosition + perTick;
      window.scrollTo(0, scrollPosition)

      if (scrollPosition <= to) { return; }

      this.scrollTo(scrollPosition, to, duration - 10);
    }, 10)
  }
}
