# frozen_string_literal: true

class CreateCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :categories, id: :uuid do |t|
      t.string  :name,                   null: false
      t.boolean :public, default: false, null: false

      t.timestamps
    end

    add_index :categories, :name, unique: true
    add_index :categories, :public, name: 'index_categories_on_public_categories', where: '(public IS TRUE)'
  end
end
