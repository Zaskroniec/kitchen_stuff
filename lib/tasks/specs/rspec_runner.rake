# frozen_string_literal: true

require './config/boot'
require 'rspec/core/rake_task'

namespace :specs do
  desc 'run unit specs'
  RSpec::Core::RakeTask.new(:unit) do |task|
    file_list = FileList['spec/**/*_spec.rb']

    %w[acceptance].each do |exclude|
      file_list = file_list.exclude("spec/#{exclude}/**/*_spec.rb")
    end

    task.pattern = file_list
  end

  desc 'run acceptance specs'
  RSpec::Core::RakeTask.new(:acc) do |task|
    file_list = FileList['spec/**/*_spec.rb']

    %w[unit].each do |exclude|
      file_list = file_list.exclude("spec/#{exclude}/**/*_spec.rb")
    end

    task.pattern = file_list
  end

  desc 'run acceptance specs with snapshots'
  task snap: :environment do
    system("PERCY_TOKEN=#{Credentials[:percy, :token]} rake specs:acc")
  end
end
