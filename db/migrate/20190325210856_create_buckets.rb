# frozen_string_literal: true

class CreateBuckets < ActiveRecord::Migration[6.0]
  def change
    create_table :buckets, id: :uuid do |t|
      t.string     :name,                                 null: false
      t.string     :slug,                                 null: false
      t.belongs_to :user, type: :uuid, foreign_key: true, null: false, index: true

      t.timestamps
    end

    add_index :buckets, %i[name user_id], unique: true
    add_index :buckets, %i[slug user_id], unique: true
  end
end
