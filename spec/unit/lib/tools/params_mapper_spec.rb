# frozen_string_literal: true

require 'rails_helper'

module Tools
  describe ParamsMapper do
    describe '#call' do
      let(:params) { { 'test' => 1 } }
      subject { described_class.new.(params: params) }

      context 'basic hash' do
        it 'returns hash' do
          expect(subject).to eq(params)
        end
      end

      context 'hash with mapped array' do
        let(:params) do
          {
            'test_attributes' => {
              '0' => { 'name' => 'test_value' }
            }
          }
        end
        let(:exptected_hash) do
          {
            'test_attributes' => [
              { 'name' => 'test_value' }
            ]
          }
        end

        it 'returns hash' do
          expect(subject).to eq(exptected_hash)
        end
      end
    end
  end
end
