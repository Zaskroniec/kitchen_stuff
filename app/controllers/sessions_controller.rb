# frozen_string_literal: true

class SessionsController < ApplicationController
  include DependencyImporter['services.static_meta_tags']

  before_action :redirect_to_my_buckets, only: %i[new create]
  before_action :redirect_to_new_sesseion, only: %i[destroy]

  skip_before_action :verify_authenticity_token, only: :new

  def new
    set_meta_tags(**static_meta_tags.(key: :login))
    flash.now[:error] = warden.message if warden.message.present?
  end

  def create
    warden.authenticate!
    redirect_to buckets_path, notice: t('flash.login')
  end

  def destroy
    warden.logout
    redirect_to new_sessions_path, notice: t('flash.logout')
  end
end
