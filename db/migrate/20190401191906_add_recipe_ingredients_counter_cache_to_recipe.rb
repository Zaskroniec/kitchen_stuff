# frozen_string_literal: true

class AddRecipeIngredientsCounterCacheToRecipe < ActiveRecord::Migration[6.0]
  def change
    add_column :recipes, :recipe_ingredients_count, :integer, default: 0, null: false
  end
end
