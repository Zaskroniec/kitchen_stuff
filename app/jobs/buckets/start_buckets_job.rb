# frozen_string_literal: true

module Buckets
  class StartBucketsJob < ApplicationJob
    queue_as :default

    def perform(user_id:)
      buckets_generator.(user_id: user_id)
    end

    private

    def buckets_generator
      @buckets_generator ||= dependency_container['services.buckets.generate_starting_buckets']
    end
  end
end
