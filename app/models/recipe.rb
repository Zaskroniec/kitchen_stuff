# frozen_string_literal: true

class Recipe < ApplicationRecord
  include PgSearch::Model
  include FileUploader::Attachment.new(:image)

  belongs_to :bucket, counter_cache: true, touch: true
  belongs_to :user

  has_many :recipe_ingredients,  dependent: :destroy
  has_many :recipe_instructions, dependent: :destroy
  has_many :recipe_cuisines,     dependent: :destroy
  has_many :recipe_categories,   dependent: :destroy
  has_many :cuisines,            through:   :recipe_cuisines,   dependent: :nullify, autosave: true
  has_many :categories,          through:   :recipe_categories, dependent: :nullify, autosave: true

  pg_search_scope :search_for_name,
                  against:            :name,
                  ignoring:           :accents,
                  associated_against: {
                    recipe_ingredients: :name
                  },
                  using:              {
                    tsearch: {
                      dictionary: I18n.t("languages.#{I18n.locale}"),
                      any_word:   true
                    },
                    trigram: {
                      word_similarity: true
                    }
                  }

  accepts_nested_attributes_for :recipe_ingredients,  allow_destroy: true
  accepts_nested_attributes_for :recipe_instructions, allow_destroy: true
  accepts_nested_attributes_for :categories,          allow_destroy: true
  accepts_nested_attributes_for :cuisines,            allow_destroy: true
  accepts_nested_attributes_for :recipe_categories
  accepts_nested_attributes_for :recipe_cuisines

  enum difficulty: %i[very_easy easy medium hard very_hard]

  scope :imported,   -> { where(imported: true) }
  scope :searchable, lambda { |**args|
    Containers::DependencyContainer['queries.recipes.searchable']
      .(
        user:       args[:user],
        bucket:     args[:bucket],
        query:      args[:query],
        options:    args[:options],
        eager_load: args[:eager_load]
      )
  }

  def total_cooking
    return time_calculator.(array: [total_time], options: { format: :short }) if total_time.any?

    time_calculator.(array: [prep_time, cook_time], options: { format: :short })
  end

  def total_prep_cooking
    time_calculator.(array: [prep_time], options: { format: :short })
  end

  def total_cook_cooking
    time_calculator.(array: [cook_time], options: { format: :short })
  end

  def import_attributes
    {
      imported:    imported,
      external:    external,
      id:          id,
      bucket_slug: bucket.slug
    }
  end

  private

  def time_calculator
    Containers::DependencyContainer['services.recipes.time_calculator']
  end
end
