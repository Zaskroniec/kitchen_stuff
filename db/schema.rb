# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_05_194842) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_trgm"
  enable_extension "pgcrypto"
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "buckets", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "slug", null: false
    t.uuid "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "recipes_count", default: 0, null: false
    t.index ["name", "user_id"], name: "index_buckets_on_name_and_user_id", unique: true
    t.index ["slug", "user_id"], name: "index_buckets_on_slug_and_user_id", unique: true
    t.index ["user_id"], name: "index_buckets_on_user_id"
  end

  create_table "categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.boolean "public", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_categories_on_name", unique: true
    t.index ["public"], name: "index_categories_on_public_categories", where: "(public IS TRUE)"
  end

  create_table "contacts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "subject", null: false
    t.string "email"
    t.text "content", null: false
    t.uuid "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["subject"], name: "index_contacts_on_subject"
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "cuisines", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.boolean "public", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_cuisines_on_name", unique: true
    t.index ["public"], name: "index_cuisines_on_public_cuisines", where: "(public IS TRUE)"
  end

  create_table "recipe_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "recipe_id", null: false
    t.uuid "category_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_recipe_categories_on_category_id"
    t.index ["recipe_id", "category_id"], name: "index_recipe_categories_on_recipe_id_and_category_id", unique: true
    t.index ["recipe_id"], name: "index_recipe_categories_on_recipe_id"
  end

  create_table "recipe_cuisines", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "recipe_id", null: false
    t.uuid "cuisine_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cuisine_id"], name: "index_recipe_cuisines_on_cuisine_id"
    t.index ["recipe_id", "cuisine_id"], name: "index_recipe_cuisines_on_recipe_id_and_cuisine_id", unique: true
    t.index ["recipe_id"], name: "index_recipe_cuisines_on_recipe_id"
  end

  create_table "recipe_imports", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "result", default: 0
    t.jsonb "imports", default: []
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "recipe_ingredients", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.uuid "recipe_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_recipe_ingredients_on_name"
    t.index ["recipe_id"], name: "index_recipe_ingredients_on_recipe_id"
  end

  create_table "recipe_instructions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "content", null: false
    t.uuid "recipe_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["recipe_id"], name: "index_recipe_instructions_on_recipe_id"
  end

  create_table "recipes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.text "image_data"
    t.text "description"
    t.string "recipe_url"
    t.string "servings"
    t.jsonb "nutrition", default: {}, null: false
    t.jsonb "prep_time", default: {"hours"=>nil, "minutes"=>nil}, null: false
    t.jsonb "cook_time", default: {"hours"=>nil, "minutes"=>nil}, null: false
    t.jsonb "total_time", default: {"hours"=>nil, "minutes"=>nil}, null: false
    t.string "keywords"
    t.boolean "imported", default: false, null: false
    t.boolean "external", default: false, null: false
    t.uuid "bucket_id", null: false
    t.uuid "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "recipe_ingredients_count", default: 0, null: false
    t.jsonb "import_errors", default: {}
    t.integer "difficulty", default: 2, null: false
    t.index ["bucket_id"], name: "index_recipes_on_bucket_id"
    t.index ["name"], name: "index_recipes_on_name"
    t.index ["recipe_url", "bucket_id"], name: "index_recipes_on_recipe_url_and_bucket_id", unique: true, where: "(external IS TRUE)"
    t.index ["user_id"], name: "index_recipes_on_user_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", null: false
    t.string "encrypted_password", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "admin", default: false, null: false
    t.string "nick", null: false
    t.string "confirmation_token"
    t.boolean "confirmed", default: false, null: false
    t.string "reset_token"
    t.datetime "reset_token_lifetime"
    t.datetime "reset_token_at"
    t.index ["admin"], name: "index_users_on_admins", where: "(admin IS TRUE)"
    t.index ["admin"], name: "index_users_on_non_admins", where: "(admin IS FALSE)"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["confirmed"], name: "index_users_on_confirmed_users", where: "(confirmed IS TRUE)"
    t.index ["confirmed"], name: "index_users_on_unconfirmed_users", where: "(confirmed IS FALSE)"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["nick"], name: "index_users_on_nick", unique: true
    t.index ["reset_token"], name: "index_users_on_reset_token", unique: true
  end

  add_foreign_key "buckets", "users"
  add_foreign_key "contacts", "users"
  add_foreign_key "recipe_categories", "categories"
  add_foreign_key "recipe_categories", "recipes"
  add_foreign_key "recipe_cuisines", "cuisines"
  add_foreign_key "recipe_cuisines", "recipes"
  add_foreign_key "recipe_ingredients", "recipes"
  add_foreign_key "recipe_instructions", "recipes"
  add_foreign_key "recipes", "buckets"
  add_foreign_key "recipes", "users"
end
