# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  scope :newest, -> { order(updated_at: :desc) }
end
