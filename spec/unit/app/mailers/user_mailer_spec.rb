# frozen_string_literal: true

require 'rails_helper'

describe UserMailer, type: :mailer do
  describe '#confirmation_email' do
    let(:user) { double(:user, email: Faker::Internet.email, confirmation_token: 'tes1234') }
    subject { described_class.with(user: user).confirmation_email }

    it 'renders valid subject' do
      expect(subject.subject).to eq('Account confirmation.')
    end

    it 'renders valid recipient' do
      expect(subject.to).to eq([user.email])
    end

    it 'renders valid sender' do
      expect(subject.from).to eq(%w[no-reply@kitchen-stuff.com])
    end

    it 'renders body' do
      expect(subject.body.encoded).to include(user.confirmation_token)
    end
  end

  describe '#reset_password_email' do
    let(:user) { double(:user, email: Faker::Internet.email, reset_token: random_token) }
    subject { described_class.with(user: user).reset_password_email }

    it 'renders valid subject' do
      expect(subject.subject).to eq('Reset password.')
    end

    it 'renders valid recipient' do
      expect(subject.to).to eq([user.email])
    end

    it 'renders valid sender' do
      expect(subject.from).to eq(%w[no-reply@kitchen-stuff.com])
    end

    it 'renders body' do
      expect(subject.body.encoded).to include(user.reset_token)
    end
  end
end
