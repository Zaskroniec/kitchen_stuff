# frozen_string_literal: true

class ErrorStruct < Dry::Struct
  attribute :message, Types::Nominal::Any
  attribute :backtrace, Types::Nominal::String
end
