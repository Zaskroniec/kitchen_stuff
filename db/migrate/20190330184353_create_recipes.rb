# frozen_string_literal: true

class CreateRecipes < ActiveRecord::Migration[6.0]
  def change
    create_table :recipes, id: :uuid do |t|
      t.string     :name, null: false
      t.text       :image_data
      t.text       :description
      t.string     :recipe_url
      t.string     :servings
      t.jsonb      :nutrition,   default: {},                           null: false
      t.jsonb      :prep_time,   default: { hours: nil, minutes: nil }, null: false
      t.jsonb      :cook_time,   default: { hours: nil, minutes: nil }, null: false
      t.jsonb      :total_time,  default: { hours: nil, minutes: nil }, null: false
      t.string     :keywords
      t.boolean    :imported,    default: false,                        null: false
      t.boolean    :external,    default: false,                        null: false
      t.belongs_to :bucket,      type: :uuid, foreign_key: true,        null: false
      t.belongs_to :user,        type: :uuid, foreign_key: true,        null: false

      t.timestamps
    end

    add_index :recipes, %i[recipe_url bucket_id], unique: true, where: '(external IS TRUE)'
    add_index :recipes, :name
  end
end
