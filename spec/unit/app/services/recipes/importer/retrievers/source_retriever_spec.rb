# frozen_string_literal: true

require 'rails_helper'

module Recipes
  module Importer
    module Retrievers
      describe SourceRetriever do
        describe '#self.call' do
          subject { described_class.(url: default_recipe_url) }

          context 'success' do
            let(:request_args) do
              {
                method:    :get,
                url:       default_recipe_url,
                options:   {
                  headers: { 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'\
                                             ' (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' }
                },
                resp_code: 200,
                body:      read_example('example_import_html.txt')
              }
            end

            before { mock_request_with_result(request_args) }

            it 'returns parsed file with oga structure' do
              expect(subject.doctype.name).to eq('html')
            end
          end

          context 'failure' do
            context 'timeout' do
              let(:request_args) do
                {
                  method:  :get,
                  url:     default_recipe_url,
                  options: {
                    headers: { 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'\
                                               ' (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' }
                  }
                }
              end

              before { mock_request_with_timeout(request_args) }

              it 'raises error' do
                expect { subject }.to raise_error(
                  Error::ProcessError,
                  'We are sorry, but external service is currently unavailable.'
                )
              end
            end
          end

          context 'failure status' do
            let(:request_args) do
              {
                method:    :get,
                url:       default_recipe_url,
                options:   {
                  headers: { 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'\
                                             ' (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' }
                },
                resp_code: 500,
                body:      ''
              }
            end

            before { mock_request_with_result(request_args) }

            it 'raises error' do
              expect { subject }.to raise_error(
                Error::ProcessError,
                'We are sorry, but external service is currently unavailable.'
              )
            end
          end

          context 'invalid type of content' do
            let(:request_args) do
              {
                method:    :get,
                url:       default_recipe_url,
                options:   {
                  headers: { 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'\
                                             ' (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' }
                },
                resp_code: 200,
                body:      file_body_example('pasta.jpg')
              }
            end

            before { mock_request_with_result(request_args) }

            it 'raises error' do
              expect { subject }.to raise_error(
                Error::ProcessError,
                'Invalid type of document to parse.'
              )
            end
          end
        end
      end
    end
  end
end
