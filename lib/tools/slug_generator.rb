# frozen_string_literal: true

module Tools
  class SlugGenerator
    include DependencyImporter['services.translations']

    def call(string:)
      string
        .then(&method(:validate_presence_of_string))
        .then(&:downcase)
        .then(&method(:split_by_space))
        .then(&method(:snake_case_join))
        .then(&:parameterize)
    end

    def self.call(**args)
      new.(**args)
    end

    private

    def validate_presence_of_string(str)
      str.tap { |s| raise Error::ProcessError, translations.(%i[exceptions blank_slug]) if s.blank? }
    end

    def split_by_space(str)
      str.split(' ')
    end

    def snake_case_join(str)
      str.join('_')
    end
  end
end
