# frozen_string_literal: true

module Buckets
  class BaseOperation
    include DependencyImporter[
      'validators.buckets.bucket_validator',
      'repositories.bucket_repo',
      'services.translations',
      'tools.slug_generator',
      'tools.error_message'
    ]

    def call
      raise 'Abstract method'
    end

    private

    def validate_params(params)
      bucket_validator.(params).tap do |validator|
        raise Error::ProcessError, { errors: validator.errors.to_h }.to_json if validator.failure?
      end.to_h
    end

    def prepare_data(output)
      output.tap do |hash|
        hash.delete(:user_id)
        hash.delete(:id)
      end
    end

    def generate_slug(output)
      output.tap do |raw_output|
        raw_output[:slug] = slug_generator.(string: output[:name])
      end
    end

    def bucket_operation
      raise 'Abstract method'
    end

    def result
      raise 'Abstract method'
    end
  end
end
