# frozen_string_literal: true

module Users
  class ConfirmationJob < ApplicationJob
    queue_as :mailers

    def perform(user_id:)
      confirmation_generator.(user_id: user_id)
    end

    private

    def confirmation_generator
      @confirmation_generator ||= dependency_container['services.users.generate_confirmation_instructions']
    end
  end
end
