# frozen_string_literal: true

class CreateContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :contacts, id: :uuid do |t|
      t.integer    :subject, null: false
      t.string     :email
      t.text       :content, null: false
      t.belongs_to :user, type: :uuid, foreign_key: true, index: true

      t.timestamps
    end

    add_index :contacts, :subject
  end
end
