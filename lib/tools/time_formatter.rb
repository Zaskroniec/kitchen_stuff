# frozen_string_literal: true

module Tools
  class TimeFormatter
    include DependencyImporter['libs.duration']

    def full_time(value:)
      value.strftime('%H:%M %d/%m/%Y')
    end

    def short(value:)
      value
        .then(&method(:build_time_hash))
        .then(&method(:build_string))
    end

    private

    def build_time_hash(value)
      value
        .then(&duration.method(:build))
        .then(&:parts)
    end

    def build_string(hash)
      return Constants::DEFAULT_TIME_STRING if hash.blank?

      "#{hash[:hours] || 0}:#{hash[:minutes] || 0}"
    end
  end
end
