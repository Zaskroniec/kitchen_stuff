# frozen_string_literal: true

class Translations
  include DependencyImporter['libs.i18n']

  # TODO: change to argument keys
  def call(keys, options = {})
    i18n.t(keys.join('.'), options)
  end

  def self.call(*keys)
    new.(*keys)
  end
end
