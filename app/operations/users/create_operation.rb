# frozen_string_literal: true

module Users
  class CreateOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
    include DependencyImporter[
      'validators.users.new_user_validator',
      'repositories.user_repo',
      'services.translations',
      'jobs.users.confirmation_job',
      'jobs.buckets.start_buckets_job',
      'tools.error_message',
      'tools.captcha_client',
      'libs.bcrypt'
    ]

    CREATE_ATTRS = %i[password password_confirmation terms].freeze

    def call(params:)
      validate_params(params)
        .then(&method(:generate_encrypted_password))
        .then(&method(:verify_captcha))
        .then(&user_repo.method(:create))
        .then(&method(:schedule_confirmation_instructions))
        .then(&method(:prepare_starting_buckets))
        .then(&method(:result))
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def validate_params(params)
      new_user_validator.(params).tap do |validator|
        raise Error::ProcessError, { errors: validator.errors.to_h }.to_json if validator.failure?
      end.to_h
    end

    def generate_encrypted_password(output)
      output.tap do |o|
        o[:encrypted_password] = bcrypt::Password.create(o[:password])
        CREATE_ATTRS.each { |attr| o.delete(attr) }
      end
    end

    def verify_captcha(output)
      output.tap { |origin_output| captcha_client.(token: origin_output.delete(:'g-recaptcha-response')) }
    end

    def schedule_confirmation_instructions(user)
      user.tap { confirmation_job.perform_later(user_id: user.id) }
    end

    def prepare_starting_buckets(user)
      user.tap { start_buckets_job.perform_later(user_id: user.id) }
    end

    def result(_param)
      Success(notice: translations.(%i[flash signup]))
    end
  end
end
