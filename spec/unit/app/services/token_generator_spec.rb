# frozen_string_literal: true

require 'rails_helper'

describe TokenGenerator do
  describe '#self.call' do
    let(:repo)      { double(:user_repo) }
    let(:attribute) { 'confirmation_token' }
    subject         { described_class.(repo: repo, attribute: attribute) }

    context 'executes service one time' do
      before do
        expect(repo)
          .to receive(:find_by)
          .and_return([])
      end

      it 'returns valid token' do
        expect(subject.length).to eq(192)
      end
    end

    context 'executes service twice' do
      let(:user) { double(:user) }

      before do
        expect(repo)
          .to receive(:find_by)
          .and_return([user])

        expect(repo)
          .to receive(:find_by)
          .and_return([])
      end

      it 'returns valid token' do
        expect(subject.length).to eq(192)
      end
    end
  end
end
