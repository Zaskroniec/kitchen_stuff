# frozen_string_literal: true

module Containers
  module Namespaces
    Operations = Dry::Container::Namespace.new('operations') do
      namespace(:users) do
        register(:create_operation)                 { Users::CreateOperation.new }
        register(:confirmation_operation)           { Users::ConfirmationOperation.new }
        register(:reset_password_operation)         { Users::ResetPasswordOperation.new }
        register(:update_password_operation)        { Users::UpdatePasswordOperation.new }
        register(:inline_update_password_operation) { Users::InlineUpdatePasswordOperation.new }
      end

      namespace(:buckets) do
        register(:create_operation)  { Buckets::CreateOperation.new }
        register(:update_operation)  { Buckets::UpdateOperation.new }
        register(:destroy_operation) { Buckets::DestroyOperation.new }
      end

      namespace(:recipes) do
        register(:import_operation)       { Recipes::ImportOperation.new }
        register(:create_operation)       { Recipes::CreateOperation.new }
        register(:update_operation)       { Recipes::UpdateOperation.new }
        register(:destroy_operation)      { Recipes::DestroyOperation.new }
        register(:download_pdf_operation) { Recipes::DownloadPdfOperation.new }
      end

      namespace(:contacts) do
        register(:create_operation) { Contacts::CreateOperation.new }
      end
    end
  end
end
