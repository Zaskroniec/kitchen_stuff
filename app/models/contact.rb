# frozen_string_literal: true

class Contact < ApplicationRecord
  belongs_to :user, optional: true

  enum subject: %i[account payments improvement other]
end
