# frozen_string_literal: true

class RecipeCuisine < ApplicationRecord
  belongs_to :recipe
  belongs_to :cuisine
end
