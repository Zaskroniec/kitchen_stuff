# frozen_string_literal: true

module API
  module V1
    class SearchController < ApplicationController
      before_action :redirect_to_new_sesseion
      before_action :validate_query, only: %i[categories cuisines]

      def categories
        render json: { data: categories_query }, status: :ok
      end

      def cuisines
        render json: { data: cuisines_query }, status: :ok
      end

      private

      # rubocop:disable Style/GuardClause
      def validate_query
        if unsafe_params[:search].blank?
          return render json: { message: t('exceptions.empty_params') }, status: :unprocessable_entity
        end

        if unsafe_params[:search].length < 3
          return render json: { message: t('exceptions.invalid_params') }, status: :unprocessable_entity
        end
      end
      # rubocop:enable Style/GuardClause

      def categories_query
        Category.published(params[:search]).first(Constants::DEFAULT_PAGINATION_SIZE)
      end

      def cuisines_query
        Cuisine.published(params[:search]).first(Constants::DEFAULT_PAGINATION_SIZE)
      end
    end
  end
end
