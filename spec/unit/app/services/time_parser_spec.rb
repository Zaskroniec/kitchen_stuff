# frozen_string_literal: true

require 'rails_helper'

describe TimeParser do
  describe '#parse_recipe_time' do
    subject { described_class.new.parse_recipe_time(value: value) }

    context 'nil value' do
      let(:value) { nil }

      it 'returns hash with hours' do
        expect(subject[:hours]).to be_nil
      end

      it 'returns hash with empty minutes' do
        expect(subject[:minutes]).to be_nil
      end
    end

    context 'only hours' do
      let(:value) { 'PT2H' }

      it 'returns hash with hours' do
        expect(subject[:hours]).to eq('2H')
      end

      it 'returns hash with empty minutes' do
        expect(subject[:minutes]).to be_nil
      end
    end

    context 'only minutes' do
    end

    context 'uppercase' do
      let(:value) { 'PT50M' }

      it 'returns hash with empty hours' do
        expect(subject[:hours]).to be_nil
      end

      it 'returns hash with minutes' do
        expect(subject[:minutes]).to eq('50M')
      end
    end

    context 'lowercase' do
      let(:value) { 'pt2h50m' }

      it 'returns hash with hours' do
        expect(subject[:hours]).to eq('2H')
      end

      it 'returns hash with minutes' do
        expect(subject[:minutes]).to eq('50M')
      end
    end
  end
end
