# frozen_string_literal: true

class Credentials
  ENVIRONMENTS = %i[development test staging production].freeze

  class << self
    def env
      Rails.env.to_sym
    end

    def [](*keys)
      keys = ENVIRONMENTS.include?(keys.first) ? keys : keys.unshift(Rails.env.to_sym)
      credentials = Rails.application.credentials.dig(*keys)
      raise ArgumentError, "Credentials invalid: #{keys}" if credentials.nil?

      credentials
    end
  end
end
