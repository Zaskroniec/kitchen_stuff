# frozen_string_literal: true

module Containers
  module Namespaces
    Jobs = Dry::Container::Namespace.new('jobs') do
      namespace(:users) do
        register(:confirmation_job)   { Users::ConfirmationJob }
        register(:reset_password_job) { Users::ResetPasswordJob }
      end

      namespace(:buckets) do
        register(:start_buckets_job) { Buckets::StartBucketsJob }
      end

      namespace(:recipes) do
        register(:importer_job) { Recipes::ImporterJob }
      end
    end
  end
end
