# frozen_string_literal: true

module Recipes
  module Importer
    module Retrievers
      class BaseRetriever
        include DependencyImporter[
          'services.time_parser',
          'services.translations',
          'libs.json'
        ]

        def call(raw_data:); end

        def self.call(**args)
          new.(**args)
        end

        private

        def parse_time(string)
          string.then { |str| time_parser.parse_recipe_time(value: str) }
        end

        def build_data(object)
          data = {}

          Constants::IMPORT_KEYS.each do |key|
            data[key.to_s.underscore.to_sym] = __send__(:"get_#{key}", object)
          end

          data
        end

        def get_name(hash); end

        def get_image(hash); end

        def get_description(hash); end

        def get_keywords(hash); end

        def get_nutrition(hash); end

        # rubocop:disable Naming/MethodName
        def get_prepTime(hash); end

        def get_cookTime(hash); end

        def get_totalTime(hash); end

        def get_recipeYield(hash); end

        def get_recipeIngredient(hash) end

        def get_recipeCuisine(hash); end

        def get_recipeCategory(hash); end

        def get_recipeInstructions(hash); end
        # rubocop:enable Naming/MethodName
      end
    end
  end
end
