# frozen_string_literal: true

module Containers
  module Namespaces
    Structs = Dry::Container::Namespace.new('structs') do
      register(:error_struct) { ErrorStruct }
      register(:recipe_time_struct) { RecipeTimeStruct }
    end
  end
end
