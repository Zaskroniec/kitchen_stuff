# frozen_string_literal: true

module Containers
  module Namespaces
    Mailers = Dry::Container::Namespace.new('mailers') do
      register(:user_mailer) { UserMailer }
    end
  end
end
