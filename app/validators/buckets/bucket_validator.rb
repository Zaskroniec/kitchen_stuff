# frozen_string_literal: true

module Buckets
  class BucketValidator < ApplicationValidator
    include ValidatorExtenders::BasicExtender

    option :bucket_repo

    params do
      required(:name).value(:string, size?: 2..30, format?: Constants::BUCKET_NAME_FORMAT)
      required(:user_id).value(:string)
      optional(:id).value(:string, :filled?)
    end

    rule(:name) do
      unless unique_field?(bucket_repo, { name: value, user_id: values.data[:user_id] }, values.data[:id])
        key.failure(translations.(%i[errors unique_field?]))
      end
    end
  end
end
