# Features

#### ***<span style="color: #77B592;">[&larr; Go to Guide page](./guide.md)</span>***

List of current features and future:


* [Recipe manager](#manager)
* [Recipe importer](#importer)
* [Exporter](#exporter)
* [Social profle](#social)
* [Cookbook (Future)](#cookbook)
* [Diet calendar (Future)](#diet)


## [Recipe manager](#manager)

It's the heart of application where you can collect your tastes by creating your own  
recipes or use already checked position from your favorites websites using internal tool from ***<span style="color: #F44336;">KS</span>***    
to downalod and save. Each recipe assigned to user account is editable, but only recipes with your  
authorship are shareable betweem other users or visible in global search.

---

## [Recipe importer](#importer)

Tool for extracting chunk od data from external websites based on schema from `https://schema.org`  
for recipes. Gathered data is converted into ***<span style="color: #F44336;">KS</span>*** structure and saved into database.  
Imported recipes are not shareable, but visible on public user profile.

---

## [Recipe exporter](#exporter)

Quick and easy soluton to export whole collection of recipes to CSV file or single recipe into PDf file if  
you want to i.e share your doscovery or invention with people, who are not registered in ***<span style="color: #F44336;">KS</span>***.

---

## [Social profile](#social)

To do...

---

## [Cookbook](Future)](#cookbook)

To do...

---

## [Diet calendar (Future)](#diet)

To do...

---
