# Business Plan

#### ***<span style="color: #77B592;">[&larr; Go to Guide page](./guide.md)</span>***
### ***<span style="color: #77B592;">My mobile cookbook</span>***

* [Core Questions](#questions)
  * [What is the Kitchen Stuff?](#idea)
  * [Which problem Kitchen Stuff solves?](#problem)
  * [What are the benefits from using Kitchen Stuff](#benefits)
  * [Who is a target?](#target)
* [Marketing](#marketing)
* [Promotions](#promo)
* [Avertising](#advert)
* [Technology](#tech)


## [Core Questions](#questions)

### [What is the Kitchen Stuff?](#idea)

Web application with full responsive view which serves as creator and stockeroom for cooking recipes his authorship or via internal tool which allows importing already created cooking recipes from external web services.

### [Which problem Kitchen Stuff solves?](#problem)

The most important problem which solves Kitchen Stuff is possibility to store every cooking recipes in one place, which are created by the application users or by importing cooking recipes from favorited web sites.

### [What are the benefits from using Kitchen Stuff](#benefits)

1. Very easy in usage interface, which most of all ***<span style="color: #F44336;">KS</span>*** view is dedicated for devices with small screen (mobile and tablet).
1. Not complicated forms in recipe creation and fast imports from external sites.
1. Storage your collections is various types of categories to easly find them later.
1. Generate PDF or other formats files to quickly share them with your loved ones or community.


### [Who is a target?](#target)

For every fan of cooking or even a person, who can at least once in awhile prepare something to eat.

## [Marketing](#marketing)

*To do...*

## [Promotions](#promo)

*To do...*

## [Avertising](#advert)

*To do...*

## [Technology](#tech)

Backend application is built in ***Ruby & Ruby on Rails*** framework latest stable version. Background operations uses  
***Redis***, more advanced search on social site is supported by ***Elastic search***. Frontend is precompiled by webpack with  
small pack of JS with custom ***React*** components.
