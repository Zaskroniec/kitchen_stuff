# frozen_string_literal: true

require 'rails_helper'

describe Category, type: :model do
  it { is_expected.to have_many(:recipe_categories).dependent(:destroy) }

  it { is_expected.to have_db_index(:name).unique(true) }
  it { is_expected.to have_db_index(:public) }

  it { is_expected.to have_db_column(:name).with_options(null: false) }
  it { is_expected.to have_db_column(:public).with_options(default: false, null: false) }
end
