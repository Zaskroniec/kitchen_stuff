# frozen_string_literal: true

require 'rails_helper'

module Users
  describe InlineUpdatePasswordOperation do
    describe '#call' do
      let(:password)     { Faker::Internet.password }
      let(:new_password) { Faker::Internet.password }
      let(:user)         { double(:user, encrypted_password: BCrypt::Password.create(password)) }
      let(:params) do
        {
          current_password:      password,
          password:              new_password,
          password_confirmation: new_password
        }
      end

      subject { described_class.new.(params: params, user: user) }

      context 'success' do
        before { expect(user).to receive(:update).and_return(true) }

        it 'retuns notice message' do
          expect(subject.success[:notice]).to eq('Successfuly updated password.')
        end
      end

      context 'failure' do
        context 'validation' do
          context 'empty params' do
            let(:params) { {} }

            it 'returns errors' do
              expect(subject.failure[:message]).to be_present
            end
          end
        end

        context 'invalid current password' do
          before { params[:current_password] = 'test1234' }

          it 'returns invalid current_password error' do
            expect(subject.failure[:message]).to eq('Invalid current password.')
          end
        end
      end
    end
  end
end
