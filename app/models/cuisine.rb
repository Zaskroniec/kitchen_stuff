# frozen_string_literal: true

class Cuisine < ApplicationRecord
  include PgSearch::Model
  include TagsConcern

  has_many :recipe_cuisines, dependent: :destroy
end
