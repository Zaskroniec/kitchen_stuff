# frozen_string_literal: true

module Recipes
  class SearchableQuery
    include DependencyImporter['repositories.recipe_repo']

    def call(user: nil, bucket: nil, query: nil, eager_load: {}, options: {})
      recipe_repo
        .then { |repo|       filter_user(repo, user) }
        .then { |collection| filter_bucket(collection, bucket) }
        .then { |collection| filter_options(collection, options) }
        .then { |collection| filter_query(collection, query) }
        .then { |collection| eager_load(collection, eager_load) }
        .newest
    end

    private

    def filter_user(collection, user)
      return collection if user.blank?

      collection.where(user_id: user.id)
    end

    def filter_bucket(collection, bucket)
      return collection if bucket.blank?

      collection.where(bucket_id: bucket.id)
    end

    def filter_options(collection, options)
      return collection if options.blank?

      collection.where(options)
    end

    def filter_query(collection, query)
      return collection if query.blank?

      collection.search_for_name(query)
    end

    def eager_load(collection, eager_load)
      return collection if eager_load.blank?

      collection.eager_load(*eager_load)
    end
  end
end
