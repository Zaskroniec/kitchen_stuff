# frozen_string_literal: true

module Recipes
  class CreateOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
    include DependencyImporter[
      'validators.recipes.recipe_validator',
      'services.translations',
      'services.tag_generator',
      'structs.error_struct',
      'tools.params_mapper',
      'tools.error_message'
    ]

    def call(params:, bucket:)
      mapped_params = params_mapper.(params: params)
      validator     = recipe_validator.(mapped_params)

      return validation_error(validator) if validator.failure?

      prepare_output(validator.to_h)
        .then { |data| create_recipe(data, bucket) }
        .then(&method(:result))
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def validation_error(validator)
      Failure(
        error_struct.new(
          message:   {
            errors: validator.errors.to_h,
            params: validator.to_h
          },
          backtrace: Constants::DEFAULT_BACKTRACE
        )
      )
    end

    def prepare_output(output)
      output.tap { |hash| hash.delete(:bucket_id) }
    end

    def prepare_data(data, bucket) # rubocop:disable Metrics/MethodLength
      data.tap do
        categories_collection = tag_generator.(
          hash:   data.slice(:categories_attributes),
          target: :categories_attributes,
          result: :recipe_categories_attributes
        )
        cuisines_collection = tag_generator.(
          hash:   data.slice(:cuisines_attributes),
          target: :cuisines_attributes,
          result: :recipe_cuisines_attributes
        )
        data[:user_id] = bucket.user_id

        data.merge!(*categories_collection, *cuisines_collection)
      end
    end

    def create_recipe(data, bucket)
      ActiveRecord::Base.transaction do
        prepare_data(data, bucket)
        bucket.recipes.create(data)
      end
    end

    def result(record)
      Success(
        record: record,
        notice: translations.(%i[flash create_recipe])
      )
    end
  end
end
