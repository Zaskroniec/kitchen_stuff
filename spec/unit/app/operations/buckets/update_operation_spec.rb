# frozen_string_literal: true

require 'rails_helper'

module Buckets
  describe UpdateOperation do
    describe '#call' do
      let(:bucket_repo)      { double(:bucket_repo) }
      let(:user)             { double(:user, id: random_uuid) }
      let(:bucket)           { double(:bucket) }
      let(:bucket_validator) { Buckets::BucketValidator.new(bucket_repo: bucket_repo) }

      subject do
        described_class
          .new(bucket_repo: bucket_repo, bucket_validator: bucket_validator)
          .(params: params, bucket: bucket)
      end

      context 'success' do
        let(:params) { { name: 'Test String', user_id: user.id } }
        let(:bucket) do
          double(:bucket,
                 name:       'Old Name',
                 slug:       'old_name',
                 created_at: time_now,
                 updated_at: time_now)
        end
        let(:parsed_data) do
          {
            name: params[:name],
            slug: 'test_string'
          }
        end

        before do
          expect(bucket_repo).to receive(:find_by).with(params).and_return([])
          expect(bucket).to      receive(:update).with(parsed_data).and_return(true)
        end

        it 'returns valid message' do
          expect(subject.success[:notice]).to eq('Successfuly updated bucket.')
        end

        it 'returns valid record' do
          expect(subject.success[:record]).to eq(bucket) # in reality tap returns updated active record instance
        end
      end

      context 'failure' do
        context 'validation' do
          context 'invalid format' do
            let(:params) { { name: '$krem', user_id: user.id } }

            it 'returns valid errors' do
              expect(subject.failure[:message][:errors]).to eq(name: ['is in invalid format'])
            end
          end

          context 'invalid size' do
            let(:params) { { name: 'm', user_id: user.id } }

            it 'returns valid errors' do
              expect(subject.failure[:message][:errors]).to eq(name: ['length must be within 2 - 30'])
            end
          end

          context 'not unique' do
            let(:params) { { name: 'Test Name', user_id: user.id } }

            before { expect(bucket_repo).to receive(:find_by).with(params).and_return(bucket) }

            it 'returns valid errors' do
              expect(subject.failure[:message][:errors]).to eq(name: ['is not unique'])
            end
          end
        end
      end
    end
  end
end
