# frozen_string_literal: true

require 'rails_helper'

module Recipes
  module Importer
    describe Updater do
      describe '#call' do
        let(:recipe_id)   { random_uuid }
        let(:recipe_repo) { double(:recipe_repo) }
        subject           { described_class.new(recipe_repo: recipe_repo).(recipe_id: recipe_id) }

        context 'success' do
          let(:recipe) do
            double(:recipe,
                   id:         recipe_id,
                   recipe_url: default_recipe_url,
                   imported:   false,
                   external:   true)
          end
          let(:mapped_data) { example_mapped_data }
          let(:request_args) do
            {
              method:    :get,
              url:       recipe.recipe_url,
              options:   {
                headers: { 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'\
                                           ' (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' }
              },
              resp_code: 200,
              body:      read_example('example_import_html.txt')
            }
          end

          before do
            expect(recipe_repo).to receive(:find).with(recipe_id).and_return(recipe)
            expect(recipe).to      receive(:update).with(mapped_data).and_return(nil)

            mock_request_with_result(request_args)
          end

          it 'returns updated recipe' do
            expect(subject.id).to eq(recipe.id)
          end
        end

        context 'failure' do
          context 'recipe does not exists' do
            let(:recipe_name_class) { 'recipe' }

            before do
              expect(recipe_repo).to receive(:find).with(recipe_id).and_raise(ActiveRecord::RecordNotFound)
              expect(recipe_repo).to receive(:name).and_return(recipe_name_class)
            end

            it 'raises valid error' do
              expect { subject }.to raise_error(
                Error::ProcessError,
                "Recipe does not exists for id: #{recipe_id}."
              )
            end
          end

          context 'recipe exists' do
            before { expect(recipe_repo).to receive(:find).with(recipe_id).and_return(recipe) }

            context 'recipe is not external' do
              let(:recipe) do
                double(:recipe,
                       id:       recipe_id,
                       imported: false,
                       external: false)
              end

              it 'raises valid error' do
                expect { subject }.to raise_error(
                  Error::ProcessError,
                  "Recipe with id #{recipe_id} is already imported."
                )
              end
            end

            context 'recipe is imported' do
              let(:recipe) do
                double(:recipe,
                       id:       recipe_id,
                       imported: true,
                       external: true)
              end

              it 'raises valid error' do
                expect { subject }.to raise_error(
                  Error::ProcessError,
                  "Recipe with id #{recipe_id} is already imported."
                )
              end
            end
          end
        end
      end
    end
  end
end
