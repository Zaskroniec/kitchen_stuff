# frozen_string_literal: true

require 'rails_helper'

module Recipes
  describe ImporterJob, type: :job do
    describe '#perform' do
      let(:recipe_id) { random_uuid }
      subject         { described_class.perform_later(recipe_id: recipe_id) }

      it 'enque the job' do
        expect { subject }
          .to have_enqueued_job(described_class)
          .with(recipe_id: recipe_id)
          .on_queue('importer')
      end
    end
  end
end
