# frozen_string_literal: true

class RecipeInstruction < ApplicationRecord
  belongs_to :recipe
end
