# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def confirmation_email
    @user = user

    mail(to: user.email, subject: t('mailers.user_mailer.confirmation_account'))
  end

  def reset_password_email
    @user = user

    mail(to: user.email, subject: t('mailers.user_mailer.reset_password'))
  end

  private

  def user
    params[:user]
  end
end
