# frozen_string_literal: true

class CreateRecipeImports < ActiveRecord::Migration[6.0]
  def change
    create_table :recipe_imports, id: :uuid do |t|
      t.integer :result, default: 0
      t.jsonb   :imports, default: []

      t.timestamps
    end
  end
end
