# frozen_string_literal: true

require 'rails_helper'

module Tools
  describe SlugGenerator do
    describe '#call' do
      subject { described_class.(string: string) }

      context 'success' do
        let(:string) { 'Test String' }

        it 'returns transformed value' do
          expect(subject).to eq('test_string')
        end
      end

      context 'failure' do
        context 'blank call parameter' do
          let(:string) {}

          it 'raises specific error' do
            expect { subject }.to raise_error(
              Error::ProcessError,
              'Could not transform value into slug.'
            )
          end
        end
      end
    end
  end
end
