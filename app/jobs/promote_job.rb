# frozen_string_literal: true

class PromoteJob < ApplicationJob
  queue_as :files

  def perform(data:)
    Shrine::Attacher.promote(data)
  end
end
