# Guide

#### ***<span style="color: #EBEBEB;">[&larr; Go to Readme page](../README.md)</span>***

## Table of contents

1. **[Business Plan](./business_plan.md)**
1. **[Business Model](./business_model.md)**
1. **[Features](./features.md)**
