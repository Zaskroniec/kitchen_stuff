# frozen_string_literal: true

class RecipesController < ApplicationController
  include PaginationConcern
  include DependencyImporter[
    'repositories.recipe_repo',
    'operations.recipes.import_operation',
    'operations.recipes.create_operation',
    'operations.recipes.update_operation',
    'operations.recipes.destroy_operation',
    'operations.recipes.download_pdf_operation',
    'services.static_meta_tags'
  ]

  before_action :redirect_to_new_sesseion

  layout false, only: %i[paginate]

  def my_collection
    newest_recipes
    set_meta_tags(**static_meta_tags.(key: :recipes))
  end

  def paginate
    @pagy, @recipes = paginate_resource(:recipes)
  rescue Error::PageError => _e
    @recipes = []
    render nothing: true, status: :no_content && return
  rescue Pagy::OverflowError => _e
    @recipes = []
    render nothing: true, status: :no_content && return
  end

  def new
    show_breadcrumbs
    new_recipe
    set_meta_tags(title: t('views.labels.new').capitalize!)
  end

  def create
    create_operation.(params: prepare_recipes_params, bucket: bucket) do |service|
      service.success do |result|
        redirect_to bucket_recipe_path(bucket.slug, result[:record]), notice: result[:notice]
      end

      service.failure do |error_object|
        show_breadcrumbs
        new_recipe.attributes = error_object[:message][:params]
        render_error(:new, error_object)
      end
    end
  end

  def show
    show_breadcrumbs
    prepare_metatags
    fresh_when(recipe)
  end

  def edit
    edit_breadcrumbs
    prepare_metatags
  end

  def update
    update_operation.(params: prepare_recipes_params, recipe: recipe) do |service|
      service.success do |result|
        redirect_to bucket_recipe_path(bucket.slug, recipe), notice: result[:notice]
      end

      service.failure do |error_object|
        edit_breadcrumbs
        recipe.attributes = error_object[:message][:params]
        render_error(:edit, error_object)
      end
    end
  end

  def import
    import_operation.(params: prepare_recipes_params, bucket: bucket) do |service|
      service.success do |result|
        redirect_to bucket_recipe_path(bucket.slug, result[:record]), notice: result[:notice]
      end

      service.failure do |error_object|
        import_failure(error_object)
        render template: 'buckets/show'
      end
    end
  end

  def import_status
    render json: { imported: recipe.imported }.to_json, status: :created
  end

  def destroy
    destroy_operation.(recipe: recipe) do |service|
      service.success do |result|
        redirect_to bucket_path(bucket.slug), notice: result[:notice]
      end

      service.failure do |errors|
        redirect_to bucket_path(bucket.slug), error: errors[:message]
      end
    end
  end

  def download_pdf
    download_pdf_operation.(recipe: recipe) do |service|
      service.success do |result|
        send_data result[:record], filename: "#{recipe.name}.pdf", type: 'application/pdf', disposition: 'attachment'
      end

      service.failure do |errors|
        redirect_to bucket_recipe_path(bucket.slug, recipe), error: errors[:message]
      end
    end
  end

  private

  def prepare_metatags
    set_meta_tags(
      title:       recipe.name,
      description: recipe.description,
      keywords:    recipe.categories.pluck(:name).join(', ')
    )
  end

  def show_breadcrumbs
    add_breadcrumb t('views.labels.my_buckets'), buckets_path
    add_breadcrumb t('views.labels.bucket'), bucket_path(bucket.slug)
    add_breadcrumb t('views.labels.recipe')
  end

  def edit_breadcrumbs
    add_breadcrumb t('views.labels.my_buckets'), buckets_path
    add_breadcrumb t('views.labels.bucket'), bucket_path(bucket.slug)
    add_breadcrumb t('views.labels.recipe'), bucket_recipe_path(bucket.slug, recipe)
    add_breadcrumb t('views.labels.edit')
  end

  def prepare_recipes_params
    (unsafe_params[:recipe] || {}).tap do |hash|
      hash[:bucket_id] = bucket.id
      hash[:id] = recipe.id if params[:id]
    end
  end

  def import_failure(error_object)
    newest_recipes
    new_recipe

    return flash.now[:error] = error_object[:message] if error_object[:message].is_a?(String)

    @import_failure_dialog ||= true
    @errors ||= error_object[:message] if error_object[:message].is_a?(Hash)
  end

  def newest_recipes
    @recipes ||= recipes_scope.limit(Constants::DEFAULT_PAGINATION_SIZE).offset(0)
    fresh_when(@recipes)
  end

  def bucket
    @bucket ||= current_user.buckets.find_by!(slug: params[:bucket_slug] || params[:slug])
  end

  def recipe
    @recipe ||= bucket.recipes.find(params[:id])
  end

  def new_recipe
    @new_recipe ||= Recipe.new
  end
end
