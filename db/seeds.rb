# Create admin
password = BCrypt::Password.create('admin1234')

user = User.create(
  email:              'admin@kitchen-stuff.pl',
  nick:               'Admin',
  encrypted_password: password,
  admin:              true,
  confirmed:          true
)

Constants::DEFAULT_USER_BUCKETS.each do |name|
  user.buckets.create(name: name, slug: name.downcase)
end

['CHICKEN', 'SOUP'].each do |c|
  Category.find_or_create_by(name: c)
end

['POLISH', 'GERMAN'].each do |c|
  Cuisine.find_or_create_by(name: c)
end

categories = Category.pluck(:id)
cuisines = Cuisine.pluck(:id)

dd   = SecureRandom.hex(4)
user = User.create(email: "kitchen-#{dd}@kitchen-stuff.pl", nick: "Kitchen #{dd}", encrypted_password: password, confirmed: true)

50.times do |e|
  p "create bucket: #{e}"
  cc = SecureRandom.hex(4)

  user.buckets.create(name: "Bucket-#{cc}-#{dd}", slug: "bucket-#{cc}-#{dd}")
end

400.times do |r|
  p "create recipe: #{r}"
  rr = SecureRandom.hex(4)

  bucket_id = user.buckets.order('random()').first.id
  user_id   = user.id

  Recipe.create(
    bucket_id: bucket_id,
    user_id: user_id,
    category_ids: categories,
    cuisine_ids: cuisines,
    prep_time: { 'hours' => 0, 'minutes' => 30 },
    cook_time: { 'hours' => 0, 'minutes' => 30 },
    total_time: { 'hours' => 1, 'minutes' => 0 },
    servings: '10 portions',
    name: "Recipe-#{rr}-#{dd}",
    difficulty: 'medium',
    recipe_ingredients_attributes: [{ name: 'test' }, { name: 'test2' }, { name: 'test3' }],
    recipe_instructions_attributes: [{ content: 'test1234' }, { content: 'test5678' }]
  )
end
