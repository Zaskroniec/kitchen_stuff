# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: Constants::DEFAULT_EMAIL_FROM
  layout 'mailers/mailer'

  def call; end
end
