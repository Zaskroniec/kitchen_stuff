# frozen_string_literal: true

module Tools
  class ParamsMapper
    def call(params:)
      params.map do |key, value|
        if key.to_s.include?('_attributes') && value.is_a?(Hash)
          [key, value.map { |_nested_key, nested_value| nested_value }]
        else
          [key, value]
        end
      end.to_h
    end
  end
end
