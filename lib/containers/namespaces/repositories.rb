# frozen_string_literal: true

module Containers
  module Namespaces
    Repositories = Dry::Container::Namespace.new('repositories') do
      register(:user_repo)               { User }
      register(:bucket_repo)             { Bucket }
      register(:recipe_repo)             { Recipe }
      register(:recipe_ingredient_repo)  { RecipeIngredient }
      register(:recipe_instruction_repo) { RecipeInstruction }
      register(:recipe_category_repo)    { RecipeCategory }
      register(:recipe_cuisine_repo)     { RecipeCuisine }
      register(:category_repo)           { Category }
      register(:cuisine_repo)            { Cuisine }
      register(:contact_repo)            { Contact }

      namespace(:admin) do
        register(:recipe_import_repo) { RecipeImport }
      end
    end
  end
end
