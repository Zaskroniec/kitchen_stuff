# frozen_string_literal: true

module Contacts
  class ContactValidator < ApplicationValidator
    params do
      required(:subject).value(:integer, :filled?)
      required(:email).value(:string, :filled?, format?: Constants::EMAIL_FORMAT)
      required(:content).value(:string, :filled?, max_size?: Constants::MAX_STRING_LENGTH)
      optional(:'g-recaptcha-response').value(:string)
    end

    rule(:subject) do
      unless Contact.subjects.value?(value)
        key.failure(translations.(%i[errors invalid_subject]))
      end
    end
  end
end
