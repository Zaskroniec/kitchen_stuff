# frozen_string_literal: true

class TimeParser
  def parse_recipe_time(value:)
    value
      .then { |string| string || '' }
      .then { |string| string.remove(/PT|pt/) }
      .then { |string| string.split(/(\d+[a-zA-Z])/) }
      .then { |array|  array.reject(&:empty?) }
      .then(&method(:time_object))
  end

  private

  def time_object(array)
    {
      hours:   array.detect { |item| item =~ /^\d+H|h/ }.try(:upcase),
      minutes: array.detect { |item| item =~ /^\d+M|m/ }.try(:upcase)
    }
  end
end
