# frozen_string_literal: true

module Admin
  class AdminController < ::ApplicationController
    before_action :check_admin_privileges

    layout 'admin'

    private

    def check_admin_privileges
      redirect_to root_path if non_admin?
    end
  end
end
