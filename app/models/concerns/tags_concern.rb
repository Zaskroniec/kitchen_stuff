# frozen_string_literal: true

module TagsConcern
  extend ActiveSupport::Concern

  included do
    pg_search_scope :search_for_name,
                    against:   :name,
                    ignoring:  :accents,
                    ranked_by: ':trigram',
                    using:     {
                      tsearch: {
                        dictionary: I18n.t("languages.#{I18n.locale}"),
                        any_word:   true
                      },
                      trigram: {
                        word_similarity: true
                      }
                    }

    scope :published, ->(query) { where(public: true).search_for_name(query) }
  end
end
