# frozen_string_literal: true

class CreateCuisines < ActiveRecord::Migration[6.0]
  def change
    create_table :cuisines, id: :uuid do |t|
      t.string  :name,                   null: false
      t.boolean :public, default: false, null: false

      t.timestamps
    end

    add_index :cuisines, :name, unique: true
    add_index :cuisines, :public, name: 'index_cuisines_on_public_cuisines', where: '(public IS TRUE)'
  end
end
