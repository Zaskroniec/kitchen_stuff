# frozen_string_literal: true

class Error < StandardError
  class ProcessError < Error; end
  class InvalidUrlError < Error; end

  class RequestTimeoutError < Error
    def self.===(exception)
      exception.class == Net::OpenTimeout
    end
  end

  class JsonParseError < Error
    def self.===(exception)
      exception.class == JSON::ParserError
    end
  end

  # oga error on invalid document type handler
  class ResponseContentTypeError < Error
    def self.===(exception)
      exception.class == ArgumentError && exception.message =~ /^string contains null byte$/
    end
  end

  class SocketError < Error
    def self.===(exception)
      exception.class == ::SocketError && exception.message =~ /^Failed to open TCP/
    end
  end

  # pagy page handler
  class PageError < Error
    def self.===(exception)
      exception.class == ::ArgumentError && exception.message =~ /^expected :page >= 1; got/
    end
  end

  # pagy overflow handler
  class OverflowError < Error
    def self.===(exception)
      exception.class == ::Pagy::OverflowError && exception.message =~ /^page \d+ got no items/
    end
  end
end
