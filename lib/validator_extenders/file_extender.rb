# frozen_string_literal: true

module ValidatorExtenders
  module FileExtender
    private

    def valid_size?(form_data)
      return match_size?(form_data.size) if form_data.is_a?(ActionDispatch::Http::UploadedFile)
      return match_size?(parse_json(form_data, :size)) if form_data.is_a?(String)

      false
    end

    def valid_extension?(form_data)
      return match_format?(form_data.content_type) if form_data.is_a?(ActionDispatch::Http::UploadedFile)
      return match_format?(parse_json(form_data, :mime_type)) if form_data.is_a?(String)

      false
    end

    def parse_json(json, key)
      JSON
        .parse(json, symbolize_names: true)
        .then { |data| data.dig(:metadata, :"#{key}") }
    rescue Error::JsonParseError
      Constants::EMPTY_STRING
    end

    def match_size?(value)
      value.to_i <= Constants::MAX_REMOTE_FILE
    end

    def match_format?(value)
      return false if value.blank?

      !value.match(%r{^image/#{Constants::FILE_EXTENSIONS.join('|')}$}).nil?
    end
  end
end
