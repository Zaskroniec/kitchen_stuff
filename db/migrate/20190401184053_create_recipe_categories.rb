# frozen_string_literal: true

class CreateRecipeCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :recipe_categories, id: :uuid do |t|
      t.belongs_to :recipe,   type: :uuid, foreign_key: true, null: false
      t.belongs_to :category, type: :uuid, foreign_key: true, null: false

      t.timestamps
    end

    add_index :recipe_categories, %i[recipe_id category_id], unique: true
  end
end
