# frozen_string_literal: true

require 'rails_helper'

module Recipes
  describe DownloadPdfOperation do
    describe '#call' do
      let(:generator) { double(:generator) }
      let(:recipe)    { double(:recipe, name: 'xyz') }
      let(:generator_options) do
        {
          pdf_options:   { title: recipe.name },
          template_args: {
            template: 'recipes/show.pdf.slim',
            locals:   { '@recipe': recipe }
          }
        }
      end

      subject { described_class.new(generator: generator).(recipe: recipe) }

      context 'success' do
        let(:document_body) { '<h1>Test</h1>' }

        before do
          expect(generator).to receive(:call).with(generator_options).and_return(document_body)
        end

        it 'returns valid message' do
          expect(subject.success[:record]).to eq(document_body)
        end
      end

      context 'failure' do
        context 'generator crash' do
          before do
            expect(generator).to receive(:call).with(generator_options).and_raise(RuntimeError)
          end

          it 'returns valid message' do
            expect(subject.failure[:message])
              .to eq('Something went wrong with pdf document.')
          end
        end
      end
    end
  end
end
