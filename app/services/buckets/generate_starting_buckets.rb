# frozen_string_literal: true

module Buckets
  class GenerateStartingBuckets
    include DependencyImporter[
      'repositories.user_repo',
      'operations.buckets.create_operation'
    ]

    def call(user_id:)
      user_id
        .then(&user_repo.method(:find))
        .then(&method(:generate_starting_buckets))
    end

    private

    def generate_starting_buckets(user, ids = [])
      ids.tap do |result|
        Constants::DEFAULT_USER_BUCKETS.each do |bucket_name|
          params = { name: bucket_name, user_id: user.id }

          result << create_operation.(params: params, user: user).success[:record].id
        end
      end
    end
  end
end
