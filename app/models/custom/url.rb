# frozen_string_literal: true

# TODO: it's for import recipes!

module Custom
  class Url < OpenStruct
    extend ActiveModel::Naming

    attr_accessor :name
  end
end
