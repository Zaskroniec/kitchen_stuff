# frozen_string_literal: true

require 'rails_helper'

module Tools
  describe ErrorMessage do
    describe 'self.call' do
      subject { DummyClassError.new.mock }

      it 'returns hash with error class' do
        expect(subject[:class]).to eq(StandardError)
      end

      it 'returns hash with error message' do
        expect(subject[:message]).to eq('test message')
      end

      it 'returns hash with backtrace error' do
        expect(subject[:backtrace]).not_to be_empty
      end
    end
  end
end
