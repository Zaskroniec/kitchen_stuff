import { Controller } from 'stimulus';
import axios from 'axios';

export default class extends Controller {
  initialize() {
    const recipeAttributes = this.data.get('attributes');
    const parsedAttributes = JSON.parse(recipeAttributes);

    if(parsedAttributes['external'] && !parsedAttributes['imported']) {
      this.checkRecipeImport(parsedAttributes['id'], parsedAttributes['bucket_slug'], 0)
    }
  }

  checkRecipeImport(id, slug, counter) {
    const TICK_AMOUNT = 2000;
    const that = this;

    const timeout = setTimeout(() => {
      axios
        .get(`/buckets/${slug}/recipes/${id}/import_status`)
        .then(function(response) {
          counter++;

          if(counter === 20 ) { return; }

          if(response.data['imported']) {
            window.location.reload();
          } else {
            that.checkRecipeImport(id, slug, counter);
          }
        })
        .catch(function(error) {
          console.warn(error);
        });
    }, TICK_AMOUNT)

    if(window.location.pathname !== `/buckets/${slug}/recipes/${id}`) {
      clearTimeout(timeout)
    }
  }
}
