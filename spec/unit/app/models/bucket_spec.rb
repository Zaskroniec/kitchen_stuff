# frozen_string_literal: true

require 'rails_helper'

describe Bucket, type: :model do
  it { is_expected.to belong_to(:user) }

  it { is_expected.to have_many(:recipes).dependent(:destroy) }

  it { is_expected.to have_db_index(%i[name user_id]).unique(true) }
  it { is_expected.to have_db_index(%i[slug user_id]).unique(true) }

  it { is_expected.to have_db_column(:user_id).of_type(:uuid).with_options(null: false) }
  it { is_expected.to have_db_column(:name).of_type(:string).with_options(null: false) }
  it { is_expected.to have_db_column(:slug).of_type(:string).with_options(null: false) }
  it { is_expected.to have_db_column(:recipes_count).of_type(:integer).with_options(default: 0, null: false) }
  it { is_expected.to have_db_column(:created_at).of_type(:datetime).with_options(precision: 6, null: false) }
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime).with_options(precision: 6, null: false) }
end
