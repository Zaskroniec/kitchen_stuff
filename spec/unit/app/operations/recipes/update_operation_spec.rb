# frozen_string_literal: true

require 'rails_helper'

module Recipes
  describe UpdateOperation do
    describe '#call' do
      let(:bucket)        { double(:bucket, id: random_uuid) }
      let(:recipe)        { double(:recipe, id: random_uuid) }
      let(:tag_generator) { double(:tag_generator) }
      let(:recipe_repo)   { double(:recipe_repo) }
      let(:validator)     { Recipes::RecipeValidator.new(recipe_repo: recipe_repo) }
      subject             do
        described_class
          .new(tag_generator: tag_generator, recipe_validator: validator)
          .(params: params, recipe: recipe)
      end

      context 'success' do
        let(:params) do
          {
            name:                           Faker::Lorem.word,
            prep_time:                      { hours: 1, minutes: 30 },
            cook_time:                      { hours: 1, minutes: 30 },
            total_time:                     { hours: 1, minutes: 30 },
            difficulty:                     1,
            keywords:                       Faker::Lorem.word,
            description:                    Faker::Lorem.paragraph(2),
            servings:                       Faker::Number.number(2),
            recipe_ingredients_attributes:  [{ name: Faker::Lorem.word }],
            recipe_instructions_attributes: [{ content: Faker::Lorem.word }],
            categories_attributes:          [{ name: 'pasta', _destroy: 'false' }],
            cuisines_attributes:            [{ name: 'japanese', _destroy: 'false' }],
            bucket_id:                      bucket.id
          }
        end
        let(:category_args) do
          {
            hash:   { categories_attributes: [{ name: 'pasta', _destroy: false }] },
            target: :categories_attributes,
            result: :recipe_categories_attributes
          }
        end
        let(:cuisine_args) do
          {
            hash:   { cuisines_attributes: [{ name: 'japanese', _destroy: false }] },
            target: :cuisines_attributes,
            result: :recipe_cuisines_attributes
          }
        end
        let(:category_result) do
          [{ categories_attributes: [] }, { recipe_categories_attributes: [{ category_id: random_uuid }] }]
        end
        let(:cuisine_result) do
          [{ cuisines_attributes: [] }, { recipe_cuisines_attributes: [{ cuisine_id: random_uuid }] }]
        end

        before do
          expect(recipe_repo)
            .to receive(:find_by).with(name: params[:name], bucket_id: params[:bucket_id]).and_return(nil)
          expect(tag_generator).to receive(:call).with(category_args).and_return(category_result)
          expect(tag_generator).to receive(:call).with(cuisine_args).and_return(cuisine_result)
          expect(recipe)
            .to receive(:update)
            .with(remove_bucket_id(params.merge(*category_result, *cuisine_result))).and_return(recipe)
        end

        it 'creates new recipe' do
          expect(subject.success[:record]).to eq(recipe)
        end

        it 'returns notice message' do
          expect(subject.success[:notice]).to eq('Successfuly updated recipe.')
        end
      end

      context 'failure' do
        let(:params) { {} }

        it 'returns errors' do
          expect(subject.failure[:message]).to be_present
        end
      end
    end
  end
end
