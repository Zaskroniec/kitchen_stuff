# frozen_string_literal: true

class HomesController < ApplicationController
  include DependencyImporter['services.static_meta_tags']

  def show
    set_meta_tags(**static_meta_tags.(key: :home))
  end

  def terms
    set_meta_tags(**static_meta_tags.(key: :terms))
  end

  def privacy
    set_meta_tags(**static_meta_tags.(key: :privacy))
  end

  def company
    set_meta_tags(**static_meta_tags.(key: :company))
  end

  def changelog
    set_meta_tags(**static_meta_tags.(key: :changelog))
  end

  def account_plans
    set_meta_tags(**static_meta_tags.(key: :account_plans))
  end

  def features
    set_meta_tags(**static_meta_tags.(key: :features))
  end
end
