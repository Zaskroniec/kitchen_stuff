# frozen_string_literal: true

class TokenGenerator
  include DependencyImporter['libs.secure_random']

  def self.call(**args)
    new.(**args)
  end

  def call(repo:, attribute:, options: {})
    generate_token(options)
      .then { |token| check_uniqueness_of_token(token, repo, attribute, options) }
  end

  private

  def generate_token(options)
    secure_random.hex(options[:length] || Constants::DEFAULT_TOKEN_HEX)
  end

  def check_uniqueness_of_token(token, repo, attribute, options)
    return token if repo.find_by("#{attribute}": token).blank?

    call(repo: repo, attribute: attribute, options: options)
  end
end
