import { Controller } from 'stimulus';
import axios from 'axios';

const ENTER_KEY = 13;
const SEARCH_LIMIT = 3;
const ENDPOINTS = {
  cuisines: '/v1/search/cuisines',
  categories: '/v1/search/categories'
};

export default class extends Controller {
  static targets = ['suggestionsContainer', 'tagsContainer', 'cancer', 'autocompleteField'];

  submit(event) {
    if(event.keyCode === ENTER_KEY || event.type === 'click') {
      event.preventDefault();

      const childDOM = this.cancerTarget.dataset['nodes'];
      const inputValue = this.autocompleteFieldTarget.value;

      if(inputValue.length === 0) { return; }

      this.addTag(childDOM, inputValue);
      this.cleanAutocompleteField();
      this.cleanSuggestions();
    }
  }

  acceptSuggestion(event) {
    event.preventDefault();

    const childDOM = this.cancerTarget.dataset['nodes'];

    this.addTag(childDOM, event.target.innerHTML);
    this.cleanAutocompleteField();
    this.cleanSuggestions();
  }

  listSuggestions(event) {
    const inputValue = this.autocompleteFieldTarget.value;
    const resource = this.data.get('resource');

    if(inputValue.length >= SEARCH_LIMIT) {
      this.retrieveResults(resource, inputValue);
    } else {
      this.cleanSuggestions();
    }
  }

  retrieveResults(resource, queryValue) {
    axios
      .get(`${ENDPOINTS[resource]}?search=${queryValue}`)
      .then((response) => {
        this.displayResult(response.data['data']);
      })
      .catch((error) => {
        console.warn(error);
      });
  }

  cleanSuggestions() {
    this.suggestionsContainerTarget.innerHTML = '';
  }

  cleanAutocompleteField() {
    this.autocompleteFieldTarget.value = '';
  }

  displayResult(data) {
    this.cleanSuggestions();

    data.forEach((element) => {
      const domElement = document.createElement('li');
      const action = this.data.get('resultAction');

      domElement.classList.add('bold')
      domElement.innerHTML = element['name'];
      domElement.dataset['action'] = 'click->autocomplete#acceptSuggestion';

      this.suggestionsContainerTarget.appendChild(domElement);
    });
  }

  verifyNewTag(value) {
    const item = Array.from(this.tagsContainerTarget.querySelectorAll('li'))
                      .find((element) => {
                        let span = element.querySelector('span');

                        return span.innerHTML === value.toUpperCase();
                      });
    if(item && item.classList.contains('removed')) {
      let inputHidden = item.querySelector('input.delete-button');

      inputHidden.value = false;
      item.classList.remove('removed');

      return true;
    } else if (item) {
      return true;
    } else {
      return false;
    }
  }

  addTag(childDOM, value) {
    if(this.verifyNewTag(value)) {
      return;
    }

    const resource = this.data.get('resource');
    const newElementId = (new Date()).getTime();
    const domParser = new DOMParser();
    const parsedChildData = childDOM.replace(new RegExp(/\d/, 'g'), newElementId);
    const doc = domParser.parseFromString(parsedChildData, 'text/html');
    const nameInput = doc.getElementById(`recipe_${resource}_attributes_${newElementId}_name`);
    const nameSpan = doc.querySelector('span');

    nameInput.value = value.toUpperCase();
    nameSpan.innerHTML = value.toUpperCase();

    this.tagsContainerTarget.appendChild(doc.body.firstChild);
  }
}
