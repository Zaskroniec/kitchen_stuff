# frozen_string_literal: true

require 'rails_helper'

module Tools
  describe TimeFormatter do
    describe '#short' do
      subject { described_class.new.short(value: time_value) }

      context 'hours and minutes present' do
        let(:time_value) { 9000 }

        it 'returns valid string' do
          expect(subject).to eq('2:30')
        end
      end

      context 'hours present' do
        let(:time_value) { 7200 }

        it 'returns valid string' do
          expect(subject).to eq('2:0')
        end
      end

      context 'hours and time not present' do
        let(:time_value) { 0 }

        it 'returns valid string' do
          expect(subject).to eq('Not provided')
        end
      end
    end
  end
end
