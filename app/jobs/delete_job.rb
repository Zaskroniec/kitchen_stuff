# frozen_string_literal: true

class DeleteJob < ApplicationJob
  queue_as :files

  def perform(data:)
    Shrine::Attacher.delete(data)
  end
end
