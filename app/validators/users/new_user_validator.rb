# frozen_string_literal: true

module Users
  class NewUserValidator < ApplicationValidator
    include ValidatorExtenders::BasicExtender

    option :user_repo

    params do
      required(:email).value(:string, format?: Constants::EMAIL_FORMAT, max_size?: Constants::MAX_STRING_LENGTH)
      required(:nick).value(:string, size?: 4..16)
      required(:password).value(:string, size?: 8..32, format?: Constants::PASSWORD_FORMAT)
      required(:password_confirmation).maybe(:string)
      required(:'g-recaptcha-response').value(:string)
      required(:terms).filled(Types::Coercible::Bool, :true?)
    end

    rule(:email) do
      unless unique_field?(user_repo, { email: value }, nil)
        key.failure(translations.(%i[errors unique_field?]))
      end
    end

    rule(:nick) do
      unless unique_field?(user_repo, { nick: value }, nil)
        key.failure(translations.(%i[errors unique_field?]))
      end
    end

    rule(:password_confirmation).validate(:match_passwords?)
  end
end
