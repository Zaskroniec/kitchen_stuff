# frozen_string_literal: true

require 'rails_helper'

module Users
  describe ConfirmationJob, type: :job do
    describe '#perform' do
      let(:user_id) { 'test_id' }
      subject       { described_class.perform_later(user_id: user_id) }

      it 'queue the job' do
        expect { subject }
          .to have_enqueued_job(described_class)
          .with(user_id: user_id)
          .on_queue('mailers')
      end
    end
  end
end
