# frozen_string_literal: true

def mock_request(args)
  stub_request(args[:method], args[:url])
    .with(args[:options])
end

def mock_request_with_result(args)
  mock_request(args)
    .to_return(
      { status: args[:resp_code], body: args[:body] }.merge(headers: args[:headers])
    )
end

def mock_request_with_timeout(args)
  mock_request(args)
    .to_timeout
end

def mock_request_with_error(args)
  mock_request(args)
    .to_raise(args[:error])
end
