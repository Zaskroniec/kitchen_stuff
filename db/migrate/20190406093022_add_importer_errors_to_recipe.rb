# frozen_string_literal: true

class AddImporterErrorsToRecipe < ActiveRecord::Migration[6.0]
  def change
    add_column :recipes, :import_errors, :jsonb, default: {}
  end
end
