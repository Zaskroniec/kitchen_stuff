# frozen_string_literal: true

require 'rails_helper'

describe User, type: :model do
  it { is_expected.to have_many(:buckets) }
  it { is_expected.to have_many(:contacts) }
  it { is_expected.to have_many(:recipes) }

  it { is_expected.to have_db_index(:email).unique(true) }
  it { is_expected.to have_db_index(:nick).unique(true) }
  it { is_expected.to have_db_index(:admin) }
  it { is_expected.to have_db_index(:confirmed) }
  it { is_expected.to have_db_index(:confirmation_token).unique(true) }
  it { is_expected.to have_db_index(:reset_token).unique(true) }

  it { is_expected.to have_db_column(:email).of_type(:string).with_options(null: false) }
  it { is_expected.to have_db_column(:nick).of_type(:string).with_options(null: false) }
  it { is_expected.to have_db_column(:admin).of_type(:boolean).with_options(null: false, default: false) }
  it { is_expected.to have_db_column(:confirmed).of_type(:boolean).with_options(null: false, default: false) }
  it { is_expected.to have_db_column(:encrypted_password).of_type(:string).with_options(null: false) }
  it { is_expected.to have_db_column(:confirmation_token).of_type(:string) }
  it { is_expected.to have_db_column(:reset_token).of_type(:string) }
  it { is_expected.to have_db_column(:reset_token_lifetime).of_type(:datetime) }
  it { is_expected.to have_db_column(:reset_token_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:created_at).of_type(:datetime).with_options(precision: 6, null: false) }
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime).with_options(precision: 6, null: false) }

  describe '#reset_token_expired?' do
    subject { user.reset_token_expired? }

    context 'expired' do
      let(:user) { create(:default_user, reset_token_lifetime: Time.zone.now - 1.day) }

      it 'returns valid value' do
        expect(subject).to be_truthy
      end
    end

    context 'not expired' do
      let(:user) { create(:default_user, reset_token_lifetime: Time.zone.now + 1.day) }

      it 'returns valid value' do
        expect(subject).to be_falsy
      end
    end
  end
end
