# frozen_string_literal: true

class Bucket < ApplicationRecord
  include PgSearch::Model

  belongs_to :user

  has_many :recipes, dependent: :destroy

  pg_search_scope :search_for_name,
                  against:  :name,
                  ignoring: :accents,
                  using:    {
                    tsearch: {
                      dictionary: I18n.t("languages.#{I18n.locale}"),
                      any_word:   true
                    },
                    trigram: {
                      word_similarity: true
                    }
                  }

  scope :searchable, lambda { |**args|
    Containers::DependencyContainer['queries.buckets.searchable']
      .(
        user:       args[:user],
        query:      args[:query],
        options:    args[:options],
        eager_load: args[:eager_load]
      )
  }
end
