# frozen_string_literal: true

require 'rails_helper'

describe RecipeImport, type: :model do
  it { is_expected.to have_db_column(:imports).of_type(:jsonb).with_options(default: []) }
  it { is_expected.to have_db_column(:result).of_type(:integer).with_options(default: 0) }
  it { is_expected.to have_db_column(:created_at).of_type(:datetime).with_options(precision: 6, null: false) }
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime).with_options(precision: 6, null: false) }
end
