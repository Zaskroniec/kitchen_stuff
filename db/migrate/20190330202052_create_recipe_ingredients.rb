# frozen_string_literal: true

class CreateRecipeIngredients < ActiveRecord::Migration[6.0]
  def change
    create_table :recipe_ingredients, id: :uuid do |t|
      t.string     :name,                                   null: false
      t.belongs_to :recipe, type: :uuid, foreign_key: true, null: false

      t.timestamps
    end

    add_index :recipe_ingredients, :name
  end
end
