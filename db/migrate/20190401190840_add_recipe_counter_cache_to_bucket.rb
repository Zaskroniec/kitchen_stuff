# frozen_string_literal: true

class AddRecipeCounterCacheToBucket < ActiveRecord::Migration[6.0]
  def change
    add_column :buckets, :recipes_count, :integer, default: 0, null: false
  end
end
