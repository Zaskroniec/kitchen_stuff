# frozen_string_literal: true

class AddNickToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :nick, :string, null: false
    add_index :users, :nick, unique: true
  end
end
