# frozen_string_literal: true

module Buckets
  class UpdateOperation < Buckets::BaseOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)

    def call(params:, bucket:)
      params
        .then(&method(:validate_params))
        .then(&method(:prepare_data))
        .then(&method(:generate_slug))
        .then { |data| bucket_operation(data, bucket) }
        .then(&method(:result))
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def bucket_operation(data, bucket)
      bucket.tap { |record| record.update(data) }
    end

    def result(record)
      Success(
        notice: translations.(%i[flash update_bucket]),
        record: record
      )
    end
  end
end
