# frozen_string_literal: true

module Recipes
  class DestroyOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
    include DependencyImporter[
      'services.translations',
      'tools.error_message'
    ]

    def call(recipe:)
      recipe
        .then(&method(:destroy_action))
        .then(&method(:result))
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def destroy_action(recipe)
      ActiveRecord::Base.transaction do
        return translations.(%i[flash destroy_recipe], name: recipe.name) if recipe.destroy

        raise Error::ProcessError, translations.(%i[exceptions destroy_recipe], name: recipe.name)
      end
    end

    def result(message)
      Success(notice: message)
    end
  end
end
