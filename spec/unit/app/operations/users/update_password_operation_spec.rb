# frozen_string_literal: true

require 'rails_helper'

module Users
  describe UpdatePasswordOperation do
    describe '#call' do
      let(:params) do
        {
          token:                 random_token,
          password:              password,
          password_confirmation: password
        }
      end
      let(:user_repo) { double(:user_repo) }
      let(:password)  { Faker::Internet.password }
      subject         { described_class.new(user_repo: user_repo).(params: params) }

      context 'success' do
        let(:user) { double(:user, reset_token: params[:token], reset_token_lifetime: Time.zone.now - 1.day) }

        before do
          expect(user_repo).to receive(:find_by).with(reset_token: params[:token]).and_return(user)
          expect(user).to      receive(:reset_token_expired?).and_return(false)
          expect(user).to      receive(:update).and_return(true)
        end

        it 'retuns notice message' do
          expect(subject.success[:notice]).to eq('Successfuly updated password, you can now login.')
        end
      end

      context 'failure' do
        context 'validation' do
          context 'empty params' do
            let(:params) { {} }

            it 'returns errors' do
              expect(subject.failure[:message]).to be_present
            end
          end
        end

        context 'invalid token' do
          before { expect(user_repo).to receive(:find_by).with(reset_token: params[:token]).and_return(nil) }

          it 'returns invalid token error' do
            expect(subject.failure[:message]).to eq('We are sorry, but the token provided from the url is invalid.')
          end
        end

        context 'expired token' do
          let(:user) { double(:user, reset_token: params[:token], reset_token_lifetime: Time.zone.now + 1.day) }

          before do
            expect(user_repo).to receive(:find_by).with(reset_token: params[:token]).and_return(user)
            expect(user).to      receive(:reset_token_expired?).and_return(true)
          end

          it 'returns expired token error' do
            expect(subject.failure[:message]).to eq('We are sorry, but the token provided is already expired.')
          end
        end
      end
    end
  end
end
