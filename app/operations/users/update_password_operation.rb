# frozen_string_literal: true

module Users
  class UpdatePasswordOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
    include DependencyImporter[
      'repositories.user_repo',
      'validators.users.update_password_validator',
      'services.translations',
      'tools.error_message',
      'libs.bcrypt'
    ]

    PASSWORD_ATTR = %i[password password_confirmation].freeze

    def call(params:)
      validate_params(params)
        .then(&method(:generate_encrypted_password))
        .then(&method(:retrieve_user))
        .then(&method(:update_user))
        .then(&method(:result))
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def validate_params(params)
      update_password_validator.(params).tap do |validator|
        raise Error::ProcessError, { errors: validator.errors.to_h }.to_json if validator.failure?
      end.to_h
    end

    def generate_encrypted_password(output)
      output.tap do |o|
        o[:encrypted_password] = bcrypt::Password.create(o[:password])
        PASSWORD_ATTR.each { |attr| o.delete(attr) }
      end
    end

    def retrieve_user(output)
      user = user_repo.find_by(reset_token: output[:token]).tap do |u|
        raise Error::ProcessError, translations.(%i[exceptions invalid_token]) if u.blank?
        raise Error::ProcessError, translations.(%i[exceptions expired_token]) if u.reset_token_expired?
      end

      { user: user, output: output }
    end

    def update_user(hash)
      hash[:user].update(
        encrypted_password:   hash.dig(:output, :encrypted_password),
        reset_token:          nil,
        reset_token_lifetime: nil,
        reset_token_at:       Time.zone.now
      )
    end

    def result(_flag)
      Success(notice: translations.(%i[flash update_password]))
    end
  end
end
