# frozen_string_literal: true

require 'rails_helper'

module Tools
  describe UrlParser do
    describe '#self.call' do
      subject { described_class.(url: url) }

      context 'success' do
        let(:url) { default_recipe_url }

        it 'returns hash with scheme' do
          expect(subject[:scheme]).to eq('https')
        end

        it 'returns hash with host' do
          expect(subject[:host]).to eq('www.allrecipes.com')
        end

        it 'returns hash with path' do
          expect(subject[:path]).to eq('/recipe/132815/balsamic-roasted-pork-loin')
        end

        it 'returns hash with query' do
          expect(subject[:query]).to be_nil
        end

        it 'returns hash with base_url' do
          expect(subject[:base_url]).to eq(default_recipe_url)
        end
      end

      context 'failure' do
        context 'invalid path' do
          let(:url) { 'test' }

          it 'raises error' do
            expect { subject }.to raise_error(
              Error::InvalidUrlError,
              'We do not support this url.'
            )
          end
        end
      end
    end
  end
end
