# frozen_string_literal: true

require 'rails_helper'

module Recipes
  describe DestroyOperation do
    describe '#call' do
      let(:name)           { Faker::Internet.name }
      let(:existed_recipe) { double(:recipe, id: random_uuid, name: name) }
      let(:deleted_recipe) { double(:recipe, id: nil, name: name) }
      subject              { described_class.new.(recipe: existed_recipe) }

      context 'success' do
        before { expect(existed_recipe).to receive(:destroy).and_return(deleted_recipe) }

        it 'returns valid message' do
          expect(subject.success[:notice]).to eq("Successfuly destroyed recipe: #{name}.")
        end
      end

      context 'failure' do
        before { expect(existed_recipe).to receive(:destroy).and_return(false) }

        it 'returns valid message' do
          expect(subject.failure[:message])
            .to eq("We are sorry, but we our system occured error while deleting recipe: #{name}. Try it later.")
        end
      end
    end
  end
end
