# frozen_string_literal: true

module Constants
  # TODO: Remember to add more extensions later, especially before lunch to production
  FILE_EXTENSIONS      = %w[jpg jpeg png].freeze
  BIG_FILE_SIZE        = [600, 600].freeze
  MEDIUM_FILE_SIZE     = [400, 400].freeze
  SMALL_FILE_SIZE      = [250, 250].freeze
  CACHE_STORAGE        = 'cache'
  S3_BUCKET_EXPIRATION = 7
  MAX_REMOTE_FILE      = 20 * 1024 * 1024

  EMPTY_STRING = ''

  DEFAULT_TOKEN_HEX         = 96
  DEFAULT_PAGINATION_SIZE   = 6
  DEFAULT_PAGINATION_RANGE  = [1, 4, 4, 1].freeze
  DEFAULT_EMAIL_FROM        = 'no-reply@kitchen-stuff.com'
  DEFAULT_USER_BUCKETS      = %w[Breakfast Lunch Dinner Snack Supper].freeze
  DEFAULT_IMPORT_NAME       = 'Imported Recipe'
  DEFAULT_TIME_STRING       = 'Not provided'
  DEFAULT_BACKTRACE         = nil

  HTML_AND_SCRIPT_TAG                = 'recipeIngredient'
  NAME_TAGS                          = %w[h1 div span].freeze
  SCRIPT_TAG                         = 'script'
  HTML_NAME_ATTRIBUTE                = "[itemprop='name']"
  HTML_IMAGE_ATTRIBUTE               = "[itemprop='image']"
  HTML_DESCRIPTION_ATTRIBUTE         = "[itemprop='description']"
  HTML_PREP_TIME_ATTRIBUTE           = "[itemprop='prepTime']"
  HTML_COOK_TIME_ATTRIBUTE           = "[itemprop='cookTime']"
  HTML_TOTAL_TIME_ATTRIBUTE          = "[itemprop='totalTime']"
  HTML_RECIPE_YIELD_ATTRIBUTE        = "[itemprop='recipeYield']"
  HTML_RECIPE_INGREDIENT_ATTRIBUTE   = "[itemprop='recipeIngredient']"
  HTML_INGREDIENT_ATTRIBUTE          = "[itemprop='ingredients']"
  HTML_RECIPE_CUISINE_ATTRIBUTE      = "[itemprop='recipeCuisine']"
  HTML_RECIPE_CATEGORY_ATTRIBUTE     = "[itemprop='recipeCategory']"
  HTML_RECIPE_INSTRUCTIONS_ATTRIBUTE = "[itemprop='recipeInstructions']"
  IMPORT_KEYS                        = %i[name image description recipeIngredient
                                          prepTime cookTime totalTime recipeCuisine
                                          recipeCategory recipeYield recipeInstructions
                                          keywords nutrition].freeze
  NUTRITION_REJECT_TYPE_KET          = :@type
  NUTRITION_REJECT_CONTEXT_KET       = :@context
  HTML_CONTENT_ATTRIBUTE             = 'content'
  HTML_DATETIME_ATTRIBUTE            = 'datetime'

  EMAIL_FORMAT       = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  PASSWORD_FORMAT    = /^[a-zA-Z0-9]+$/.freeze
  BUCKET_NAME_FORMAT = /^[a-zA-Z\s]+$/.freeze
  REQUEST_FORMAT     = /^https?/.freeze
  URL_PATH_FORMAT    = /^.*\.(#{FILE_EXTENSIONS.join('|')})$/.freeze
  MAX_STRING_LENGTH  = 255
  MAX_TEXT_SIZE      = 2000

  SUPPORTED_LANGUAGES = %i[en].freeze

  SUPPORTED_PAGES = [
    {
      name: 'allrecipes',
      url:  'https://www.allrecipes.com/recipe/259870/briam-greek-baked-zucchini-and-potatoes'
    },
    {
      name: 'jamieoliver',
      url:  'https://www.jamieoliver.com/recipes/eggs-recipes/roasted-chilli-frittata'
    },
    {
      name: 'taste',
      url:  'https://www.taste.com.au/recipes/grilled-miso-fish-snow-pea-salad/ac72d134-a7c1-4989-89db-0cf45f439353'
    },
    {
      name: 'foodnetworkUK',
      url:  'http://www.foodnetwork.co.uk/article/mothers-day-dinners/easy-carve-roast-lamb/2.html'
    },
    {
      name: 'yummly',
      url:  'https://www.yummly.com/recipe/No-Bake-Lemon-Mango-Cheesecakes-with-Speculoos-crust-781945'
    },
    {
      name: 'cookpad',
      url:  'https://cookpad.com/pl/przepisy/7710559-gulasz-wieprzowy'
    }
  ].freeze

  CAPTCHA_URL = 'https://www.google.com/recaptcha/api/siteverify'
end
