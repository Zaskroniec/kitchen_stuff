# frozen_string_literal: true

class UsersController < ApplicationController
  include DependencyImporter[
    'operations.users.inline_update_password_operation',
    'services.static_meta_tags'
  ]

  before_action :redirect_to_new_sesseion, only: %i[account update]

  def account
    prepare_account
    set_meta_tags(**static_meta_tags.(key: :account))
  end

  def update_password
    inline_update_password_operation.(params: unsafe_params, user: current_user) do |service|
      service.success do |result|
        redirect_to "#{account_users_path}#changePassword", notice: result[:notice]
      end

      service.failure do |error_object|
        prepare_account
        render_error(:account, error_object)
      end
    end
  end

  def prepare_account
    @contacts   ||= current_user.contacts.newest.first(Constants::DEFAULT_PAGINATION_SIZE)
    @statistics ||= current_user.show_statistics
  end
end
