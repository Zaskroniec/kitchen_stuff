# frozen_string_literal: true

module Recipes
  module Importer
    module Retrievers
      class ScriptDataRetriever < Recipes::Importer::Retrievers::BaseRetriever
        EMPTY_ARRAY = [].freeze

        def call(raw_data:)
          raw_data
            .then(&:children)
            .then(&:text)
            .then(&method(:parse_json))
            .then(&method(:build_data))
        end

        private

        def parse_json(json_data)
          object = json.parse(json_data, symbolize_names: true)

          return object if object.is_a?(Hash)

          object.detect { |h| h[:@type].eql?('Recipe') }
        rescue Error::JsonParseError
          raise Error::ProcessError,
                translations.(%i[exceptions corrupted_data])
        end

        def get_name(hash)
          hash[:name]
        end

        def get_image(hash)
          return hash[:image][:url] if hash[:image].is_a?(Hash)

          hash[:image]
        end

        def get_description(hash)
          hash[:description]
        end

        # rubocop:disable Naming/MethodName
        def get_prepTime(hash)
          parse_time(hash[:prepTime])
        end

        def get_cookTime(hash)
          parse_time(hash[:cookTime])
        end

        def get_totalTime(hash)
          parse_time(hash[:totalTime])
        end

        def get_recipeYield(hash)
          hash[:recipeYield]
        end

        def get_recipeIngredient(hash)
          hash[:recipeIngredient]
        end

        def get_recipeCuisine(hash)
          return [hash[:recipeCuisine]] || EMPTY_ARRAY if hash[:recipeCuisine].is_a?(String)

          hash[:recipeCuisine] || EMPTY_ARRAY
        end

        def get_recipeCategory(hash)
          return [hash[:recipeCategory]] || EMPTY_ARRAY if hash[:recipeCategory].is_a?(String)

          hash[:recipeCategory] || EMPTY_ARRAY
        end

        def get_recipeInstructions(hash)
          return [hash[:recipeInstructions]] if hash[:recipeInstructions].is_a?(String)

          hash[:recipeInstructions].map do |instruction|
            if instruction.is_a?(Hash)
              instruction[:text]
            else
              instruction
            end
          end
        end

        def get_keywords(hash)
          hash[:keywords]
        end

        def get_nutrition(hash)
          return nil if hash[:nutrition].blank?

          hash[:nutrition]
            .then { |h| h.reject { |key, _value| key.eql?(Constants::NUTRITION_REJECT_TYPE_KET) } }
            .then { |h| h.reject { |key, _value| key.eql?(Constants::NUTRITION_REJECT_CONTEXT_KET) } }
        end
        # rubocop:enable Naming/MethodName
      end
    end
  end
end
