# frozen_string_literal: true

module Containers
  module Namespaces
    Controllers = Dry::Container::Namespace.new('controllers') do
      register(:application_controller) { ApplicationController.new }
    end
  end
end
