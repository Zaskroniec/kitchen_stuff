# frozen_string_literal: true

module Recipes
  class RecipeValidator < ApplicationValidator
    include ValidatorExtenders::BasicExtender
    include ValidatorExtenders::FileExtender

    option :recipe_repo

    params do # rubocop:disable Metrics/BlockLength
      required(:bucket_id).filled(:string)
      required(:name).filled(:string, max_size?: Constants::MAX_STRING_LENGTH)
      required(:difficulty).filled(:integer)
      required(:recipe_ingredients_attributes).array(:hash) do
        optional(:id).value(:string)
        required(:name).value(:string, :filled?, max_size?: Constants::MAX_STRING_LENGTH)
        optional(:_destroy).value(Types::Coercible::Bool)
      end
      required(:recipe_instructions_attributes).array(:hash) do
        optional(:id).value(:string)
        required(:content).value(:string, :filled?)
        optional(:_destroy).value(Types::Coercible::Bool)
      end
      required(:prep_time).schema do
        optional(:hours).maybe(:integer)
        optional(:minutes).maybe(:integer)
      end
      required(:cook_time).schema do
        optional(:hours).maybe(:integer)
        optional(:minutes).maybe(:integer)
      end
      required(:total_time).schema do
        optional(:hours).maybe(:integer)
        optional(:minutes).maybe(:integer)
      end
      optional(:id).value(:string, :filled?)
      optional(:keywords).maybe(:string, max_size?: Constants::MAX_STRING_LENGTH)
      optional(:description).maybe(:string)
      optional(:servings).maybe(:string, max_size?: Constants::MAX_STRING_LENGTH)
      optional(:image).value(:filled?)
      optional(:categories_attributes).array(:hash) do
        optional(:id).value(:string)
        required(:name).value(:string, max_size?: Constants::MAX_STRING_LENGTH)
        required(:_destroy).value(Types::Coercible::Bool)
      end
      optional(:cuisines_attributes).array(:hash) do
        optional(:id).value(:string)
        required(:name).value(:string, max_size?: Constants::MAX_STRING_LENGTH)
        required(:_destroy).value(Types::Coercible::Bool)
      end
    end

    rule(:image) do
      if value.present? && !valid_size?(value)
        key.failure(translations.(%i[errors valid_size?]))
      end

      if value.present? && !valid_extension?(value)
        key.failure(translations.(%i[errors valid_extension?]))
      end
    end

    rule(:name) do
      unless unique_field?(recipe_repo, { name: value, bucket_id: values.data[:bucket_id] }, values.data[:id])
        key.failure(translations.(%i[errors unique_field?]))
      end
    end

    rule(:categories_attributes) do
      if value.present? && value.size != value.uniq.size
        key.failure(translations.(%i[errors duplicated_items]))
      end
    end

    rule(:cuisines_attributes) do
      if value.present? && value.size != value.uniq.size
        key.failure(translations.(%i[errors duplicated_items]))
      end
    end
  end
end
