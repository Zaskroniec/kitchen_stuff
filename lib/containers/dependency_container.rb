# frozen_string_literal: true

module Containers
  class DependencyContainer
    extend Dry::Container::Mixin
    import Containers::Namespaces::Controllers
    import Containers::Namespaces::Repositories
    import Containers::Namespaces::Validators
    import Containers::Namespaces::Services
    import Containers::Namespaces::Queries
    import Containers::Namespaces::Mailers
    import Containers::Namespaces::Jobs
    import Containers::Namespaces::Operations
    import Containers::Namespaces::Tools
    import Containers::Namespaces::Structs
    import Containers::Namespaces::Libs
  end
end
