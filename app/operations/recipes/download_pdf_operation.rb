# frozen_string_literal: true

module Recipes
  class DownloadPdfOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
    include DependencyImporter[
      'services.documents.pdfs.generator',
      'services.translations',
      'tools.error_message'
    ]

    def call(recipe:)
      prepare_document_options(recipe)
        .then(&method(:generate_document))
        .then(&method(:result))
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def prepare_document_options(recipe)
      {
        template_args: {
          template: 'recipes/show.pdf.slim',
          locals:   { '@recipe': recipe }
        },
        pdf_options:   {
          title: recipe.name
        }
      }
    end

    def generate_document(args)
      generator.(**args)
    rescue StandardError => _e
      raise Error::ProcessError, translations.(%i[exceptions pdf_crash])
    end

    def result(document)
      Success(record: document)
    end
  end
end
