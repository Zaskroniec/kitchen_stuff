# frozen_string_literal: true

module Recipes
  module Importer
    module Retrievers
      class HtmlDataRetriever < Recipes::Importer::Retrievers::BaseRetriever
        def call(raw_data:)
          raw_data.then(&method(:build_data))
        end

        private

        def parse_single_sentence(string)
          string.try(:tr, "\n", '').try(:strip)
        end

        def squeeze_text(element)
          element.text.try(:squeeze, ' ')
        end

        def parse_text(text)
          text.match(/^[a-zA-Z\d].+/).try(:[], 0)
        end

        def generate_time_node(raw_data, html_attr)
          element = raw_data.css(html_attr)
          content = element.text.try(:tr, ' ', '')
          return parse_time(content) if content =~ /(\d+[a-zA-Z])/

          content = element
                    .map { |el| retrieve_element_value(el, Constants::HTML_CONTENT_ATTRIBUTE) }
                    .try(:first)

          parse_time(content)
        end

        def retrieve_text(raw_data, itemprop, html_tags)
          html_tags.each do |tag|
            item = itemprop.split.unshift(tag).join
            element = raw_data.css(item)
            return parse_single_sentence(element.text) if element.text.present?
          end
        end

        def retrieve_element_value(element, attribute_name)
          element.attributes.detect do |attr|
            attr.name.eql?(attribute_name)
          end.try(:value)
        end

        def get_name(raw_data)
          collection = raw_data.css(Constants::HTML_NAME_ATTRIBUTE)
          collection_size = collection.size

          if collection_size > 1
            retrieve_text(raw_data, Constants::HTML_NAME_ATTRIBUTE, Constants::NAME_TAGS)
          elsif collection_size == 1
            parse_single_sentence(collection[0].text)
          end
        end

        def get_image(raw_data)
          raw_data
            .css(Constants::HTML_IMAGE_ATTRIBUTE)
            .first
            .try(:[], :src)
        end

        def get_description(raw_data)
          raw_data
            .css(Constants::HTML_DESCRIPTION_ATTRIBUTE)
            .then(&method(:squeeze_text))
        end

        def get_keywords(_hash)
          nil
        end

        def get_nutrition(_hash)
          nil
        end

        # rubocop:disable Naming/MethodName
        def get_prepTime(raw_data)
          generate_time_node(raw_data, Constants::HTML_PREP_TIME_ATTRIBUTE)
        end

        def get_cookTime(raw_data)
          generate_time_node(raw_data, Constants::HTML_COOK_TIME_ATTRIBUTE)
        end

        def get_totalTime(raw_data)
          generate_time_node(raw_data, Constants::HTML_TOTAL_TIME_ATTRIBUTE)
        end

        def get_recipeYield(raw_data)
          raw_data
            .css(Constants::HTML_RECIPE_YIELD_ATTRIBUTE)
            .map { |el| retrieve_element_value(el, Constants::HTML_CONTENT_ATTRIBUTE) }
            .try(:first)
        end

        def get_recipeIngredient(raw_data)
          content = raw_data
                    .css(Constants::HTML_RECIPE_INGREDIENT_ATTRIBUTE)
                    .map(&method(:squeeze_text))
                    .map(&method(:parse_text))
          return content if content.present?

          raw_data
            .css(Constants::HTML_INGREDIENT_ATTRIBUTE)
            .map(&method(:squeeze_text))
            .map(&method(:parse_text))
        end

        def get_recipeCuisine(raw_data)
          raw_data
            .css(Constants::HTML_RECIPE_CUISINE_ATTRIBUTE)
            .map { |el| retrieve_element_value(el, Constants::HTML_CONTENT_ATTRIBUTE) }
        end

        def get_recipeCategory(raw_data)
          raw_data
            .css(Constants::HTML_RECIPE_CATEGORY_ATTRIBUTE)
            .map { |el| retrieve_element_value(el, Constants::HTML_CONTENT_ATTRIBUTE) }
        end

        def get_recipeInstructions(raw_data)
          raw_data
            .css(Constants::HTML_RECIPE_INSTRUCTIONS_ATTRIBUTE)
            .map(&method(:squeeze_text))
        end
        # rubocop:enable Naming/MethodName
      end
    end
  end
end
