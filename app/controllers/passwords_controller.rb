# frozen_string_literal: true

class PasswordsController < ApplicationController
  include DependencyImporter[
    'operations.users.reset_password_operation',
    'operations.users.update_password_operation'
  ]

  before_action :redirect_to_my_buckets

  def edit; end

  def update_password
    update_password_operation.(params: unsafe_params) do |service|
      service.success do |result|
        redirect_to new_sessions_path, notice: result[:notice]
      end

      service.failure do |error_object|
        render_error(:edit, error_object)
      end
    end
  end

  def reset_password
    reset_password_operation.(params: unsafe_params) do |service|
      service.success do |result|
        redirect_to recover_passwords_path, notice: result[:notice]
      end

      service.failure do |error_object|
        render_error(:recover, error_object)
      end
    end
  end

  def recover; end
end
