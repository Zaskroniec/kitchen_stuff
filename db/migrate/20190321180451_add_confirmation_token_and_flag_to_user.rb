# frozen_string_literal: true

class AddConfirmationTokenAndFlagToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :confirmation_token, :string
    add_column :users, :confirmed, :boolean, default: false, null: false

    add_index :users, :confirmation_token, unique: true
    add_index :users, :confirmed, name: 'index_users_on_confirmed_users', where: '(confirmed IS TRUE)'
    add_index :users, :confirmed, name: 'index_users_on_unconfirmed_users', where: '(confirmed IS FALSE)'
  end
end
