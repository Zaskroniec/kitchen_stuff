# frozen_string_literal: true

require 'rails_helper'

describe Translations do
  describe '#self.call' do
    let(:translation) { %i[exceptions external_api unavailable] }

    subject { described_class.(translation) }

    it 'returns translation' do
      expect(subject).to eq('We are sorry, but external service is currently unavailable.')
    end
  end
end
