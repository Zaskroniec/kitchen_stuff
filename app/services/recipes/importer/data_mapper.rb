# frozen_string_literal: true

module Recipes
  module Importer
    class DataMapper
      include DependencyImporter[
        'services.tag_generator',
        'validators.recipes.data_mapper_validator',
        'services.translations'
      ]

      ATTRIBUTES = %i[name description recipe_url keywords].freeze
      IMPORTED   = true

      def call(data:)
        map_data(data).then(&method(:validate_mapped_data))
      end

      private

      def map_data(data) # rubocop:disable Metrics/AbcSize
        data.slice(*ATTRIBUTES).tap do |mapped_data|
          mapped_data[:image_remote_url]               = data[:image]
          mapped_data[:recipe_ingredients_attributes]  = prepare_hash_collection(retrieve_ingredient_field(data), :name)
          mapped_data[:recipe_instructions_attributes] = prepare_hash_collection(data[:recipe_instructions], :content)
          mapped_data[:nutrition]                      = prepare_nutrition(data[:nutrition])
          mapped_data[:servings]                       = data[:recipe_yield].try(:to_s)
          mapped_data[:prep_time]                      = prepare_time_values(data[:prep_time])
          mapped_data[:cook_time]                      = prepare_time_values(data[:cook_time])
          mapped_data[:total_time]                     = prepare_time_values(data[:total_time])
          mapped_data[:imported]                       = IMPORTED
          prepare_tags(mapped_data, data)
        end
      end

      def validate_mapped_data(mapped_data)
        data_mapper_validator.(mapped_data).tap do |validator|
          if validator.failure?
            raise Error::ProcessError, translations.(%i[exceptions invalid_mapped_data], body: mapped_data.to_json)
          end
        end.to_h
      end

      def retrieve_ingredient_field(data)
        data[:recipe_ingredient] || data[:ingredients]
      end

      def prepare_hash_collection(array, key)
        return [] if array.blank?

        array.map { |item| { "#{key}": item } }
      end

      def prepare_nutrition(hash)
        return {} if hash.blank?

        hash.transform_keys { |key| key.to_s.underscore }
      end

      def prepare_time_values(hash)
        return {} if hash.blank?

        hash.transform_values { |value| value.gsub(/H|M$/, '').to_i if value.present? }
      end

      def prepare_tags(mapped_data, data) # rubocop:disable Metrics/MethodLength
        params = data
                 .slice(:recipe_category, :recipe_cuisine)
                 .map { |item| { "#{item[0]}": item[1].map { |value| { name: value } } } }

        categories_collection = tag_generator.(
          hash:   { categories_attributes: params[0][:recipe_category] },
          target: :categories_attributes,
          result: :recipe_categories_attributes
        )
        cuisines_collection = tag_generator.(
          hash:   { cuisines_attributes: params[1][:recipe_cuisine] },
          target: :cuisines_attributes,
          result: :recipe_cuisines_attributes
        )

        mapped_data.merge!(*categories_collection, *cuisines_collection)
      end
    end
  end
end
