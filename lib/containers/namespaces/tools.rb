# frozen_string_literal: true

module Containers
  module Namespaces
    Tools = Dry::Container::Namespace.new('tools') do
      register(:error_message)  { ::Tools::ErrorMessage.new }
      register(:url_parser)     { ::Tools::UrlParser.new }
      register(:captcha_client) { ::Tools::CaptchaClient.new }
      register(:slug_generator) { ::Tools::SlugGenerator.new }
      register(:time_formatter) { ::Tools::TimeFormatter.new }
      register(:params_mapper)  { ::Tools::ParamsMapper.new }
    end
  end
end
