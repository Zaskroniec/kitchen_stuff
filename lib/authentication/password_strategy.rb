# frozen_string_literal: true

module Authentication
  class PasswordStrategy < Warden::Strategies::Base
    include DependencyImporter[
      'libs.bcrypt',
      'services.translations',
      'repositories.user_repo'
    ]

    def valid?
      params['email'] && params['password']
    end

    def authenticate!
      user = user_repo.find_by(email: params['email'])
      return invalid_session_error if user.blank?
      return confirmation_error unless user.confirmed

      user_password = bcrypt::Password.new(user.encrypted_password)
      return invalid_session_error if user_password != params['password']

      success!(user)
    end

    private

    def invalid_session_error
      fail!(translations.(%i[errors session]))
    end

    def confirmation_error
      fail!(translations.(%i[errors confirmation]))
    end
  end
end
