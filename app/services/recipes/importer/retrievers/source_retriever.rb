# frozen_string_literal: true

module Recipes
  module Importer
    module Retrievers
      class SourceRetriever
        include DependencyImporter[
          'libs.oga',
          'libs.httparty',
          'services.translations'
        ]

        OPTIONS = {
          timeout: 5,
          headers: {
            'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'\
                            ' (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36'
          }
        }.freeze

        def call(url:)
          httparty
            .then { |service| service.get(url, **OPTIONS) }
            .then(&method(:verify_response))
            .then(&oga.method(:parse_html))
        rescue Error::RequestTimeoutError
          raise Error::ProcessError, translations.(%i[exceptions external_api unavailable])
        rescue Error::ResponseContentTypeError
          raise Error::ProcessError, translations.(%i[exceptions invalid_document_type])
        end

        def self.call(**args)
          new.(**args)
        end

        private

        def verify_response(response)
          return response if response.success?

          raise Error::ProcessError, translations.(%i[exceptions external_api unavailable])
        end
      end
    end
  end
end
