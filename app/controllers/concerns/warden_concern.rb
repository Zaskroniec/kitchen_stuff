# frozen_string_literal: true

module WardenConcern
  extend ActiveSupport::Concern

  included do
    helper_method :current_user, :signed_in?, :unsigned?

    def current_user
      warden.user
    end

    def signed_in?
      current_user.present?
    end

    def unsigned?
      current_user.blank?
    end

    private

    def non_admin?
      !current_user.try(:admin)
    end

    def warden
      request.env['warden']
    end
  end
end
