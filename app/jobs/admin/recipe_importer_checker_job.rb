# frozen_string_literal: true

module Admin
  class RecipeImporterCheckerJob < ApplicationJob
    queue_as :admin

    def perform
      importer_checker.(supported_sites: Constants::SUPPORTED_PAGES)
    end

    private

    def importer_checker
      @importer_checker ||= dependency_container['services.admin.importer_checker']
    end
  end
end
