# frozen_string_literal: true

require 'rails_helper'

module Users
  describe GenerateConfirmationInstruction do
    describe '#call' do
      let(:user_repo) { double(:user_repo) }
      let(:user_id)   { 'test1234' }
      let(:user)      { double(:user, email: Faker::Internet.email, confirmation_token: 'test') }
      subject         { described_class.new(user_repo: user_repo).(user_id: user_id) }

      before do
        # token generator
        expect(user_repo)
          .to receive(:find_by)
          .and_return([])

        expect(user_repo)
          .to receive(:update)
          .and_return(user)
      end

      it 'has valid class name' do
        expect(subject.class).to eq(Mail::Message)
      end
    end
  end
end
