import { Controller } from 'stimulus';

export default class extends Controller {
  static targets = ['image'];

  previewImage(event) {
    const reader = new FileReader;
    const file = event.target.files;
    const that = this;

    reader.onload = (event) => {
      that.imageTarget.src = event.target.result;
    };

    if(file && file[0]) {
      reader.readAsDataURL(file[0]);
    }
  }
}
