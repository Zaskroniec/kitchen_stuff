# frozen_string_literal: true

class StaticMetaTags
  include DependencyImporter['services.translations']

  def call(key:)
    meta_tags[key]
  end

  private

  def meta_tags
    {
      home: {
        title:       translations.(%i[meta_tags home title]),
        description: translations.(%i[meta_tags home description]),
        keywords:    translations.(%i[meta_tags home keywords])
      },
      contact: {
        title:       translations.(%i[meta_tags contact title]),
        description: translations.(%i[meta_tags contact description]),
        keywords:    translations.(%i[meta_tags contact keywords])
      },
      features: {
        title:       translations.(%i[meta_tags features title]),
        description: translations.(%i[meta_tags features description]),
        keywords:    translations.(%i[meta_tags features keywords])
      },
      login: {
        title:       translations.(%i[meta_tags login title]),
        description: translations.(%i[meta_tags login description]),
        keywords:    translations.(%i[meta_tags login keywords])
      },
      signup: {
        title:       translations.(%i[meta_tags signup title]),
        description: translations.(%i[meta_tags signup description]),
        keywords:    translations.(%i[meta_tags signup keywords])
      },
      changelog: {
        title:       translations.(%i[meta_tags changelog title]),
        description: translations.(%i[meta_tags changelog description]),
        keywords:    translations.(%i[meta_tags changelog keywords])
      },
      terms: {
        title:       translations.(%i[meta_tags terms title]),
        description: translations.(%i[meta_tags terms description]),
        keywords:    translations.(%i[meta_tags terms keywords])
      },
      privacy: {
        title:       translations.(%i[meta_tags privacy title]),
        description: translations.(%i[meta_tags privacy description]),
        keywords:    translations.(%i[meta_tags privacy keywords])
      },
      company: {
        title:       translations.(%i[meta_tags company title]),
        description: translations.(%i[meta_tags company description]),
        keywords:    translations.(%i[meta_tags company keywords])
      },
      account_plans: {
        title:       translations.(%i[meta_tags account_plans title]),
        description: translations.(%i[meta_tags account_plans description]),
        keywords:    translations.(%i[meta_tags account_plans keywords])
      },
      buckets: {
        title:       translations.(%i[meta_tags buckets title]),
        description: translations.(%i[meta_tags buckets description]),
        keywords:    translations.(%i[meta_tags buckets keywords])
      },
      recipes: {
        title:       translations.(%i[meta_tags recipes title]),
        description: translations.(%i[meta_tags recipes description]),
        keywords:    translations.(%i[meta_tags recipes keywords])
      },
      account: {
        title:       translations.(%i[meta_tags account title]),
        description: translations.(%i[meta_tags account description]),
        keywords:    translations.(%i[meta_tags account keywords])        
      }
    }
  end
end
