# frozen_string_literal: true

module Containers
  module Namespaces
    Queries = Dry::Container::Namespace.new('queries') do
      namespace(:recipes) do
        register(:searchable) { Recipes::SearchableQuery.new }
      end

      namespace(:buckets) do
        register(:searchable) { Buckets::SearchableQuery.new }
      end
    end
  end
end
