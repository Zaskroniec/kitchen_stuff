# frozen_string_literal: true

class BucketsController < ApplicationController
  include PaginationConcern
  include DependencyImporter[
    'repositories.bucket_repo',
    'repositories.recipe_repo',
    'operations.buckets.create_operation',
    'operations.buckets.update_operation',
    'operations.buckets.destroy_operation',
    'services.static_meta_tags'
  ]

  before_action :redirect_to_new_sesseion
  before_action :newest_buckets, except: %i[show paginate]

  layout false, only: %i[paginate]

  def index
    new_bucket
    set_meta_tags(**static_meta_tags.(key: :buckets))
  end

  def paginate
    @pagy, @buckets = paginate_resource(:buckets)
  rescue Error::PageError => _e
    @buckets = []
    render nothing: true, status: :no_content && return
  rescue Pagy::OverflowError => _e
    @buckets = []
    render nothing: true, status: :no_content && return
  end

  def show
    fresh_when(bucket)
    show_breadcrumbs
    new_recipe
    newest_recipes
    set_meta_tags(title: bucket.name)
  end

  def create
    create_operation.(params: prepare_bucket_params, user: current_user) do |service|
      service.success do |result|
        redirect_to bucket_path(slug: result[:record].slug), notice: result[:notice]
      end

      service.failure do |error_object|
        new_bucket.attributes = unsafe_params[:bucket]
        @create_failure_dialog ||= true
        render_error(:index, error_object)
      end
    end
  end

  def update
    update_operation
      .(params: prepare_bucket_params, bucket: bucket) do |service|
      service.success do |result|
        redirect_to bucket_path(slug: result[:record].slug), notice: result[:notice]
      end

      service.failure do |error_object|
        @update_failure_bucket_id ||= bucket.id
        new_bucket
        render_error(:index, error_object)
      end
    end
  end

  def destroy
    destroy_operation.(bucket: bucket) do |service|
      service.success do |result|
        redirect_to buckets_path, notice: result[:notice]
      end

      service.failure do |error_object|
        redirect_to buckets_path, error: error_object[:message]
      end
    end
  end

  private

  def show_breadcrumbs
    add_breadcrumb t('views.labels.my_buckets'), buckets_path
    add_breadcrumb t('views.labels.bucket')
  end

  def prepare_bucket_params
    (unsafe_params[:bucket] || {}).tap do |hash|
      hash[:user_id] = current_user.id
      hash[:id]      = bucket.id if params[:slug]
    end
  end

  def newest_buckets
    @buckets ||= buckets_scope.limit(Constants::DEFAULT_PAGINATION_SIZE).offset(0)
    fresh_when(@buckets)
  end

  def newest_recipes
    @recipes ||= recipes_scope.limit(Constants::DEFAULT_PAGINATION_SIZE).offset(0)
    fresh_when(@recipes)
  end

  def bucket
    @bucket ||= current_user.buckets.find_by!(slug: params[:slug])
  end

  def new_recipe
    @new_recipe ||= Recipe.new
  end

  def new_bucket
    @new_bucket ||= current_user.buckets.new
  end
end
