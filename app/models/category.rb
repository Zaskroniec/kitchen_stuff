# frozen_string_literal: true

class Category < ApplicationRecord
  include PgSearch::Model
  include TagsConcern

  has_many :recipe_categories, dependent: :destroy
end
