# frozen_string_literal: true

require 'rails_helper'

describe RecipeIngredient, type: :model do
  it { is_expected.to belong_to(:recipe).counter_cache(true) }

  it { is_expected.to have_db_index(:name) }

  it { is_expected.to have_db_column(:name).of_type(:string).with_options(null: false) }
  it { is_expected.to have_db_column(:recipe_id).of_type(:uuid).with_options(null: false) }
end
