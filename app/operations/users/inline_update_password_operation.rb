# frozen_string_literal: true

module Users
  class InlineUpdatePasswordOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
    include DependencyImporter[
      'validators.users.inline_update_password_validator',
      'services.translations',
      'tools.error_message',
      'libs.bcrypt'
    ]

    PASSWORD_ATTR = %i[current_password password password_confirmation].freeze

    def call(params:, user:)
      validate_params(params)
        .then { |output| verify_current_password(output, user) }
        .then(&method(:generate_encrypted_password))
        .then { |output| update_user(output, user) }
        .then(&method(:result))
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def validate_params(params)
      inline_update_password_validator.(params).tap do |validator|
        raise Error::ProcessError, { errors: validator.errors.to_h }.to_json if validator.failure?
      end.to_h
    end

    def verify_current_password(output, user)
      output.tap do
        unless bcrypt::Password.new(user.encrypted_password) == output[:current_password]
          raise Error::ProcessError, translations.(%i[exceptions invalid_current_password])
        end
      end
    end

    def generate_encrypted_password(output)
      output.tap do |o|
        o[:encrypted_password] = bcrypt::Password.create(o[:password])
        PASSWORD_ATTR.each { |attr| o.delete(attr) }
      end
    end

    def update_user(output, user)
      user.update(output)
    end

    def result(_flag)
      Success(notice: translations.(%i[flash inline_update_password]))
    end
  end
end
