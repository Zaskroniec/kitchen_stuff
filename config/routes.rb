# frozen_string_literal: true

require 'sidekiq/web'
require 'sidekiq-scheduler/web'

Rails.application.routes.draw do # rubocop:disable Metrics/BlockLength
  # TODO: add constraints { subdomain: 'api' }
  scope module: :api, defaults: { format: :json } do
    namespace :v1 do
      resources :search, only: %i[] do
        collection do
          get :cuisines
          get :categories
        end
      end
    end
  end

  namespace :admin do
    scope constraints: ->(request) { request.env['warden'].user.try(:admin) || false } do
      mount Sidekiq::Web => '/sidekiq'
    end

    resource :dashboard, only: %i[show]

    resources :tools, only: %i[] do
      collection do
        get  :style_guide
        get  :new_recipe
        post :create_recipe
      end
    end
  end

  resources :recipes, only: %i[] do
    get :my_collection, on: :collection
    get :paginate, on: :collection
  end

  resources :buckets, param: :slug do
    get :paginate, on: :collection
    resources :recipes, except: %i[index] do
      collection do
        post :import
      end
      member do
        get :import_status
        get :download_pdf
      end
    end
  end
  resources :contacts, only: %i[index create] do
    get :paginate, on: :collection
  end
  resources :users, only: %i[] do
    get :account, on: :collection
    patch :update_password, on: :collection
  end

  resources :registrations, only: %i[new create], param: :token do
    get :confirmation, on: :member
  end
  resources :passwords, only: %i[edit], param: :token do
    patch :update_password, on: :member
    collection do
      get :recover
      patch :reset_password
    end
  end

  resource :sessions,  only: %i[new create destroy]
  resource :home,      only: %i[show] do
    collection do
      get :terms
      get :privacy
      get :company
      get :changelog
      get :account_plans
      get :features
    end
  end

  # get '*pages' => redirect('/404.html', status: 302)

  root 'homes#show'
end
