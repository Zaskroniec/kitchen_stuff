# frozen_string_literal: true

module Users
  class GenerateConfirmationInstruction
    include DependencyImporter[
      'mailers.user_mailer',
      'repositories.user_repo',
      'services.token_generator'
    ]

    def call(user_id:)
      token_generator
        .(repo: user_repo, attribute: 'confirmation_token')
        .then { |token|  user_repo.update(user_id, confirmation_token: token) }
        .then { |user|   user_mailer.with(user: user) }
        .then(&:confirmation_email)
        .then(&:deliver_now)
    end
  end
end
