# frozen_string_literal: true

module Tools
  class UrlParser
    include DependencyImporter['services.translations', 'libs.uri']

    def call(url:)
      uri.parse(url)
         .then(&method(:validate_uri))
         .then(&method(:result))
    end

    def self.call(**args)
      new.(**args)
    end

    private

    def validate_uri(uri_object)
      if uri_object.path.blank? || uri_object.host.blank?
        raise Error::InvalidUrlError,
              translations.(%i[exceptions invalid_url])
      end

      uri_object
    end

    def result(uri_object)
      {
        scheme:   uri_object.scheme,
        host:     uri_object.host,
        path:     uri_object.path,
        query:    uri_object.query,
        base_url: "#{uri_object.scheme}://#{uri_object.host}#{uri_object.path}"
      }
    end
  end
end
