# Instalation ([Home page](../README.md))

## Using docker
Development environment is build using dockers. Make sure to have installed docker SDK on your local machine (if you don't have then visit this link to offical [documentation](https://docs.docker.com/) instalation). Production will not be setup using Dockers.

To run build project run:

```bash
docker-compose build
```

---
To run all services in application located in `docker-compose.yml`

*If you don't want to run containers in the background then remove option `-d`.*

*Puting services names after "up" will run only defined services i.e `docker-compose up -d web`.*
```bash
docker-compose up -d
```

---
To stop all containers run
*Puting services names after "stop" will stop only defined services i.e `docker-compose stop web`*

```bash
docker-compose stop
```

---
To speed up docker on OSX install docker-sync (for more check [documentation](https://docker-sync.readthedocs.io/en/latest/index.html))

```bash
gem install docker-sync
brew install unison
brew install eugenmayer/dockersync/unox

docker-sync start | docker-synch stop | docker-sync clean
```


---
To login to current container run

```bash
docker exec -it CONTAINER_ID /bin/bash
```

***For more usefull commands for docker-compose check official [documentation](https://docs.docker.com/).***


**Summary of development on docker:**
```bash
docker-sync build
docker-compose run --rm web yarn install && bundle exec rake db:create && bundle exec rake db:migrate
docker-compose up -d
```

## Manual installation

  1. Install ruby at least 2.6.1 version (i.e chruby and ruby-install)
  2. Install bundler `gem install bundler`
  3. Install postgres (make sure to run service) and create user
  4. Install redis (make sure to run service)
  5. Install nodeJS
  6. Go to project and bundle dependencies `bundle install`
  6. Run project by running `foreman start`

## Tips

### Postgres
For testing postgres database install `pgcli` in your shell and connect to psql:

```bash
pgcli --host localhost --port 5432 --dbname kitchen_stuff_"environment" --user kitchen_stuff
```
