# frozen_string_literal: true

Warden::Strategies.add(:password, Authentication::PasswordStrategy)

Rails.application.config.middleware.use Warden::Manager do |manager|
  manager.default_strategies :password
  manager.failure_app = lambda do |env|
    SessionsController.action(:new).(env)
  end
end

Warden::Manager.serialize_into_session(&:id)
Warden::Manager.serialize_from_session { |id| User.find(id) }
