import { Controller } from 'stimulus';
import axios from 'axios';

const DECIMAL_SYSTEM = 10;
const NEXT_PAGE = 1;
const ENDPOINTS = {
  buckets: '/buckets/paginate',
  recipes: '/recipes/paginate',
  contacts: '/contacts/paginate',
};

export default class extends Controller {
  static targets = ['container', 'buttonLoader', 'loader', 'input', 'filterContainer', 'submit'];

  initialize() {
    const input = document.querySelector("input[data-target='search.input']");

    if(input) {
      this.validate();
    }
  }

  mapEndpoint(params, endpoint) {
    let string = '';

    Object.keys(params).forEach((key) => {
      string = endpoint.replace(/:[a-zA-Z_]+/, params[key]);
    });

    return string || endpoint;
  }

  validateForm(event) {
    event.preventDefault();

    if(this.validate(event)) {
      event.target.submit();
    }
  }

  validate() {
    if(this.inputTarget.value.length >= 3) {
      this.submitTarget.removeAttribute('disabled');
      return true;
    } else {
      this.submitTarget.setAttribute('disabled', 'disabled');
      return false;
    }
  }

  retrieveRecords(event) {
    event.preventDefault();

    const nextPage = parseInt(this.buttonLoaderTarget.dataset['page'], DECIMAL_SYSTEM) + NEXT_PAGE;
    const resource = this.buttonLoaderTarget.dataset['resource'];
    const params = JSON.parse(this.buttonLoaderTarget.dataset['params']);

    this.loaderTarget.classList.remove('hidden');

    axios
      .get(`${ENDPOINTS[resource]}?page=${nextPage}`, { params: params })
      .then((response) => {
        if(response.data.length === 0 || response.data.length === 1) {
          this.buttonLoaderTarget.remove();
          this.loaderTarget.classList.add('hidden');
          return;
        }

        const domParser = new DOMParser();
        const doc = domParser.parseFromString(response.data, 'text/html');

        this.buttonLoaderTarget.dataset['page'] = nextPage;

        Array.from(doc.body.children).forEach((element) => {
          this.containerTarget.append(element);
        })
        this.loaderTarget.classList.add('hidden');
      })
      .catch((error) => {
        console.warn(error);
      });

  }
}
