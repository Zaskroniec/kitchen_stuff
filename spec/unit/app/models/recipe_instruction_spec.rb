# frozen_string_literal: true

require 'rails_helper'

describe RecipeInstruction, type: :model do
  it { is_expected.to belong_to(:recipe) }

  it { is_expected.to have_db_column(:content).of_type(:text).with_options(null: false) }
  it { is_expected.to have_db_column(:recipe_id).of_type(:uuid).with_options(null: false) }
end
