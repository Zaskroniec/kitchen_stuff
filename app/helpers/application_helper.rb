# frozen_string_literal: true

module ApplicationHelper
  # include Pagy::Frontend

  def not_cached_page?
    current_page?(controller: 'registrations', action: :new) ||
      current_page?(controller: 'contacts', action: :index) ||
      current_page?(controller: 'passwords', action: :recover)
  end

  def current_page
    1
  end

  def default_image(object, size)
    return asset_pack_path('media/images/svgs/empty-meal.svg') if object.try(:image).blank?

    object.image_url(size)
  end

  def allowed_image_extensions
    Constants::FILE_EXTENSIONS.map { |ext| "image/#{ext}" }.join(', ')
  end

  def contact_subjects
    Contact.subjects.map do |key, value|
      [t("views.labels.#{key}"), value]
    end
  end

  def disabled_attribute
    return 'disabled' if current_user.blank?

    false
  end

  def add_nested_field(form_object, assoc)
    html_options = {
      class: 'align-s-fe',
      data:  {
        controller:            'nested-button',
        'nested-button-child': render_assoc(form_object, assoc),
        action:                'click->nested-button#addChild'
      }
    }

    link_to('#', html_options) do
      tag.div(class: 'btn__circled btn__circled__primary btn__circled--plus')
    end
  end

  def remove_nested_item(item)
    html_options = {
      data: {
        controller:                 'nested-button',
        'nested-button-new-record': item.object.new_record?,
        action:                     'click->nested-button#removeChild'
      }
    }

    link_to('#', html_options) do
      tag.div(class: 'btn__circled btn__circled__danger btn__circled--remove btn__circled--very-small')
    end
  end

  def render_assoc(form_object, assoc)
    new_record  = form_object.object.__send__(:"#{assoc}").new
    parent_path = form_object.object.class.name.downcase.pluralize

    form_object.fields_for :"#{assoc}", new_record do |r|
      render("#{parent_path}/nested/#{assoc}", dynamic: true, r: r)
    end
  end
end
