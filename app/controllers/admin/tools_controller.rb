# frozen_string_literal: true

module Admin
  class ToolsController < AdminController
    def style_guide; end

    def new_recipe; end

    def create_recipe
      data = Recipes::Importer::Runner.new.(url: params[:url])

      render json: data, status: :created
    end
  end
end
