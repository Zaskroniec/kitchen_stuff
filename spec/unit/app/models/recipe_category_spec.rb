# frozen_string_literal: true

require 'rails_helper'

describe RecipeCategory, type: :model do
  it { is_expected.to belong_to(:recipe) }
  it { is_expected.to belong_to(:category) }

  it { is_expected.to have_db_index(%i[recipe_id category_id]).unique(true) }

  it { is_expected.to have_db_column(:recipe_id).of_type(:uuid).with_options(null: false) }
  it { is_expected.to have_db_column(:category_id).of_type(:uuid).with_options(null: false) }
end
