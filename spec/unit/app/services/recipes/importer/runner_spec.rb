# frozen_string_literal: true

require 'rails_helper'

module Recipes
  module Importer
    describe Runner do
      describe '#call' do
        subject { described_class.new.(url: url) }

        context 'success' do
          context 'parse script tag' do
            let(:url) { default_recipe_url }
            let(:request_args) do
              {
                method:    :get,
                url:       url,
                options:   {
                  headers: { 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'\
                                             ' (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' }
                },
                resp_code: 200,
                body:      read_example('example_import_html.txt')
              }
            end

            before { mock_request_with_result(request_args) }

            it 'returns parsed name' do
              expect(subject[:name]).to eq('No-bake Raspberry Cheesecake Pots')
            end

            it 'returns parsed image' do
              expect(subject[:image]).to include('https://lh3.googleusercontent.com')
            end

            it 'returns filled description' do
              expect(subject[:description]).not_to be_empty
            end

            it 'returns parsed total time' do
              expect(subject[:total_time][:hours]).to eq('1H')
              expect(subject[:total_time][:minutes]).to be_nil
            end

            it 'returns total servigns' do
              expect(subject[:recipe_yield]).to eq('16 servings')
            end

            it 'returns collection which counts 7 of ingredients' do
              expect(subject[:recipe_ingredient].size).to eq(7)
            end

            it 'returns recipe url' do
              expect(subject[:recipe_url]).to eq(default_recipe_url)
            end
          end

          context 'parse html tags' do
            let(:url) { html_recipe_url }
            let(:request_args) do
              {
                method:    :get,
                url:       url,
                options:   {
                  headers: { 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'\
                                             ' (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' }
                },
                resp_code: 200,
                body:      read_example('example_import_html_tags.txt')
              }
            end

            before { mock_request_with_result(request_args) }

            it 'returns parsed name' do
              expect(subject[:name]).to eq('Roasted Eggplant Pastitsio')
            end

            it 'returns parsed image' do
              expect(subject[:image]).to include('https://images.media-allrecipes.com')
            end

            it 'returns filled description' do
              expect(subject[:description]).not_to be_empty
            end

            it 'returns collection which counts 14 of ingredients' do
              expect(subject[:recipe_ingredient].size).to eq(26)
            end

            it 'returns parsed prep time' do
              expect(subject[:prep_time][:hours]).to be_nil
              expect(subject[:prep_time][:minutes]).to eq('25M')
            end

            it 'returns parsed cook time' do
              expect(subject[:cook_time][:hours]).to eq('1H')
              expect(subject[:cook_time][:minutes]).to eq('45M')
            end

            it 'returns parsed total time' do
              expect(subject[:total_time][:hours]).to eq('2H')
              expect(subject[:total_time][:minutes]).to eq('20M')
            end

            it 'returns filled recipe yield' do
              expect(subject[:recipe_yield]).to eq('12')
            end

            it 'returns filled recipe instructions' do
              expect(subject[:recipe_instructions]).not_to be_empty
            end
          end
        end

        context 'failure' do
          context 'not supported page' do
            let(:url) { not_supported_url }
            let(:request_args) do
              {
                method:    :get,
                url:       url,
                options:   {
                  headers: { 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'\
                                             ' (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' }
                },
                resp_code: 200,
                body:      read_example('empty_import_html.txt')
              }
            end

            before { mock_request_with_result(request_args) }

            it 'returns valid error' do
              expect { subject }.to raise_error(
                Error::ProcessError,
                'We are sorry, but currently we do not support this page.'
              )
            end
          end
        end
      end
    end
  end
end
