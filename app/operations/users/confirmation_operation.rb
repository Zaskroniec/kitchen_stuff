# frozen_string_literal: true

module Users
  class ConfirmationOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
    include DependencyImporter[
      'repositories.user_repo',
      'services.translations',
      'tools.error_message'
    ]

    def call(params:)
      retrieve_user(params[:token])
        .then(&method(:confirm_user))
        .then(&method(:result))
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def retrieve_user(token)
      raise Error::ProcessError, translations.(%i[exceptions invalid_token]) if token.blank?

      user = user_repo.find_by(confirmation_token: token)

      raise Error::ProcessError, translations.(%i[exceptions invalid_token]) if user.blank?

      user
    end

    def confirm_user(user)
      user.update(confirmation_token: nil, confirmed: true)
    end

    def result(_param)
      Success(notice: translations.(%i[flash confirmation]))
    end
  end
end
