# frozen_string_literal: true

require 'rails_helper'

describe Recipe, type: :model do
  it { is_expected.to belong_to(:bucket).counter_cache(true) }
  it { is_expected.to belong_to(:user) }

  it { is_expected.to have_many(:recipe_ingredients).dependent(:destroy) }
  it { is_expected.to have_many(:recipe_instructions).dependent(:destroy) }

  it { is_expected.to have_db_index(%i[recipe_url bucket_id]).unique(true) }
  it { is_expected.to have_db_index(:name) }

  it { is_expected.to have_db_column(:name).of_type(:string).with_options(null: false) }
  it { is_expected.to have_db_column(:image_data).of_type(:text) }
  it { is_expected.to have_db_column(:description).of_type(:text) }
  it { is_expected.to have_db_column(:recipe_url).of_type(:string) }
  it { is_expected.to have_db_column(:servings).of_type(:string) }
  it { is_expected.to have_db_column(:nutrition).of_type(:jsonb).with_options(default: {}, null: false) }
  it { is_expected.to have_db_column(:difficulty).of_type(:integer).with_options(default: 'medium', null: false) }
  it do
    is_expected
      .to have_db_column(:prep_time)
      .of_type(:jsonb)
      .with_options(default: { 'hours' => nil, 'minutes' => nil }, null: false)
  end
  it do
    is_expected
      .to have_db_column(:cook_time)
      .of_type(:jsonb)
      .with_options(default: { 'hours' => nil, 'minutes' => nil }, null: false)
  end
  it do
    is_expected
      .to have_db_column(:total_time)
      .of_type(:jsonb)
      .with_options(default: { 'hours' => nil, 'minutes' => nil }, null: false)
  end
  it { is_expected.to have_db_column(:keywords).of_type(:string) }
  it { is_expected.to have_db_column(:external).of_type(:boolean).with_options(default: false) }
  it { is_expected.to have_db_column(:imported).of_type(:boolean).with_options(default: false) }
  it { is_expected.to have_db_column(:bucket_id).of_type(:uuid).with_options(null: false) }
  it { is_expected.to have_db_column(:user_id).of_type(:uuid).with_options(null: false) }
  it do
    is_expected
      .to have_db_column(:recipe_ingredients_count)
      .of_type(:integer)
      .with_options(default: 0, null: false)
  end

  it { is_expected.to accept_nested_attributes_for(:recipe_ingredients).allow_destroy(true) }
  it { is_expected.to accept_nested_attributes_for(:recipe_instructions).allow_destroy(true) }
  it { is_expected.to accept_nested_attributes_for(:categories).allow_destroy(true) }
  it { is_expected.to accept_nested_attributes_for(:cuisines).allow_destroy(true) }
  it { is_expected.to accept_nested_attributes_for(:recipe_cuisines) }
  it { is_expected.to accept_nested_attributes_for(:recipe_categories) }

  it { is_expected.to define_enum_for(:difficulty).with_values(%i[very_easy easy medium hard very_hard]) }
end
