# frozen_string_literal: true

require 'rails_helper'

describe Contact, type: :model do
  it { is_expected.to belong_to(:user).optional }

  it { is_expected.to have_db_index(:subject) }
  it { is_expected.to have_db_index(:user_id) }

  it { is_expected.to have_db_column(:subject).of_type(:integer).with_options(null: false) }
  it { is_expected.to have_db_column(:email).of_type(:string) }
  it { is_expected.to have_db_column(:content).of_type(:text).with_options(null: false) }
  it { is_expected.to have_db_column(:user_id).of_type(:uuid) }
  it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }

  it { is_expected.to define_enum_for(:subject).with_values(%i[account payments improvement other]) }
end
