# frozen_string_literal: true

module Documents
  module PDFs
    class Generator
      include DependencyImporter[
        'controllers.application_controller',
        'libs.wicked_pdf'
      ]

      PDF_LAYOUT       = 'layouts/pdf.pdf.slim'
      DOCUMENT_OPTIONS = {
        page_size:   'A4',
        orientation: 'Portrait'
      }.freeze

      def call(template_args:, pdf_options:)
        generate_view(template_args)
          .then { |html_string| wicked_pdf.pdf_from_string(html_string, DOCUMENT_OPTIONS.merge(pdf_options)) }
      end

      private

      def generate_view(template_args)
        application_controller.render_to_string(
          layout:   PDF_LAYOUT,
          template: template_args.fetch(:template),
          locals:   template_args.fetch(:locals)
        )
      end
    end
  end
end
