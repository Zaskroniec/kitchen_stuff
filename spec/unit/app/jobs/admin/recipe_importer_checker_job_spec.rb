# frozen_string_literal: true

require 'rails_helper'

module Admin
  describe RecipeImporterCheckerJob, type: :job do
    describe '#perform' do
      let(:user_id) { 'test_id' }
      subject       { described_class.perform_later }

      it 'queue the job' do
        expect { subject }
          .to have_enqueued_job(described_class)
          .on_queue('admin')
      end
    end
  end
end
