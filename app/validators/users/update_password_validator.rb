# frozen_string_literal: true

module Users
  class UpdatePasswordValidator < Users::PasswordValidator
    params do
      required(:token).filled(:string)
    end
  end
end
