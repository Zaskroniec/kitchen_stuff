# frozen_string_literal: true

FactoryBot.define do
  factory :default_user, class: Containers::DependencyContainer['repositories.user_repo'] do
    email                { Faker::Internet.email }
    nick                 { Faker::Internet.username }
    encrypted_password   { Faker::Internet.password }
    admin                { false }
    confirmed            { true }
    confirmation_token   { nil }
    reset_token          { nil }
    reset_token_lifetime { nil }
    reset_token_at       { nil }

    trait(:adimn) do
      admin { true }
    end
  end
end
