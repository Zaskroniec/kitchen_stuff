# Kitchen stuff

## Requirements
***Project build upon Ruby on Rails version 6.0.0.beta1***

- Ruby `2.6.3`
- PostgreSQL `11.4`
- Redis `5.0.3`
- NodeJS `11.1.X`
- Webpacker `4.0`

---

## Business related informations
[Go to business documentation](./documentation/business_logic/guide.md)

## Instalation
[Go to instalation documentation](./documentation/installation.md)

## API documentation

[Go to API documentation](./documentation/api/)
