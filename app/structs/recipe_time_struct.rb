# frozen_string_literal: true

class RecipeTimeStruct < Dry::Struct
  transform_keys(&:to_sym)

  attribute :hours, Types::Nominal::Integer
  attribute :minutes, Types::Nominal::Integer
end
