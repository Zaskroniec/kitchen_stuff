# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include WardenConcern
  include ErrorsConcern

  protect_from_forgery with: :null_session

  before_action :basic_auth

  private

  def basic_auth
    return unless Rails.env.staging?

    authenticate_or_request_with_http_basic do |username, password|
      username == 'kitchen_stuff' && password == '2533d122733b4037b03307ccf02c4b09'
    end
  end

  def redirect_to_new_sesseion
    redirect_to new_sessions_path if unsigned?
  end

  def redirect_to_my_buckets
    redirect_to buckets_path if signed_in?
  end

  def unsafe_params
    params.to_unsafe_h.deep_symbolize_keys
  end

  def render_error(action, error, options = {})
    flash.now[:error] = error[:message] if error[:message].is_a?(String)
    @errors = error[:message][:errors] if error[:message].is_a?(Hash)
    render(action, **options)
  end
end
