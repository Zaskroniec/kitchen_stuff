import { Controller } from 'stimulus';

export default class extends Controller {
  static targets = [ 'input' ]

  initialize() {
    const showDialog = this.data.get('showDialog');

    if(showDialog === 'true') {
      this.element.setAttribute('open', null);
    }
  }

  close() {
    const errorFormList = this.element.querySelector('ul');
    const emptyInput = this.data.get('cleanInput');

    this.element.removeAttribute('open');

    if(emptyInput === 'true') { this.inputTarget.value = null; }
    if(errorFormList) { errorFormList.remove(); }
  }
}
