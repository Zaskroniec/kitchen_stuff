# frozen_string_literal: true

require 'rails_helper'

module Users
  describe ResetPasswordOperation do
    describe '#call' do
      let(:user_repo) { double(:user_repo) }
      subject { described_class.new(user_repo: user_repo).(params: params) }

      context 'success' do
        let(:token)       { '1234' }
        let(:user_entity) { double(:user, id: random_uuid) }
        let(:params) do
          {
            email:                  Faker::Internet.email,
            'g-recaptcha-response': token
          }
        end
        let(:request_args) do
          {
            method:    :post,
            url:       Constants::CAPTCHA_URL,
            options:   {
              body: {
                response: token,
                secret:   Credentials[:google, :captcha_secret_key]
              }
            },
            resp_code: 200,
            body:      { 'success' => true }.to_json,
            headers:   { 'Content-Type' => 'application/json' }
          }
        end

        before do
          mock_request_with_result(request_args)

          expect(user_repo).to receive(:find_by_email).with(params[:email]).and_return(user_entity)
        end

        it 'returns notice message' do
          expect(subject.success[:notice]).to eq(
            "If your email #{params[:email]} was provided correctly "\
            'then you should get an email with reset password link.'
          )
        end

        it 'enqueue new job with confirmation instructions' do
          expect { subject }.to enqueue_job(Users::ResetPasswordJob)
        end
      end

      context 'failure' do
        context 'validation' do
          context 'empty params' do
            let(:params) { {} }

            it 'returns errors' do
              expect(subject.failure[:message]).to be_present
            end

            it 'not enqueue new job with reset_password instructions' do
              expect { subject }.not_to enqueue_job(Users::ResetPasswordJob)
            end
          end
        end
      end
    end
  end
end
