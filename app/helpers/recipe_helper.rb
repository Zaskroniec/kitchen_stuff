# frozen_string_literal: true

module RecipeHelper
  def recipe_time_object(recipe, type)
    Containers::DependencyContainer['structs.recipe_time_struct'].new(recipe.__send__(type))
  end

  def recipe_difficulties
    Recipe.difficulties.map do |key, value|
      [t("views.labels.#{key}"), value]
    end
  end

  def imported?(recipe)
    recipe.external && !recipe.imported
  end

  def safe_back_recipe_path(bucket)
    return bucket_path(bucket.slug) if current_page?(request.referer)

    :back
  end
end
