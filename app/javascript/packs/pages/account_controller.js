import { Controller } from 'stimulus';

const DEFAULT_SECTION = 'statistics';
const HASH_SYMBOL = 1;
const SCROLL_RENDER_AFTER = 10;

export default class extends Controller {
  initialize() {
    const id = window.location.hash;

    if(id) {
      const parsedId = id.substring(HASH_SYMBOL);

      this.showSection(parsedId);
      this.scrollUp();
      return;
    }

    this.showSection(DEFAULT_SECTION);
  }

  selectSection(event) {
    event.preventDefault();

    const buttonId = event.target.parentNode.dataset['section'];
    const windowPath = window.location.pathname;
    const windowOrigin = window.location.origin;
    const anchorLocation = `${windowOrigin}${windowPath}#${buttonId}`;

    if(window.location.href !== anchorLocation) {
      window.location.href = anchorLocation;
    }

    this.showSection(buttonId);
  }

  showSection(id) {
    const element = document.getElementById(id);
    const sections = document.querySelectorAll(`.account__action-body:not(#${id})`);

    if(element === null) {
      this.showSection(DEFAULT_SECTION);
      return;
    }

    sections.forEach((section) => {
      section.classList.add('hidden');
    });

    element.classList.remove('hidden');
  }

  scrollUp() {
    setTimeout(() => {
      window.scrollTo(0, 0);
    }, SCROLL_RENDER_AFTER);
  }
}
