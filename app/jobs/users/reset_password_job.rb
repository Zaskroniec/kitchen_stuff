# frozen_string_literal: true

module Users
  class ResetPasswordJob < ApplicationJob
    queue_as :mailers

    def perform(email:)
      reset_password_generator.(email: email)
    end

    private

    def reset_password_generator
      @reset_password_generator ||= dependency_container['services.users.generate_reset_password_instructions']
    end
  end
end
