# frozen_string_literal: true

require 'rails_helper'

module Contacts
  describe CreateOperation do
    describe '#call' do
      let(:email)        { Faker::Internet.email }
      let(:contact_repo) { double(:contact_repo) }
      subject            { described_class.new(contact_repo: contact_repo).(params: params, user: user) }

      context 'success' do
        let(:contact) { double(:contact) }
        let(:params) do
          {
            subject:                '1',
            email:                  email,
            content:                Faker::Lorem.paragraph,
            'g-recaptcha-response': '1234'
          }
        end
        let(:request_args) do
          {
            method:    :post,
            url:       Constants::CAPTCHA_URL,
            options:   {
              body: {
                response: params[:'g-recaptcha-response'],
                secret:   Credentials[:google, :captcha_secret_key]
              }
            },
            resp_code: 200,
            body:      { 'success' => true }.to_json,
            headers:   { 'Content-Type' => 'application/json' }
          }
        end

        before do
          mock_request_with_result(request_args)

          expect(contact_repo).to receive(:create).and_return(contact)
        end

        context 'with user' do
          let(:user)  { double(:user, id: random_uuid, email: email) }

          it 'returns notice message' do
            expect(subject.success[:notice]).to eq('Successfuly sent message to our support.')
          end
        end

        context 'without user' do
          let(:user) { nil }

          it 'returns notice message' do
            expect(subject.success[:notice]).to eq('Successfuly sent message to our support.')
          end
        end
      end

      context 'failure' do
        let(:user) { nil }

        context 'validation' do
          context 'empty params' do
            let(:params) { {} }

            it 'returns valid errors' do
              expect(subject.failure[:message]).to be_present
            end
          end

          context 'invalid subject' do
            let(:params) do
              {
                subject:                '10',
                email:                  email,
                content:                Faker::Lorem.paragraph,
                'g-recaptcha-response': '1234'
              }
            end

            it 'returns valid errors' do
              expect(subject.failure[:message][:errors][:subject]).to eq(['Invalid subject.'])
            end
          end
        end
      end
    end
  end
end
