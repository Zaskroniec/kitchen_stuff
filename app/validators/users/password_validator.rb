# frozen_string_literal: true

module Users
  class PasswordValidator < ApplicationValidator
    params do
      required(:password).filled(:string, size?: 8..32, format?: Constants::PASSWORD_FORMAT)
      required(:password_confirmation).maybe(:string)
    end

    rule(:password_confirmation).validate(:match_passwords?)
  end
end
