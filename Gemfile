source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

gem 'rails', '~> 6.0.0.rc2'
gem 'breadcrumbs_on_rails'
gem 'pg', '>= 0.18', '< 2.0'
gem 'bcrypt', '~> 3.1.13'
gem 'pagy', '~> 3.4.1'

# Background processes
gem 'sidekiq', '~> 5.2'
gem 'sidekiq-scheduler', '~> 3.0.0'

gem 'slim', '>= 4'
gem 'turbolinks', '~> 5.2'
gem 'webpacker', '>= 4.0'

# web server
gem 'falcon', '>= 0.34'

gem 'bootsnap', '>= 1.4', require: false

gem 'warden', '~> 1.2.8'

# SEO
gem 'meta-tags', '~> 2.11'

# pdf lib
gem 'wicked_pdf', '~> 1.4'

# Search engine
gem 'pg_search', '~> 2.3'

# error notifier
gem 'rollbar', '>= 2.19'

# Uploader dependencies
gem 'shrine', '>= 2.1'
gem 'aws-sdk-s3', '~> 1.45'
gem 'fastimage', '>= 2.1'
gem 'mini_magick', '>= 4.9'
gem 'mime-types', '>= 3.2'
gem 'image_processing', '~> 1.9.2'

# Html/xml parser
gem 'oga', glob: 'yorickpeterse/oga'
gem 'httparty', '~> 0.17'

# Dry gems
gem 'dry-struct', '~> 1.0'
gem 'dry-container', '~> 0.7.2'
gem 'dry-auto_inject', '~> 0.6.1'
gem 'dry-monads', '~> 1.2.0'
gem 'dry-matcher', '~> 0.7'
gem 'dry-validation', '~> 1.2.1'

group :development, :test, :ci do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rubocop', '>= 0.73'
  gem 'rubocop-performance', '>= 1.0'
  gem 'brakeman', '>= 4.5'
  gem 'fasterer', '>= 0.6'
  gem 'pry-rails', '~> 0.3.9'
  gem 'awesome_print', '>= 1.8'
  gem 'factory_bot_rails', '~> 5.0.2'
  gem 'faker', '~> 1.9.6'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring', '~> 2.1.0'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'letter_opener', '~> 1.7.0'
  gem 'foreman'
  gem 'puma'
  gem 'guard'
  gem 'guard-brakeman'
  gem 'guard-rubocop'
  gem 'guard-rspec'
  gem 'bullet', '~> 6.0.1'
  # gem 'rack-mini-profiler', '~> 1.0.2'

  gem 'capistrano-rails'
end

group :test, :ci do
  gem 'thin', '>= 1.7'
  gem 'rspec-rails', '~> 3.8.2'
  gem 'shoulda-matchers', '>= 4.0'
  gem 'simplecov', '~> 0.17', require: false
  gem 'database_cleaner', '~> 1.7.0'
  gem 'percy-capybara', '>= 3.2'
  gem 'webmock', '>= 3.6'
  gem 'capybara', '>= 3.1'
  gem 'selenium-webdriver', '~> 3.142.3'
  gem 'webdrivers', '>= 2.1'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
