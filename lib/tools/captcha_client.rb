# frozen_string_literal: true

module Tools
  class CaptchaClient
    include DependencyImporter[
      'services.translations',
      'libs.httparty'
    ]

    def call(token:)
      httparty
        .post(Constants::CAPTCHA_URL, build_options(token))
        .then(&method(:result))
    rescue Error::SocketError
      raise Error::ProcessError, translations.(%i[exceptions socket_connection])
    end

    def self.call(**args)
      new.(**args)
    end

    private

    def build_options(token)
      {
        body: {
          secret:   Credentials[:google, :captcha_secret_key],
          response: token
        }
      }
    end

    def result(response)
      return 'Ok' if response['success']

      raise Error::ProcessError, translations.(%i[exceptions invalid_captcha])
    end
  end
end
