# frozen_string_literal: true

module Users
  class ResetPasswordValidator < ApplicationValidator
    params do
      required(:email).value(:string, format?: Constants::EMAIL_FORMAT, max_size?: Constants::MAX_STRING_LENGTH)
      required(:'g-recaptcha-response').value(:string)
    end
  end
end
