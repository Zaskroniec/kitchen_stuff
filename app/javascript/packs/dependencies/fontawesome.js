import 'images/svgs/plus-light-green.svg';
import 'images/svgs/external-link-alt-light-green.svg';
import 'images/svgs/pen-light-green.svg';

import 'images/svgs/meal.svg';
import 'images/svgs/user-plus.svg';
import 'images/svgs/mail-bulk.svg';
import 'images/svgs/frown.svg';
import 'images/svgs/empty-meal.svg';
import 'images/svgs/arrow-alt-circle-up.svg';
import 'images/svgs/twitter.svg';
import 'images/svgs/facebook.svg';

import 'images/svgs/plus-green.svg';
import 'images/svgs/times-green.svg';
import 'images/svgs/external-link-alt-green.svg';
import 'images/svgs/window-close-green.svg';
import 'images/svgs/pen-green.svg';
import 'images/svgs/check-green.svg';
import 'images/svgs/search-green.svg';
import 'images/svgs/spinner-green.svg';

import 'images/svgs/plus-red.svg';
import 'images/svgs/times-red.svg';
import 'images/svgs/exclamation-red.svg';
import 'images/svgs/window-close-red.svg';

import 'images/svgs/times-light-red.svg';
import 'images/svgs/sitemap-white.svg';
import 'images/svgs/sign-in-alt-white.svg';
import 'images/svgs/sign-out-alt-white.svg';
import 'images/svgs/column-white.svg';
import 'images/svgs/ellipsis-v-white.svg';
import 'images/svgs/cogs-white.svg';
