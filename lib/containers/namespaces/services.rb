# frozen_string_literal: true

module Containers
  module Namespaces
    Services = Dry::Container::Namespace.new('services') do
      register(:token_generator)  { TokenGenerator.new }
      register(:translations)     { Translations.new }
      register(:time_parser)      { TimeParser.new }
      register(:tag_generator)    { TagGenerator.new }
      register(:static_meta_tags) { StaticMetaTags.new }

      namespace(:admin) do
        register(:importer_checker) { Admin::ImporterChecker.new }
      end

      namespace(:documents) do
        namespace(:pdfs) do
          register(:generator) { Documents::PDFs::Generator.new }
        end
      end

      namespace(:users) do
        register(:generate_confirmation_instructions)   { Users::GenerateConfirmationInstruction.new }
        register(:generate_reset_password_instructions) { Users::GenerateResetPasswordInstructions.new }
        register(:statistics)                           { Users::Statistics.new }
      end

      namespace(:buckets) do
        register(:generate_starting_buckets) { Buckets::GenerateStartingBuckets.new }
      end

      namespace(:recipes) do
        register(:time_calculator) { Recipes::TimeCalculator.new }

        namespace(:importer) do
          register(:runner)      { Recipes::Importer::Runner.new }
          register(:updater)     { Recipes::Importer::Updater.new }
          register(:data_mapper) { Recipes::Importer::DataMapper.new }

          namespace(:retrievers) do
            register(:source_retriever)      { Recipes::Importer::Retrievers::SourceRetriever.new }
            register(:script_data_retriever) { Recipes::Importer::Retrievers::ScriptDataRetriever.new }
            register(:html_data_retriever)   { Recipes::Importer::Retrievers::HtmlDataRetriever.new }
          end
        end
      end
    end
  end
end
