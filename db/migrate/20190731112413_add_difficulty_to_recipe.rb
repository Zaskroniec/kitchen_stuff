# frozen_string_literal: true

class AddDifficultyToRecipe < ActiveRecord::Migration[6.0]
  def change
    add_column :recipes, :difficulty, :integer, null: false, default: 2
  end
end
