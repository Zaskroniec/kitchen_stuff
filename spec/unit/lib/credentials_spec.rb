# frozen_string_literal: true

require 'rails_helper'

describe Credentials do
  describe '#self.[]' do
    subject { described_class[*keys] }

    context 'success' do
      context 'with present environment key' do
        let(:keys) { %i[test db dbname] }

        it 'returs valid dbname' do
          expect(subject).to eq('kitchen_stuff_test')
        end
      end

      context 'without environment key' do
        let(:keys) { %i[db dbname] }

        it 'returs valid dbname' do
          expect(subject).to eq("kitchen_stuff_#{Rails.env}")
        end
      end
    end

    context 'failure' do
      let(:keys) { %i[test test] }

      it 'raises error on invalid key' do
        expect { subject }.to raise_error(
          ArgumentError,
          "Credentials invalid: #{keys}"
        )
      end
    end
  end
end
