# frozen_string_literal: true

if Rails.env.production? || Rails.env.staging?
  S3_OPTIONS = {
    bucket:            Credentials[:aws, :bucket],
    access_key_id:     Credentials[:aws, :public_key],
    secret_access_key: Credentials[:aws, :secret_key],
    region:            Credentials[:aws, :region]
  }.freeze

  URL_OPTIONS = {
    public: true,
    host: Credentials[:aws, :host]
  }.freeze

  Shrine.storages = {
    cache: Shrine::Storage::S3.new(prefix: 'cache', upload_options: { acl: 'public-read' }, **S3_OPTIONS),
    store: Shrine::Storage::S3.new(prefix: 'store', upload_options: { acl: 'public-read' }, **S3_OPTIONS)
  }
else
  require 'shrine/storage/file_system'

  Shrine.storages = {
    cache: Shrine::Storage::FileSystem.new('public', prefix: 'cache'),
    store: Shrine::Storage::FileSystem.new('public', prefix: 'store')
  }
end

Shrine.plugin :activerecord
Shrine.plugin :default_url_options, cache: URL_OPTIONS, store: URL_OPTIONS if Rails.env.production? || Rails.env.staging?
Shrine.plugin :cached_attachment_data
Shrine.plugin :restore_cached_data
Shrine.plugin :remote_url, max_size: Constants::MAX_REMOTE_FILE
Shrine.plugin :infer_extension
Shrine.plugin :instrumentation
Shrine.plugin :store_dimensions
Shrine.plugin :determine_mime_type
Shrine.plugin :processing
Shrine.plugin :versions
Shrine.plugin :delete_raw
Shrine.plugin :upload_endpoint
Shrine.plugin :recache
Shrine.plugin :backgrounding

Shrine::Attacher.promote { |data| PromoteJob.perform_later(data: data) }
Shrine::Attacher.delete { |data| DeleteJob.perform_later(data: data) }
