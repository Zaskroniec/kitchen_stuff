# frozen_string_literal: true

class CreateRecipeCuisines < ActiveRecord::Migration[6.0]
  def change
    create_table :recipe_cuisines, id: :uuid do |t|
      t.belongs_to :recipe,  type: :uuid, foreign_key: true, null: false
      t.belongs_to :cuisine, type: :uuid, foreign_key: true, null: false

      t.timestamps
    end

    add_index :recipe_cuisines, %i[recipe_id cuisine_id], unique: true
  end
end
