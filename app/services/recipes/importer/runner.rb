# frozen_string_literal: true

module Recipes
  module Importer
    class Runner
      include DependencyImporter[
        'services.recipes.importer.retrievers.source_retriever',
        'services.recipes.importer.retrievers.script_data_retriever',
        'services.recipes.importer.retrievers.html_data_retriever',
        'services.translations',
        'tools.url_parser',
      ]

      def call(url:)
        url_object      = url_parser.(url: url)
        document_object = source_retriever.(url: url_object[:base_url])

        generate_recipe_data(document_object, url_object[:base_url])
      end

      private

      def generate_recipe_data(document_object, base_url)
        document_object
          .then { |source|  source.css(Constants::SCRIPT_TAG) }
          .then { |scripts| scripts.detect(&method(:contains_script_main_key?)) }
          .then { |script|  run_data_retriever(script, document_object) }
          .then { |data|    result(data, base_url) }
      end

      def run_data_retriever(script, document_object)
        return script_data_retriever.(raw_data: script) if script.present?
        return html_data_retriever.(raw_data: document_object) if contains_html_main_prop?(document_object)

        # TODO: add custom ingredient retrievers i.e for kwestiasmaku.pl

        raise Error::ProcessError,
              translations.(%i[exceptions external_api import_not_supported])
      end

      def result(data, base_url)
        data.merge(recipe_url: base_url)
      end

      def contains_html_main_prop?(raw_data)
        raw_data.css(Constants::HTML_RECIPE_INGREDIENT_ATTRIBUTE).text.present? ||
          raw_data.css(Constants::HTML_INGREDIENT_ATTRIBUTE).text.present? ||
          raw_data.css(Constants::HTML_NAME_ATTRIBUTE).text.present?
      end

      def contains_script_main_key?(node)
        node.children.text.include?(Constants::HTML_AND_SCRIPT_TAG)
      end
    end
  end
end
