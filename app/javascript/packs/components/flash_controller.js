import { Controller } from 'stimulus';

export default class extends Controller {
  close() {
    const duration = 1500;

    this.removeElement();
    this.startTimer(duration);
  }

  removeElement() {
    this.element.classList.add('flash-closed');
  }

  startTimer(miliseconds) {
    let timeout = setTimeout(() => {
      this.element.remove();

      clearTimeout(timeout);
    }, miliseconds);
  }
};
