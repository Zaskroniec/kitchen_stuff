import { Controller } from 'stimulus';

export default class extends Controller {
  static targets = ['suggestionDestroyField'];

  removeSuggestion(event) {
    event.preventDefault();

    const isNewRecord = this.data.get('newRecord');
    const parent = this.element.parentNode;

    if(isNewRecord === 'true') {
      parent.removeChild(this.element)
      return;
    }

    this.element.classList.add('removed');
    this.suggestionDestroyFieldTarget.value = true;
  }
}
