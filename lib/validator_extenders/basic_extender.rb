# frozen_string_literal: true

module ValidatorExtenders
  module BasicExtender
    private

    def unique_field?(repo, query, id)
      return repo.find_by(query).blank? if id.blank?

      repo.where(query).where.not(id: id).first.blank?
    end

    def no_file_extension?(value)
      !Constants::URL_PATH_FORMAT.match?(value)
    end
  end
end
