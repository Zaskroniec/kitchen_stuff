# frozen_string_literal: true

module Users
  class Statistics
    include DependencyImporter[
      'tools.time_formatter',
      'libs.uri'
    ]

    def call(user:)
      buckets ||= user.buckets

      {
        base:           {
          number_of_buckets: number_of_buckets(buckets),
          number_of_recipes: number_of_recipes(buckets),
          date_from:         date_from(user)
        },
        favourite_urls: favourite_urls(user)
      }
    end

    private

    def number_of_buckets(buckets)
      buckets.size
    end

    def number_of_recipes(buckets)
      buckets
        .pluck(:recipes_count)
        .inject(:+)
    end

    def date_from(user)
      time_formatter.full_time(value: user.created_at)
    end

    def favourite_urls(user)
      user
        .recipes
        .imported
        .map      { |r| { host: uri.parse(r.recipe_url).host, url: r.recipe_url } }
        .group_by { |r| r[:host] }
        .map      { |key, value| { url: key, size: value.size } }
        .sort_by  { |hash| hash[:size] }
        .reverse!
        .first(5)
    end
  end
end
