# frozen_string_literal: true

class TagGenerator
  include DependencyImporter[
    'services.translations',
    'libs.kernel'
  ]

  GENERATOR_NAME_KEY = :name

  # rubocop:disable Metrics/CyclomaticComplexity
  # rubocop:disable Metrics/PerceivedComplexity
  def call(hash:, target:, result:)
    result_hash = { "#{result}": [] }

    return [hash, result_hash] if hash[target].blank?

    hash[target] = hash[target].map do |h|
      if h.key?(:id) && h.key?(:_destroy) && !h[:_destroy]
        nil
      elsif h.key?(:id) && h.key?(:_destroy) && h[:_destroy]
        h
      elsif !h.key?(:id) && h.key?(:name)
        generate_indirect_relation(h, target, result_hash, result)
        nil
      end
    end.compact

    [hash, result_hash]
  end
  # rubocop:enable Metrics/CyclomaticComplexity
  # rubocop:enable Metrics/PerceivedComplexity

  private

  def generate_indirect_relation(hash, target, result_hash, result)
    klass_name = target.to_s.split('_').first.singularize

    kernel
      .const_get(klass_name.capitalize)
      .then { |klass|  klass.find_or_create_by(name: hash[GENERATOR_NAME_KEY].upcase) }
      .then { |record| result_hash[result] << { "#{klass_name}_id": record.id } }
  end
end
