# frozen_string_literal: true

ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __dir__)

require 'bundler/setup' # Set up gems listed in the Gemfile.
require 'bootsnap/setup' # Speed up boot time by caching expensive operations.

require 'rollbar'
require 'shrine'
require 'shrine/storage/s3'
require 'dry/monads/result'
require 'dry/matcher/result_matcher'

require_relative File.expand_path('./lib/credentials')
require_relative File.expand_path('./lib/error')
require_relative File.expand_path('./lib/constants')
