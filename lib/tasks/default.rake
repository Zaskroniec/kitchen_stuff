# frozen_string_literal: true

desc 'Run all services'
task default: :environment do
  system('bundle exec brakeman && bundle exec fasterer && bundle exec rubocop && rake specs:unit')
end
