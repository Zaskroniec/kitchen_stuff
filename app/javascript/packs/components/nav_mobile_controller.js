import { Controller } from 'stimulus';

export default class extends Controller {
  closeNav() {
    if(this.element.hasAttribute('open')) {
      this.element.removeAttribute('open');
    }
  }
}
