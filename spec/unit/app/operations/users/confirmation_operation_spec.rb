# frozen_string_literal: true

require 'rails_helper'

module Users
  describe ConfirmationOperation do
    describe '#call' do
      let(:user_repo) { double(:user_repo) }
      let(:user)      { double(:user, confirmation_token: token) }
      let(:token)     { '1234' }
      let(:params)    { { token: token } }
      subject         { described_class.new(user_repo: user_repo).(params: params) }

      context 'success' do
        before do
          expect(user_repo)
            .to receive(:find_by)
            .with(confirmation_token: token)
            .and_return(user)

          expect(user)
            .to receive(:update)
            .with(confirmation_token: nil, confirmed: true)
            .and_return(user)
        end

        it 'returns valid message' do
          expect(subject.success[:notice]).to eq('Successfuly confirmed account, you can now login to Kitchen Stuff')
        end
      end

      context 'failure' do
        context 'token param is blank' do
          let(:token) { '' }

          it 'returns invalid token error' do
            expect(subject.failure[:message])
              .to eq('We are sorry, but the token provided from the url is invalid.')
          end
        end

        context 'user not found' do
          before do
            expect(user_repo)
              .to receive(:find_by)
              .with(confirmation_token: token)
              .and_return(nil)
          end

          it 'returns token error' do
            expect(subject.failure[:message])
              .to eq('We are sorry, but the token provided from the url is invalid.')
          end
        end
      end
    end
  end
end
