# frozen_string_literal: true

class AddAdminFlagToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :admin, :boolean, default: false, null: false

    add_index :users, :admin, name: 'index_users_on_non_admins', where: '(admin IS FALSE)'
    add_index :users, :admin, name: 'index_users_on_admins', where: '(admin IS TRUE)'
  end
end
