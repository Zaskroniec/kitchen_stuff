# frozen_string_literal: true

module Containers
  module Namespaces
    Libs = Dry::Container::Namespace.new('libs') do
      register(:uri)           { URI }
      register(:oga)           { Oga }
      register(:json)          { JSON }
      register(:httparty)      { HTTParty }
      register(:i18n)          { I18n }
      register(:bcrypt)        { BCrypt }
      register(:secure_random) { SecureRandom }
      register(:kernel)        { Kernel }
      register(:duration)      { ActiveSupport::Duration }
      register(:wicked_pdf)    { WickedPdf.new }
    end
  end
end
