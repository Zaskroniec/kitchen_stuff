# frozen_string_literal: true

module Users
  class GenerateResetPasswordInstructions
    include DependencyImporter[
      'mailers.user_mailer',
      'repositories.user_repo',
      'services.token_generator'
    ]

    def call(email:)
      token_generator
        .(repo: user_repo, attribute: 'reset_token')
        .then { |token| update_user(token, email) }
        .then { |user|  user_mailer.with(user: user) }
        .then(&:reset_password_email)
        .then(&:deliver_now)
    end

    private

    def update_user(token, email)
      user_repo.find_by_email(email).tap do |user|
        user.update(reset_token: token, reset_token_lifetime: token_lifetime)
      end
    end

    def token_lifetime
      Time.zone.now + 7.days
    end
  end
end
