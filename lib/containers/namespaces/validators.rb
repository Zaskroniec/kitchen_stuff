# frozen_string_literal: true

module Containers
  module Namespaces
    Validators = Dry::Container::Namespace.new('validators') do
      namespace(:users) do
        register(:new_user_validator) do
          Users::NewUserValidator.new(user_repo: self['repositories.user_repo'])
        end
        register(:reset_password_validator)         { Users::ResetPasswordValidator.new }
        register(:update_password_validator)        { Users::UpdatePasswordValidator.new }
        register(:inline_update_password_validator) { Users::InlineUpdatePasswordValidator.new }
      end

      namespace(:contacts) do
        register(:contact_validator) { Contacts::ContactValidator.new }
      end

      namespace(:buckets) do
        register(:bucket_validator) { Buckets::BucketValidator.new(bucket_repo: self['repositories.bucket_repo']) }
      end

      namespace(:recipes) do
        register(:importer_validator) do
          Recipes::ImporterValidator.new(recipe_repo: self['repositories.recipe_repo'])
        end

        register(:recipe_validator) do
          Recipes::RecipeValidator.new(recipe_repo: self['repositories.recipe_repo'])
        end

        register(:data_mapper_validator) { Recipes::DataMapperValidator.new }
      end
    end
  end
end
