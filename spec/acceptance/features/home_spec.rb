# frozen_string_literal: true

require_relative '../acceptance_helper'

describe 'As a guest I can see home page:', type: :feature do
  it 'shows page' do
    visit root_path

    expect(page.find('body').text).to have_content('Hello')

    Percy::Capybara.snapshot(page, name: SNAPSHORTS[:guest_home])
  end
end
