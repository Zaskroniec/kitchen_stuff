# frozen_string_literal: true

module Users
  class ResetPasswordOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
    include DependencyImporter[
      'repositories.user_repo',
      'validators.users.reset_password_validator',
      'jobs.users.reset_password_job',
      'services.translations',
      'tools.captcha_client',
      'tools.error_message'
    ]

    def call(params:)
      validate_params(params)
        .then(&method(:verify_captcha))
        .then(&method(:schedule_reset_instructions))
        .then(&method(:result))
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def validate_params(params)
      reset_password_validator.(params).tap do |validator|
        raise Error::ProcessError, { errors: validator.errors.to_h }.to_json if validator.failure?
      end.to_h
    end

    def verify_captcha(output)
      output.tap { |origin_output| captcha_client.(token: origin_output.delete(:'g-recaptcha-response')) }
    end

    def schedule_reset_instructions(output)
      output.tap do
        reset_password_job.perform_later(email: output[:email]) if user_repo.find_by_email(output[:email]).present?
      end
    end

    def result(output)
      Success(notice: translations.(%i[flash reset_password], email: output[:email]))
    end
  end
end
