# frozen_string_literal: true

require 'rails_helper'

module Recipes
  describe ImportOperation do
    describe '#call' do
      let(:bucket)             { double(:bucket, id: random_uuid, user_id: random_uuid) }
      let(:recipe_repo)        { double(:recipe_repo) }
      let(:url)                { default_recipe_url }
      let(:params)             { { url: url, bucket_id: bucket.id } }
      let(:importer_validator) { Recipes::ImporterValidator.new(recipe_repo: recipe_repo) }

      subject { described_class.new(importer_validator: importer_validator).(params: params, bucket: bucket) }

      context 'success' do
        let(:data) do
          {
            user_id:    bucket.user_id,
            recipe_url: 'https://www.allrecipes.com/recipe/132815/balsamic-roasted-pork-loin',
            external:   true,
            name:       'Imported Recipe'
          }
        end
        let(:recipe) do
          double(:recipe,
                 id:         'test_id',
                 recipe_url: data[:recipe_url],
                 external:   data[:external],
                 name:       data[:name])
        end

        before do
          expect(recipe_repo)
            .to receive(:find_by).with(recipe_url: url, bucket_id: params[:bucket_id]).and_return([])
          expect(bucket).to      receive(:recipes).and_return(recipe_repo)
          expect(recipe_repo).to receive(:create).with(data).and_return(recipe)
        end

        it 'schedule new job' do
          expect { subject }.to enqueue_job(Recipes::ImporterJob)
        end

        it 'returns notice message' do
          expect(subject.success[:notice]).to eq('Successfuly imported recipe from external url.')
        end

        it 'returns record with id' do
          expect(subject.success[:record].id).to be_present
        end

        it 'returns record with valid url' do
          expect(subject.success[:record].recipe_url)
            .to eq('https://www.allrecipes.com/recipe/132815/balsamic-roasted-pork-loin')
        end
      end

      context 'failure' do
        context 'validation' do
          context 'contains file extension' do
            let(:url) { "#{default_recipe_url}.jpg" }

            before do
              expect(recipe_repo)
                .to receive(:find_by).with(recipe_url: url, bucket_id: params[:bucket_id]).and_return([])
            end

            it 'returns valid error message' do
              expect(subject.failure[:message])
                .to eq('We are sorry, but the link you provided contains corrupted data.')
            end
          end

          context 'not unique' do
            let(:recipe) { double(:recipe) }

            before do
              expect(recipe_repo)
                .to receive(:find_by).with(recipe_url: url, bucket_id: params[:bucket_id]).and_return([recipe])
            end

            it 'returns valid error message' do
              expect(subject.failure[:message])
                .to eq('We are sorry, but you cannot create a recipe from the same external url in this bucket.')
            end
          end
        end
      end
    end
  end
end
