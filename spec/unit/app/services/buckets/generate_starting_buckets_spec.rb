# frozen_string_literal: true

require 'rails_helper'

module Buckets
  describe GenerateStartingBuckets, type: :job do
    describe '#perform' do
      let(:user_id)     { random_uuid }
      let(:bucket_repo) { double(:bucket_repo) }
      let(:user)        { double(:user, id: user_id) }
      let(:user_repo)   { double(:user_repo) }
      let(:validator)   { Buckets::BucketValidator.new(bucket_repo: bucket_repo) }
      let(:operation)   { Buckets::CreateOperation.new(bucket_validator: validator) }
      subject           { described_class.new(user_repo: user_repo, create_operation: operation).(user_id: user_id) }

      before do
        expect(user_repo).to receive(:find).with(user_id).and_return(user)

        Constants::DEFAULT_USER_BUCKETS.each do |bucket_name|
          bucket_entity = double(:bucket_entity)
          bucket        = double(:bucket, id: SecureRandom.uuid)
          params        = { name: bucket_name, slug: bucket_name.downcase }

          expect(user).to          receive(:buckets).and_return(bucket_entity)
          expect(bucket_repo).to   receive(:find_by).with(name: params[:name], user_id: user_id).and_return([])
          expect(bucket_entity).to receive(:create).with(params).and_return(bucket)
        end
      end

      it 'returns collection of 5 ids' do
        expect(subject.size).to eq(5)
      end
    end
  end
end
