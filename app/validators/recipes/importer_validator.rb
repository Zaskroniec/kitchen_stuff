# frozen_string_literal: true

module Recipes
  class ImporterValidator < ApplicationValidator
    include ValidatorExtenders::BasicExtender

    option :recipe_repo

    params do
      required(:scheme).value(:string, format?: Constants::REQUEST_FORMAT)
      required(:host).value(:string)
      required(:path).value(:string)
      required(:bucket_id).value(:string)
      required(:base_url).value(:string, format?: Constants::REQUEST_FORMAT)
    end

    rule(:path) do
      unless no_file_extension?(value)
        key.failure(translations.(%i[errors no_file_extension?]))
      end
    end

    rule(:base_url) do
      unless unique_field?(recipe_repo, { recipe_url: value, bucket_id: values.data[:bucket_id] }, nil)
        key.failure(translations.(%i[errors unique_field?]))
      end
    end
  end
end
