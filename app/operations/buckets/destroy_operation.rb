# frozen_string_literal: true

module Buckets
  class DestroyOperation
    include Dry::Monads::Result::Mixin
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
    include DependencyImporter[
      'services.translations',
      'tools.error_message'
    ]

    def call(bucket:)
      bucket
        .then(&method(:destroy_bucket))
        .then(&method(:result))
    rescue Error => e
      Failure(error_message.(error: e))
    end

    private

    def destroy_bucket(bucket)
      ActiveRecord::Base.transaction do
        return translations.(%i[flash destroy_bucket], name: bucket.name) if bucket.destroy

        raise Error::ProcessError, translations.(%i[exceptions destroy_bucket], name: bucket.name)
      end
    end

    def result(message)
      Success(notice: message)
    end
  end
end
