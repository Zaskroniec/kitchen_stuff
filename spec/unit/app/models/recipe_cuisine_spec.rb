# frozen_string_literal: true

require 'rails_helper'

describe RecipeCuisine, type: :model do
  it { is_expected.to belong_to(:recipe) }
  it { is_expected.to belong_to(:cuisine) }

  it { is_expected.to have_db_index(%i[recipe_id cuisine_id]).unique(true) }

  it { is_expected.to have_db_column(:recipe_id).of_type(:uuid).with_options(null: false) }
  it { is_expected.to have_db_column(:cuisine_id).of_type(:uuid).with_options(null: false) }
end
