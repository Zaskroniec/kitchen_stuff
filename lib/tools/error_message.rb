# frozen_string_literal: true

module Tools
  class ErrorMessage
    def call(error:)
      {
        class:     error.class,
        backtrace: error.backtrace.join("\n")
      }.merge(info(error))
    end

    def self.call(**args)
      new.(**args)
    end

    private

    def parse_message(message)
      JSON.parse(message, symbolize_names: true)
    rescue Error::JsonParseError
      message
    end

    def info(error)
      data = parse_message(error.message)

      return { message: data } if data.is_a?(String)

      {
        message: data.slice(:errors),
        params:  data[:params]
      }
    end
  end
end
