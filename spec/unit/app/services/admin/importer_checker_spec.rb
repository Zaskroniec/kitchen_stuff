# frozen_string_literal: true

require 'rails_helper'

module Admin
  describe ImporterChecker do
    describe '#call' do
      let(:recipe_import_repo) { double(:recipe_import_repo) }
      let(:recipe_import)      { double(:recipe_import, imports: [], result: 0) }
      let(:supported_sites) do
        [
          {
            name: 'allrecipes',
            url:  'https://www.allrecipes.com/recipe/259870/briam-greek-baked-zucchini-and-potatoes'
          }
        ]
      end
      subject do
        described_class
          .new(recipe_import_repo: recipe_import_repo)
          .(supported_sites: supported_sites)
      end

      before do
        mock_request_with_result(request_args)

        expect(recipe_import_repo)
          .to receive(:create)
          .and_return(recipe_import)

        expect(recipe_import)
          .to receive(:result=)
          .with(1)
          .and_return(recipe_import)

        expect(recipe_import)
          .to receive(:save)
      end

      context 'without errors' do
        let(:request_args) do
          {
            method:    :get,
            url:       supported_sites[0][:url],
            options:   {
              headers: { 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'\
                                         ' (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' }
            },
            resp_code: 200,
            body:      read_example('example_import_html.txt')
          }
        end

        it 'returns recipe_import record with import collection size of 1' do
          expect(subject.imports.size).to eq(1)
        end

        it 'returns valid item recipe name' do
          expect(subject.imports[0][:name]).to eq('allrecipes')
        end

        it 'returns item empty error node' do
          expect(subject.imports[0][:errors]).to eq([])
        end

        it 'returns item content of data' do
          expect(subject.imports[0][:data]).to be_present
        end
      end

      context 'with errors' do
        let(:request_args) do
          {
            method:    :get,
            url:       supported_sites[0][:url],
            options:   {
              headers: { 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'\
                                         ' (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' }
            },
            resp_code: 200,
            body:      read_example('empty_import_html.txt')
          }
        end

        it 'returns recipe_import record with import collection size of 1' do
          expect(subject.imports.size).to eq(1)
        end

        it 'returns valid item recipe name' do
          expect(subject.imports[0][:name]).to eq('allrecipes')
        end

        it 'returns item empty error node' do
          expect(subject.imports[0][:errors]).to eq(['We are sorry, but currently we do not support this page.'])
        end

        it 'returns item content of data' do
          expect(subject.imports[0][:data]).to be_blank
        end
      end
    end
  end
end
