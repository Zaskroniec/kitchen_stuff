# frozen_string_literal: true

require 'rails_helper'

module Buckets
  describe DestroyOperation do
    describe '#call' do
      let(:name)           { Faker::Internet.name }
      let(:existed_bucket) { double(:bucket, id: random_uuid, name: name) }
      let(:deleted_bucket) { double(:bucket, id: nil, name: name) }
      subject              { described_class.new.(bucket: existed_bucket) }

      context 'success' do
        before { expect(existed_bucket).to receive(:destroy).and_return(deleted_bucket) }

        it 'returns valid message' do
          expect(subject.success[:notice]).to eq("Successfuly destroyed bucket: #{name}.")
        end
      end

      context 'failure' do
        before { expect(existed_bucket).to receive(:destroy).and_return(false) }

        it 'returns valid message' do
          expect(subject.failure[:message])
            .to eq("We are sorry, but we our system occured error while deleting bucket: #{name}. Try it later.")
        end
      end
    end
  end
end
