# frozen_string_literal: true

class RegistrationsController < ApplicationController
  include DependencyImporter[
    'operations.users.create_operation',
    'operations.users.confirmation_operation',
    'services.static_meta_tags'
  ]

  before_action :redirect_to_my_buckets

  def new
    set_meta_tags(**static_meta_tags.(key: :signup))
  end

  def create
    create_operation.(params: unsafe_params) do |service|
      service.success do |result|
        redirect_to new_sessions_path, notice: result[:notice]
      end

      service.failure do |error_object|
        render_error(:new, error_object)
      end
    end
  end

  def confirmation
    confirmation_operation.(params: unsafe_params) do |service|
      service.success do |result|
        redirect_to new_sessions_path, notice: result[:notice]
      end

      service.failure do |error_object|
        render_error(:confirmation, error_object)
      end
    end
  end
end
