# frozen_string_literal: true

require 'rails_helper'

describe TagGenerator do
  describe '#call' do
    let(:kernel) { double(:kernel) }
    subject      { described_class.new(kernel: kernel).(args) }

    context 'success' do
      context 'category' do
        let(:args) do
          {
            hash:   params,
            target: :categories_attributes,
            result: :recipe_categories_attributes
          }
        end
        let(:item)           { { name: 'pasta' } }
        let(:params)         { { categories_attributes: [item] } }
        let(:category)       { double(:category, id: random_uuid, name: item[:name].upcase) }
        let(:category_class) { double(:category_class) }

        before do
          expect(kernel).to         receive(:const_get).with('Category').and_return(category_class)
          expect(category_class).to receive(:find_or_create_by).with(name: item[:name].upcase).and_return(category)
        end

        it 'returns array with category id' do
          expect(subject)
            .to eq([{ categories_attributes: [] }, { recipe_categories_attributes: [{ category_id: category.id }] }])
        end
      end

      context 'cuisine' do
        let(:args) do
          {
            hash:   params,
            target: :cuisines_attributes,
            result: :recipe_cuisines_attributes
          }
        end
        let(:item)          { { name: 'japanese' } }
        let(:params)        { { cuisines_attributes: [item] } }
        let(:cuisine)       { double(:cuisine, id: random_uuid, name: item[:name].upcase) }
        let(:cuisine_class) { double(:cuisine_class) }

        before do
          expect(kernel).to        receive(:const_get).with('Cuisine').and_return(cuisine_class)
          expect(cuisine_class).to receive(:find_or_create_by).with(name: item[:name].upcase).and_return(cuisine)
        end

        it 'returns array with category id' do
          expect(subject)
            .to eq([{ cuisines_attributes: [] }, { recipe_cuisines_attributes: [{ cuisine_id: cuisine.id }] }])
        end
      end
    end
  end
end
