# frozen_string_literal: true

require 'dry-types'

module Types
  include Dry.Types()

  Coercible::Bool = Types::Bool.constructor { |value| ActiveRecord::Type::Boolean.new.serialize(value) }
end
