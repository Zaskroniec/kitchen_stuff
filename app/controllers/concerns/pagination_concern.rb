# frozen_string_literal: true

module PaginationConcern
  extend ActiveSupport::Concern
  include Pagy::Backend

  included do
    Pagy::VARS[:items] = Constants::DEFAULT_PAGINATION_SIZE
    Pagy::VARS[:size]  = Constants::DEFAULT_PAGINATION_RANGE

    private

    def paginate_resource(resource)
      pagy_countless(__send__(:"#{resource}_scope"))
    end

    def recipes_scope
      recipe_repo.searchable(
        user:       current_user,
        bucket:     (bucket if retrieve_bucket?),
        query:      (params[:search] if params[:search].present? && params[:search].length >= 3),
        eager_load: ([:bucket] unless retrieve_bucket?)
      )
    end

    def buckets_scope
      bucket_repo.searchable(
        user:  current_user,
        query: (params[:search] if params[:search].present? && params[:search].length >= 3)
      )
    end

    def retrieve_bucket?
      params[:slug].present? || params[:bucket_slug].present?
    end
  end
end
