# frozen_string_literal: true

module Recipes
  class TimeCalculator
    include DependencyImporter['tools.time_formatter']

    DEFAULT_MINUTES = 60

    def call(array:, options: {})
      array
        .map { |hash| calculate(hash.symbolize_keys) }
        .sum(0)
        .then { |sum| format_sum(sum, options) }
    end

    private

    def calculate(hash)
      hours   = (hash[:hours] || 0)
      minutes = (hash[:minutes] || 0)

      (((hours * DEFAULT_MINUTES) + minutes) * DEFAULT_MINUTES)
    end

    def format_sum(sum, options)
      return sum if options[:format].blank?

      time_formatter.__send__(options[:format], value: sum)
    end
  end
end
